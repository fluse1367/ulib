package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.function.BiParamFunc;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.impl.io.BitConsumer;
import cc.fluse.ulib.core.impl.io.BitSupplier;
import cc.fluse.ulib.core.impl.io.VarWord;
import org.jetbrains.annotations.NotNull;

/**
 * Class for reading and writing variable bit-length numbers.
 * <p>
 * A VarNum is split into blocks subsequently followed by an indicator bit, indicating if there is a next block.
 */
public final class VarNum {

    private static <N extends Number, X extends Exception, XX extends Exception>
    @NotNull N readVarNum(int blockLen, int bits, @NotNull N init,
                          @NotNull BiParamFunc<N, Integer, N, X> accumulator,
                          @NotNull Func<Integer, XX> byteSupplier) throws X, XX {
        return VarWord.decode(VarWord.readVarWord(blockLen, bits, new BitSupplier<>(byteSupplier)), init, accumulator);
    }

    private static <X extends Exception, XX extends Exception>
    void writeVarNum(int blockLen, int bits,
                     @NotNull ParamFunc<Integer, Boolean, X> bitIndexChecker,
                     @NotNull ParamTask<? super Integer, XX> byteConsumer) throws X, XX {

        VarWord.writeVarWord(blockLen, VarWord.encode(bits, bitIndexChecker), new BitConsumer<>(byteConsumer));
    }

    /**
     * Decodes a variable length 32-bit integer.
     *
     * @param blockLen the block length (excluding the indicator bit)
     * @param supplier a function supplying the next byte
     * @return the decoded 32-bit integer
     */
    public static <X extends Exception> int readVarInt(int blockLen, @NotNull Func<Integer, X> supplier) throws X {
        return readVarNum(blockLen, 32, 0, (num, i) -> num | 1 << i, supplier);
    }

    /**
     * Decodes a variable length 32-bit integer with a block length of 8 (including the indicator bit).
     *
     * @param supplier a function supplying the next byte
     * @return the decoded 32-bit integer
     */
    public static <X extends Exception> int readVarInt(@NotNull Func<Integer, X> supplier) throws X {
        return readVarInt(7, supplier);
    }

    /**
     * Encodes a variable length 32-bit integer.
     *
     * @param blockLen the block length (excluding the indicator bit)
     * @param num      the number to encode
     * @param consumer a function consuming the encoded bytes
     */
    public static <X extends Exception> void writeVarInt(int blockLen, int num, @NotNull ParamTask<? super Integer, X> consumer)
        throws X {
        writeVarNum(blockLen, 32, i -> (num & (1 << i)) != 0, consumer);
    }

    /**
     * Encodes a variable length 32-bit integer with a block length of 8 (including the indicator bit).
     *
     * @param num      the number to encode
     * @param consumer a function consuming the encoded bytes
     */
    public static <X extends Exception> void writeVarInt(int num, @NotNull ParamTask<? super Integer, X> consumer)
        throws X {
        writeVarInt(7, num, consumer);
    }

    /**
     * Decodes a variable length 64-bit integer.
     *
     * @param blockLen the block length (excluding the indicator bit)
     * @param supplier a function supplying the next byte
     * @return the decoded 64-bit integer
     */
    public static <X extends Exception> long readVarLong(int blockLen, @NotNull Func<Integer, X> supplier) throws X {
        return readVarNum(blockLen, 64, 0L, (num, i) -> num | 1L << i, supplier);
    }

    /**
     * Decodes a variable length 64-bit integer with a block length of 8 (including the indicator bit).
     *
     * @param supplier a function supplying the next byte
     * @return the decoded 64-bit integer
     */
    public static <X extends Exception> long readVarLong(@NotNull Func<Integer, X> supplier) throws X {
        return readVarLong(7, supplier);
    }

    /**
     * Encodes a variable length 64-bit integer.
     *
     * @param blockLen the block length (excluding the indicator bit)
     * @param num      the number to encode
     * @param consumer a function consuming the encoded bytes
     */
    public static <X extends Exception> void writeVarLong(int blockLen, long num, @NotNull ParamTask<? super Integer, X> consumer)
        throws X {
        writeVarNum(blockLen, 64, i -> (num & (1L << i)) != 0, consumer);
    }

    /**
     * Encodes a variable length 64-bit integer with a block length of 8 (including the indicator bit).
     *
     * @param num      the number to encode
     * @param consumer a function consuming the encoded bytes
     */
    public static <X extends Exception> void writeVarLong(long num, @NotNull ParamTask<? super Integer, X> consumer)
        throws X {
        writeVarLong(7, num, consumer);
    }

}
