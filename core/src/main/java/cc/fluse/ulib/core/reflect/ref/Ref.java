package cc.fluse.ulib.core.reflect.ref;

import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

/**
 * Reference to a reflective object.
 */
public interface Ref<T> {

    @NotNull
    String getName();

    @NotNull
    Expect<T, ?> tryLoad();
}
