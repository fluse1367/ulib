package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.ex.SoftIOException;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.io.transform.AbstractTransformer;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import org.jetbrains.annotations.NotNull;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public final class StreamTransformer extends AbstractTransformer {

    private final OutputStream sink;

    public <X extends Exception> StreamTransformer(@NotNull ParamFunc<@NotNull OutputStream, ? extends @NotNull FilterOutputStream, X> streamFactory)
        throws X {
        this.sink = streamFactory.execute(new OutputStream() {
            private final ByteBuffer buf = Internal.bufalloc(1);

            @Override
            public void write(int b) {
                StreamTransformer.super.writeResult(buf.clear().put(0, (byte) b));
            }
        });
    }

    @Override
    protected void implWrite(@NotNull ByteBuffer src) throws InputFormatException {
        while (src.hasRemaining()) {
            try {
                sink.write(src.get());
            } catch (IOException e) {
                throw new InputFormatException(e);
            }
        }
        try {
            sink.flush();
        } catch (IOException e) {
            throw new SoftIOException(e);
        }
    }

    @Override
    protected void implComplete() throws IOTransformException {
        try {
            sink.close();
        } catch (IOException e) {
            throw new IOTransformException(e);
        }
    }
}
