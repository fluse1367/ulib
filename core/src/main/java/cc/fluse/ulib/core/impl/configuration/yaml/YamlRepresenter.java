package cc.fluse.ulib.core.impl.configuration.yaml;

import cc.fluse.ulib.core.impl.configuration.SerializationAdapters;
import cc.fluse.ulib.core.impl.configuration.SerializationAdapters.Adapter;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.representer.Representer;

import java.util.function.BiConsumer;

class YamlRepresenter extends Representer implements BiConsumer<Class<?>, Adapter<?>> {
    private final SerialisationRepresenter representer;

    YamlRepresenter(DumperOptions options) {
        super(options);
        this.representer = new SerialisationRepresenter();
    }

    @Override
    public void accept(Class<?> clazz, Adapter<?> adapter) {
        super.multiRepresenters.put(clazz, representer);
    }

    private class SerialisationRepresenter extends RepresentMap {
        @Override
        public Node representData(Object object) {

            var serialized = SerializationAdapters.getInstance().attemptSerialization(object);
            if (serialized != null) {
                return super.representData(serialized);
            }

            return super.representData(object);
        }
    }
}
