package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.io.transform.CipherTransformer;
import cc.fluse.ulib.core.impl.io.transform.CompressionTransformer;
import cc.fluse.ulib.core.impl.io.transform.NopTransformer;
import cc.fluse.ulib.core.impl.io.transform.StreamTransformer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.ReadWrite;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import java.io.FilterOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.ClosedChannelException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * An object that may modify bytes from a stream.
 */
public interface ByteTransformer extends ReadWrite, ByteChannel {

    /**
     * Constructs a transformer processing bytes continuously through a stream. The stream is constructed using a
     * factory, which is passed a sink output stream. The sink output stream is a special stream that forwards all bytes
     * written to the transformer.
     *
     * @param streamFactory the factory to construct the stream
     * @return the transformer
     * @throws X exception thrown by the factory
     */
    static <X extends Exception> @NotNull ByteTransformer of(@NotNull ParamFunc<@NotNull OutputStream, ? extends @NotNull FilterOutputStream, X> streamFactory) throws X {
        return new StreamTransformer(streamFactory);
    }

    /**
     * Constructs a transformer processing bytes continuously through a cipher.
     *
     * @param cipher an initialized cipher object
     * @return the transformer
     */
    @NotNull
    static ByteTransformer of(@NotNull Cipher cipher) {
        return new CipherTransformer(cipher);
    }

    /**
     * Constructs a transformer processing bytes continuously through an inflater ("decompressor").
     *
     * @param decompressor the inflater object responsible for decompressing the data
     * @return the transformer
     */
    @NotNull
    static ByteTransformer of(@NotNull Inflater decompressor) {
        return new CompressionTransformer(decompressor);
    }

    /**
     * Constructs a transformer processing bytes continuously through a deflator ("compressor").
     *
     * @param compressor the deflater object responsible for compressing the data
     * @return the transformer
     */
    @NotNull
    static ByteTransformer of(@NotNull Deflater compressor) {
        return new CompressionTransformer(compressor);
    }

    /**
     * Constructs a new no-operation transformer, that only buffers the data and forwards them unmodified.
     *
     * @return the transformer
     */
    @NotNull
    static ByteTransformer dummy() {
        return new NopTransformer();
    }

    /**
     * Checks if the transformer still holds any internal buffered data.
     *
     * @return {@code true} if any data is held, {@code false} otherwise
     */
    boolean holdsData() throws ClosedChannelException;

    /**
     * Updates the transformer with the next bytes.
     *
     * @param src the source buffer
     * @return the number of bytes transferred
     * @throws IllegalStateException  if the transformer has completed
     * @throws InputFormatException   if the input is invalid for the transformer
     * @throws ClosedChannelException if the transformer is closed
     */
    @Override
    int write(@NotNull ByteBuffer src) throws InputFormatException, ClosedChannelException;

    /**
     * Transfers the result bytes into a destination buffer.
     *
     * @param dst the destination buffer
     * @return the number of bytes written; {@code 0} when no more result bytes are available (and the transformer is
     * not yet closed); {@code > 0} when all resulting bytes have been transferred (and the transformer is closed)
     */
    @Override
    int read(@NotNull ByteBuffer dst) throws ClosedChannelException;

    /**
     * Attempts to complete the processing of the data. After successful completion, no more bytes may be pushed.
     * <p>
     * This method has no effect if the transformer is already completed.
     *
     * @throws IOTransformException   if the transformer cannot complete the processing
     * @throws ClosedChannelException if the transformer is closed
     */
    void complete() throws IOTransformException, ClosedChannelException;

    /**
     * @return {@code true} if the transformer has completed, {@code false} otherwise
     */
    boolean isCompleted();

    /**
     * Closes the transformer. This works regardless of whether it has completed or not.
     * <p>
     * This method has no effect if the transformer is already closed.
     */
    void close();

    /**
     * @return {@code true} if the transformer is closed, {@code false otherwise}
     */
    default boolean isClosed() {
        return !isOpen();
    }

    /**
     * Returns an {@link IOUtil#isolate(ByteChannel) isolated byte channel} for this transformer.
     *
     * @return the channel
     */
    @Override
    default @NotNull ByteChannel channel() throws ClosedChannelException {
        return IOUtil.isolate(this);
    }
}
