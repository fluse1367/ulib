package cc.fluse.ulib.core.impl.database.sql.query;

import cc.fluse.ulib.core.database.sql.query.QueryEndpoint;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class QueryEndpointImpl implements QueryEndpoint {
    protected final Metadata meta;

    private boolean limit = false;

    QueryEndpointImpl(Metadata meta) {
        this.meta = meta;
    }

    @SneakyThrows
    @Override
    public @NotNull ResultSet query(@NotNull Object @NotNull ... parameters) {
        // intentionally not closing the statement as the ResultSet would be closed as well
        //noinspection resource
        return build(parameters).executeQuery();
    }

    @SneakyThrows
    @Override
    public int update(@NotNull Object @NotNull ... parameters) {
        try (var st = build(parameters)) {
            return st.executeUpdate();
        }
    }

    @Override
    public @NotNull PreparedStatement build() {
        try (var e = meta.sql.getConnection()) {
            return meta.applyOps(e.get().prepareStatement(buildRawQuery()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    @Override
    public @NotNull PreparedStatement build(@NotNull Object @NotNull ... parameters) {
        var st = build();

        var alreadySet = meta.set();

        for (int i = 0, param = 1; i < parameters.length; param++) {
            if (alreadySet.contains(param)) {
                continue;
            }

            st.setObject(param, parameters[i]);
            i++;
        }
        return st;
    }

    @Override
    public @NotNull QueryEndpointImpl limit(long limit) {
        if (!this.limit && limit >= 0) {
            meta.query.append(String.format(" LIMIT %d", limit));
            this.limit = true;
        }
        return this;
    }

    @Override
    public @NotNull String buildRawQuery() {
        return meta.query.toString();
    }
}
