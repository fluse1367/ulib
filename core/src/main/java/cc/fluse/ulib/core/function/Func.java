package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

/**
 * A task that returns a value and may throw a throwable object.
 *
 * @apiNote only pass this task object as supplier if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface Func<T, X extends Exception> extends Callable<T>, Supplier<T> {

    @Deprecated
    static <T, X extends Exception> Func<T, X> as(@NotNull Func<T, ?> func) {
        return func::get;
    }

    T execute() throws X;

    @Override
    default T call() throws Exception {
        return execute();
    }

    @SneakyThrows
    @Override
    @Deprecated
    default T get() {
        return execute();
    }
}
