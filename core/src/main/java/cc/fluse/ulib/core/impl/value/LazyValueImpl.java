package cc.fluse.ulib.core.impl.value;

import cc.fluse.ulib.core.ex.SoftInterruptedException;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.tuple.Value;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.LazyValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Stream;

public class LazyValueImpl<T> implements LazyValue<T> {

    private final Object $lock = new Object[0];

    private final Func<? extends T, ?> fetch;
    private final boolean canFetch;
    private final ParamTask<? super T, ?> push;
    private final boolean canPush;

    private volatile Value<T> value;
    private volatile Thread runner;

    public LazyValueImpl(@Nullable Value<? extends @Nullable T> value, @Nullable Func<? extends @Nullable T, ?> fetch, @Nullable ParamTask<? super @Nullable T, ?> push) {
        this.canFetch = (this.fetch = fetch) != null;
        this.canPush = (this.push = push) != null;

        if (value == null) {
            this.value = null;
            return;
        }
        this.value = Tuple.of(value.getFirst());
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void $interrupt() {
        Expect.compute(() -> ReflectUtil.doPrivileged(
                () -> ReflectUtil.icall(this.runner, "stop0()", Param.single(Object.class, new LazyValueCancel())))
        ).rethrowRE();
    }

    @Override
    public boolean isAvailable() {
        return canFetch || isPresent();
    }

    @Override
    public boolean isPresent() {
        synchronized ($lock) {
            return value != null;
        }
    }

    @Override
    public boolean isRunning() {
        synchronized ($lock) {
            return runner != null;
        }
    }

    @Override
    public T get() throws NoSuchElementException, SoftInterruptedException {
        synchronized ($lock) {
            if (runner != null) {
                try {
                    $lock.wait();
                } catch (InterruptedException e) {
                    throw new SoftInterruptedException(e);
                }
            }
            if (value != null) return value.getFirst();
            if (!canFetch) throw new NoSuchElementException();
            runner = Thread.currentThread();
        }

        try {
            return (this.value = Tuple.of(fetch.execute())).getFirst();
        } catch (LazyValueCancel e) {
            synchronized ($lock) {
                if (this.value == null) throw new NoSuchElementException();
                return this.value.getFirst();
            }
        } catch (Exception e) {
            this.value = null;
            throw new RuntimeException(e);
        } finally {
            synchronized ($lock) {
                runner = null;
                $lock.notifyAll();
            }
        }
    }

    @Override
    public @Nullable T set(@Nullable T t) {
        synchronized ($lock) {
            if (runner != null) $interrupt();
            this.value = Tuple.of(t);

            if (canPush) {
                try {
                    push.execute(t);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            return t;
        }
    }

    @Override
    public void clear() {
        synchronized ($lock) {
            if (runner != null) $interrupt();
            this.value = null;
        }
    }

    // tuple impl

    @Override
    public T getFirst() {
        return get();
    }

    @Override
    public int size() {
        return value == null ? 0 : 1;
    }

    @Override
    public @NotNull Stream<Object> stream() {
        return Stream.of(get());
    }

    @NotNull
    @Override
    public Iterator<Object> iterator() {
        return Spliterators.iterator(spliterator());
    }

    @Override
    public Spliterator<Object> spliterator() {
        return Arrays.spliterator(new Object[]{get()});
    }
}
