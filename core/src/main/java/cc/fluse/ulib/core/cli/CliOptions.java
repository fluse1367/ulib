package cc.fluse.ulib.core.cli;

import cc.fluse.ulib.core.cli.ex.OptionException;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.impl.cli.Options;
import cc.fluse.ulib.core.impl.cli.ParsedArgs;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class for defining and parsing linux style command line options and arguments.
 */
public interface CliOptions {

    /**
     * @return a new instance
     */
    @NotNull
    static CliOptions create() {
        return new Options();
    }

    /**
     * Obtains an option by its long name.
     *
     * @param name the option's long name
     * @return an optional wrapping the option, or an empty option if the option was not found
     */
    @NotNull
    Optional<CliOption> getOption(@NotNull String name);

    /**
     * Obtains an option by one of its short names.
     *
     * @param shortName one of the option's short names
     * @return an optional wrapping the option, or an empty option if the option was not found
     */
    @NotNull
    Optional<CliOption> getOption(char shortName);

    /**
     * Adds an option to this instance.
     *
     * @param name        the long name of the option
     * @param shortNames  the short (one char) names of the option (usually the first letter of the long name)
     * @param description the option's description
     * @param argProps    the option's argument properties, defining how to accept arguments
     * @return the option object
     */
    @NotNull
    CliOption option(@NotNull String name, char @NotNull [] shortNames, @NotNull String description, @NotNull ArgProps argProps);

    /**
     * Parses an array of (usually user supplied) cli strings.
     *
     * @param cliArgs the strings to parse
     * @return an {@link ParsedArgs} objects representing the parsed data
     *
     * @throws OptionException if a parsing error occurs (usually on incompatible options)
     */
    @NotNull
    CliArgs parse(@NotNull String @NotNull [] cliArgs) throws OptionException;

    <X0 extends Exception, X1 extends Exception, X2 extends Exception, X3 extends Exception>
    void printHelp(@NotNull ParamTask<@NotNull String, X0> printer, int pfxSpacing, int nameWidth, int spacing, int descWidth,
                   @NotNull ParamFunc<@NotNull ArgProps, @Nullable String, X1> argStr,
                   @NotNull ParamFunc<@NotNull Set<@NotNull Set<@NotNull CliOption>>, @Nullable String, X2> requiresStr,
                   @NotNull ParamFunc<@NotNull Set<@NotNull CliOption>, @Nullable String, X3> incompatiblesStr)
    throws X0, X1, X2, X3;

    default <X extends Exception> void printHelp(@NotNull ParamTask<@NotNull String, X> printer, int pfxSpacing, int nameWidth, int spacing, int descWidth)
    throws X {
        printHelp(printer, pfxSpacing, nameWidth, spacing, descWidth,
                  props -> "[" + ((Function<ArgNum, String>) num -> {
                      int min = num.min();
                      int max = num.max();

                      // exact amount
                      if (min == max) {
                          return switch (min) {
                              case 0 -> "No arguments";
                              case 1 -> "Exactly one argument";
                              default -> "Exactly " + min + " arguments";
                          };
                      }

                      // at least
                      if (max == Integer.MAX_VALUE) {
                          return switch (min) {
                              case 0 -> "Any number of arguments";
                              case 1 -> "At least one argument";
                              default -> "At least " + min + " arguments";
                          };
                      }

                      // at most
                      if (min == 0) {
                          return max == 1 ? "One optional argument" : max + " optional arguments";
                      }

                      // range
                      return min + "..." + max + " arguments";
                  }).apply(props.num()) + (props.defaultArguments().isEmpty() ? "]"
                                                                              : (", Default: {%s}]".formatted(String.join(",", props.defaultArguments())))),
                  requires -> "[Requires %s]".formatted(requires.stream()
                                                                .map(set -> set.stream().map(CliOption::getName).collect(Collectors.joining(", --", "all of {--", "}")))
                                                                .collect(Collectors.joining(" or ", "any of (", ")"))),
                  incompatibles -> "[Incompatible with {%s}]".formatted(incompatibles.stream()
                                                                                     .map(CliOption::getName)
                                                                                     .collect(Collectors.joining(", --", "--", "")))
        );
    }

    default <X extends Exception> void printHelp(@NotNull ParamTask<@NotNull String, X> printer) throws X {
        printHelp(printer, 5, 20, 5, 120);
    }
}
