package cc.fluse.ulib.core.file;

import cc.fluse.ulib.core.common.Sizable;
import cc.fluse.ulib.core.impl.file.FSResource;
import cc.fluse.ulib.core.io.ReadWrite;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

/**
 * Represents a resource located at a specific path.
 * <p>
 * Each opened read channel or stream reads from the same file independently of each other. Each opened write channel or
 * stream overwrites the underlying file independently of each other. Simultaneous read and write operations are not *
 * defined and may result in undefined behavior.
 */
public interface FilesystemResource extends ReadWrite, Sizable {

    /**
     * Constructs a new filesystem resource from the given path.
     *
     * @param path the resource's location
     * @return the new object
     */
    @NotNull
    static FilesystemResource of(@NotNull Path path) {
        return new FSResource(path);
    }

    /**
     * @return the location of this resource
     */
    @NotNull
    Path getLocation();
}
