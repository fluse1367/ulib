package cc.fluse.ulib.core.cli.ex;

import cc.fluse.ulib.core.cli.CliOption;
import org.jetbrains.annotations.NotNull;

public class IllegalOptionArgumentException extends OptionException {

    public IllegalOptionArgumentException(@NotNull CliOption option, @NotNull String message) {
        super(option, "Option '%s' %s".formatted(option.getName(), message));
    }

    public IllegalOptionArgumentException(@NotNull String message) {
        super(null, message);
    }
}
