package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.PipeBufferChannel;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.function.BooleanSupplier;

public final class SyntheticChannel implements ByteChannel {

    private final ChBouncer bouncer = Bouncer.ch();
    private final ReadableByteChannel r;
    private final WritableByteChannel w;

    public SyntheticChannel(ReadableByteChannel r, WritableByteChannel w) {
        this.r = r;
        this.w = w;
    }

    @Override
    public synchronized int read(ByteBuffer dst) throws IOException {
        bouncer.pass();
        return r.read(dst);
    }

    @Override
    public synchronized int write(ByteBuffer src) throws IOException {
        bouncer.pass();
        return w.write(src);
    }

    @Override
    public synchronized boolean isOpen() {
        return bouncer.canPass() && r.isOpen() && w.isOpen();
    }

    @Override
    public synchronized void close() throws IOException {
        IOUtil.closeIO(bouncer, r, w);
    }


    public static final class Read implements ReadableByteChannel {

        public static Read nextBytes(BooleanSupplier hasNext, Func<byte @Nullable [], ? extends IOException> nextBytesSupplier, int cap) {
            return next(hasNext, buf -> {
                var bytes = nextBytesSupplier.execute();
                if (bytes == null) return false;
                buf.write(bytes);
                return true;
            }, cap);
        }

        public static Read nextBuffer(BooleanSupplier hasNext, Func<@NotNull ByteBuffer, ? extends IOException> nextBytesSupplier, int cap) {
            return next(hasNext, buf -> {
                var bytes = nextBytesSupplier.execute();
                if (!bytes.hasRemaining()) return false;
                buf.write(bytes);
                return true;
            }, cap);
        }

        private static Read next(BooleanSupplier hasNext, ParamFunc<? super PipeBufferChannel, Boolean, ? extends IOException> filler, int cap) {
            //noinspection resource
            var buf = new PipeBufferChannel();
            return new Read(hasNext, dst -> {
                var src = buf.obtain();

                if (!src.hasRemaining()) {
                    // try fill
                    if (!filler.execute(buf)) return 0;
                    src = buf.obtain();
                }

                dst.put(src);
                int written = src.position();
                buf.purge(written);
                return written;
            }, cap);
        }

        private final ChBouncer bouncer = Bouncer.ch();
        private final BooleanSupplier hasNext;
        private final ParamFunc<? super ByteBuffer, ? extends Integer, ? extends IOException> bufferFiller;
        private final ByteBuffer buffer;

        public Read(BooleanSupplier hasNext, ParamFunc<? super ByteBuffer, ? extends Integer, ? extends IOException> bufferFiller, int capacity) {
            this.hasNext = hasNext;
            this.bufferFiller = bufferFiller;
            this.buffer = Internal.bufalloc(capacity).flip();
        }

        @Override
        public synchronized int read(ByteBuffer dst) throws IOException {
            bouncer.pass();

            int i = 0, rem;
            while ((rem = dst.remaining()) > 0) {

                // attempt to fill if necessary
                if (!buffer.hasRemaining() && !fill()) return i == 0 ? -1 : i;

                // write to dst buffer
                var sl = buffer.slice(buffer.position(), Math.min(rem, buffer.remaining()));
                dst.put(sl);
                buffer.position(buffer.position() + sl.position());
                i += sl.position();
            }

            return i;
        }

        private synchronized boolean fill() throws IOException {
            if (buffer.hasRemaining()) return true; // no fill if data available
            if (!hasNext.getAsBoolean()) return false; // no more data to grab

            // fill the buffer
            buffer.clear();
            if (bufferFiller.execute(buffer) == 0) return false;
            buffer.flip();

            // return if new data is available
            return buffer.hasRemaining();
        }

        @Override
        public synchronized boolean isOpen() {
            return bouncer.canPass();
        }

        @Override
        public synchronized void close() throws IOException {
            bouncer.block();
        }
    }


    @RequiredArgsConstructor
    public final static class Write implements WritableByteChannel {
        private final ChBouncer bouncer = Bouncer.ch();
        private final ParamFunc<? super ByteBuffer, ? extends Integer, ? extends IOException> delegate;

        @Override
        public synchronized int write(ByteBuffer src) throws IOException {
            bouncer.pass();
            return delegate.execute(src);
        }

        @Override
        public synchronized boolean isOpen() {
            return bouncer.canPass();
        }

        @Override
        public synchronized void close() throws IOException {
            bouncer.block();
        }
    }


}
