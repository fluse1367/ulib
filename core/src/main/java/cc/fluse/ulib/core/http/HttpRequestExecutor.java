package cc.fluse.ulib.core.http;

import cc.fluse.ulib.core.impl.http.DigestAuthenticationExecutor;
import cc.fluse.ulib.core.impl.http.NopExecutor;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.http.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * An object that executes (sends) http requests on demand.
 *
 * @see #nop(HttpClient)
 */
public interface HttpRequestExecutor {

    /**
     * Creates a no-operation executor that only delegates the request execution to a http client.
     *
     * @param client the http client to delegate the requests to
     * @return the newly created executor
     */
    @NotNull
    static HttpRequestExecutor nop(@NotNull HttpClient client) {
        return new NopExecutor(client);
    }

    /**
     * Creates an executor implementing HTTP Digest Access Authentication as defined by <a
     * href="https://www.rfc-editor.org/rfc/rfc7616">RFC 7616</a>.
     *
     * @param client   the http client to use
     * @param username the username to use in the authentication
     * @param password the password to use in the authentication
     * @return the newly created executor
     */
    @NotNull
    static HttpRequestExecutor digestAuth(@NotNull HttpClient client, @NotNull String username, @NotNull String password) {
        return new DigestAuthenticationExecutor(client, username, password);
    }


    /**
     * Sends the request.
     *
     * @see java.net.http.HttpClient#sendAsync(HttpRequest, HttpResponse.BodyHandler)
     */
    @NotNull <T> CompletableFuture<HttpResponse<T>> sendAsync(@NotNull HttpRequest request, @NotNull HttpResponse.BodyHandler<T> responseHandler);

    @NotNull
    default <T> HttpResponse<T> send(@NotNull HttpRequest request, @NotNull HttpResponse.BodyHandler<T> responseHandler)
    throws IOException, InterruptedException {
        try {
            return sendAsync(request, responseHandler).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof IOException ioe) {
                throw ioe;
            }
            throw new RuntimeException(e);
        }
    }


}
