package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiFunction;

/**
 * A task that takes one parameter, returns a value and may throw a throwable object.
 *
 * @apiNote only pass this task object as function if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface BiParamFunc<T, U, R, X extends Exception> extends BiFunction<T, U, R> {

    @Deprecated
    static <T, U, R, X extends Exception> BiParamFunc<T, U, R, X> as(@NotNull BiParamFunc<T, U, R, ?> func) {
        return func::apply;
    }

    R execute(T t, U u) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default R apply(T t, U u) {
        return execute(t, u);
    }
}
