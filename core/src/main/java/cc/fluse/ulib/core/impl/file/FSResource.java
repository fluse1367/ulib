package cc.fluse.ulib.core.impl.file;

import cc.fluse.ulib.core.file.FilesystemResource;
import cc.fluse.ulib.core.impl.io.SyntheticChannel;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.ByteChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.*;

public class FSResource implements FilesystemResource {

    private final Path path;

    public FSResource(Path path) {
        this.path = path;
    }

    @Override
    public @NotNull Path getLocation() {
        return path;
    }

    @Override
    public @NotNull InputStream streamRead() throws IOException {
        return Files.newInputStream(path);
    }

    @Override
    public @NotNull ReadableByteChannel channelRead() throws IOException {
        return Files.newByteChannel(path, READ);
    }

    @Override
    public long getSize() {
        return Expect.compute(Files::size, path).orElseThrow();
    }

    @Override
    public @NotNull OutputStream streamWrite() throws IOException {
        return Files.newOutputStream(path);
    }

    @Override
    public @NotNull WritableByteChannel channelWrite() throws IOException {
        return Files.newByteChannel(path, WRITE, CREATE);
    }

    @Override
    public @NotNull ByteChannel channel() throws IOException {
        return new SyntheticChannel(channelRead(), channelWrite());
    }
}
