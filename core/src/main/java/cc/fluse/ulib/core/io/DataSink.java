package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Objects;

/**
 * A {@link DataOutput} with default implementations for all methods.
 */
public interface DataSink extends DataOutput {

    /**
     * @see WritableByteChannel#write(ByteBuffer)
     */
    int write(@NotNull ByteBuffer buffer) throws IOException;

    /**
     * @see OutputStream#write(int)
     */
    default void write(int b) throws IOException {
        try (var e = Pool.BYTE_BUFFER_SINGLE_POOL.obtain()) {
            e.get().put(0, (byte) b);
            write(e.get());
        }
    }

    /**
     * @see OutputStream#write(byte[])
     */
    default void write(byte @NotNull [] b) throws IOException {
        write(b, 0, b.length);
    }

    /**
     * @see OutputStream#write(byte[], int, int)
     */
    default void write(byte @NotNull [] b, int off, int len) throws IOException {
        Objects.checkFromIndexSize(off, len, b.length);
        write(ByteBuffer.wrap(b, off, len));
    }


    @Override
    default void writeBoolean(boolean v) throws IOException {
        write(v ? 1 : 0);
    }

    @Override
    default void writeByte(int v) throws IOException {
        write(v);
    }

    @Override
    default void writeShort(int v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putShort((short) v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeChar(int v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putChar((char) v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeInt(int v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putInt(v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeLong(long v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putLong(v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeFloat(float v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putFloat(v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeDouble(double v) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            e.get().putDouble(v);
            write(e.get().flip());
        }
    }

    @Override
    default void writeBytes(@NotNull String s) throws IOException {
        try (var e = Pool.BYTE_BUFFER_SINGLE_POOL.obtain()) {
            var buf = e.get();
            for (int i = 0; i < s.length(); i++) {
                buf.put(0, (byte) s.charAt(i));
                write(buf);
            }
        }
    }

    @Override
    default void writeChars(@NotNull String s) throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            var buf = e.get().limit(2);
            for (int i = 0; i < s.length(); i++) {
                buf.putChar(0, s.charAt(i));
                write(buf);
            }
        }
    }

    @Override
    default void writeUTF(@NotNull String s) throws IOException {
        Expect.compute(() -> ReflectUtil.scall(DataOutputStream.class, "writeUTF()",
                Param.listOf(String.class, s, DataOutput.class, this)))
            .rethrow(IOException.class);
    }
}
