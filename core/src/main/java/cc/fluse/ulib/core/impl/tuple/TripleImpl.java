package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.Triple;

public class TripleImpl<T, U, V> extends PairImpl<T, U> implements Triple<T, U, V> {

    public TripleImpl(T t, U u, V v) {
        super(t, u, v);
    }

    protected TripleImpl(Object... elements) {
        super(elements);
    }

    @Override
    public V getThird() {
        return getI(2);
    }
}
