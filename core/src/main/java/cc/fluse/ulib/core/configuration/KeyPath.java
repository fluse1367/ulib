package cc.fluse.ulib.core.configuration;

import cc.fluse.ulib.core.impl.configuration.KeySections;
import org.jetbrains.annotations.*;

import java.util.*;

/**
 * Represents the key path to a value in a {@link Configuration}.
 */
public interface KeyPath extends Iterable<String> {

    String SECTION_DELIMITER = ".";

    String ESCAPE_SEQUENCE = "\\";

    @NotNull
    @Contract("_ -> new")
    static KeyPath parse(@NotNull String path) {
        return new KeySections(path, KeyPath.SECTION_DELIMITER, KeyPath.ESCAPE_SEQUENCE);
    }

    @NotNull
    @Contract("_ -> new")
    static KeyPath of(@NotNull String key) {
        return new KeySections(Collections.singletonList(key));
    }

    @NotNull
    static KeyPath of(@NotNull String @NotNull ... keys) {
        return new KeySections(Arrays.asList(keys));
    }

    @NotNull
    @Contract("-> new")
    Iterator<String> iterator();

    /**
     * @return a copy of the internal held path section.
     */
    @NotNull
    @Contract("-> new")
    @Unmodifiable
    List<String> getSections();

    @NotNull
    @Contract("_ -> new")
    KeyPath chain(@NotNull KeyPath @NotNull ... others);

    @NotNull
    String toLegacyString(@NotNull String delimiter, @NotNull String escapeSequence);

    @NotNull
    default String toLegacyString() {
        return toLegacyString(SECTION_DELIMITER, ESCAPE_SEQUENCE);
    }

}
