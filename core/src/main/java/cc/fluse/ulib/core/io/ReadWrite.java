package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.impl.io.SyntheticChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * Represents something that can be read from and written to.
 *
 * @implSpec In contrast to {@link Readable} and {@link Writable}, only {@link #channel()} has to be overwritten.
 * Specific characteristics for the methods have to be defined by the implementing class.
 */
public interface ReadWrite extends Readable, Writable {

    /**
     * Creates a new {@link ReadWrite} object from the specified streams. Each opened stream or channel is backed by the
     * respective readable or writable, thus they inherit the characteristics of the respective object.
     *
     * @param readable the readable to read from
     * @param writable the writable to write to
     * @return a new {@link ReadWrite} object wrapping the given objects
     */
    static @NotNull ReadWrite of(@NotNull Readable readable, @NotNull Writable writable) {
        return new ReadWrite() {
            @Override
            public @NotNull InputStream streamRead() throws IOException {
                return readable.streamRead();
            }

            @Override
            public @NotNull OutputStream streamWrite() throws IOException {
                return writable.streamWrite();
            }

            @Override
            public @NotNull ReadableByteChannel channelRead() throws IOException {
                return readable.channelRead();
            }

            @Override
            public @NotNull WritableByteChannel channelWrite() throws IOException {
                return writable.channelWrite();
            }

            @Override
            public @NotNull ByteChannel channel() throws IOException {
                return new SyntheticChannel(readable.channelRead(), writable.channelWrite());
            }
        };
    }

    /**
     * Creates a new {@link ReadWrite} object from the specified channels. Each opened stream or channel is backed by
     * the respective channel, thus they <b>do not</b> operate independently. The returned streams and channels are
     * isolated, meaning a close operation has no effect on the given source channels.
     *
     * @param readChannel  the channel to read from
     * @param writeChannel the channel to write to
     * @return a new {@link ReadWrite} object wrapping the given channels
     */
    static @NotNull ReadWrite of(@NotNull ReadableByteChannel readChannel, @NotNull WritableByteChannel writeChannel) {
        return new ReadWrite() {
            @Override
            public @NotNull ReadableByteChannel channelRead() {
                return IOUtil.isolate(readChannel);
            }

            @Override
            public @NotNull WritableByteChannel channelWrite() {
                return IOUtil.isolate(writeChannel);
            }

            @Override
            public @NotNull ByteChannel channel() {
                return new SyntheticChannel(IOUtil.isolate(readChannel), IOUtil.isolate(writeChannel));
            }
        };
    }

    /**
     * Creates a new {@link ReadWrite} object from the specified channel. Each opened stream or channel is backed by the
     * given channel, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source channel.
     *
     * @param channel the channel to read from and write to
     * @return a new {@link ReadWrite} object wrapping the given channel
     */
    static @NotNull ReadWrite of(@NotNull ByteChannel channel) {
        return of(channel, channel);
    }


    /**
     * Creates a new {@link ReadWrite} object from the specified streams. Each opened stream or channel is backed by the
     * respective stream, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source streams.
     *
     * @param in  the stream to read from
     * @param out the stream to write to
     * @return a new {@link ReadWrite} object wrapping the given streams
     */
    static @NotNull ReadWrite of(@NotNull InputStream in, @NotNull OutputStream out) {
        return of(Channels.newChannel(in), Channels.newChannel(out));
    }

    /**
     * Opens a channel to read and write contents from and to the object. The channel is not closed automatically,
     * closure has to be handled by the caller.
     *
     * @return the channel
     * @implSpec Specific characteristics for the channel have to be defined by the implementing class.
     */
    @NotNull ByteChannel channel() throws IOException;

    /**
     * Creates a read-only view of this object. Characteristics from {@link #channel()} are inherited.
     *
     * @return a read-only view of this object
     */
    default @NotNull Readable asReadOnly() {
        return () -> new ReadableByteChannel() {
            private final ReadableByteChannel channel = ReadWrite.this.channel();

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return channel.read(dst);
            }

            @Override
            public boolean isOpen() {
                return channel.isOpen();
            }

            @Override
            public void close() throws IOException {
                channel.close();
            }
        };
    }

    /**
     * Creates a write-only view of this object. Characteristics from {@link #channel()} are inherited.
     *
     * @return a write-only view of this object
     */
    default @NotNull Writable asWriteOnly() {
        return () -> new WritableByteChannel() {
            private final WritableByteChannel channel = ReadWrite.this.channel();

            @Override
            public int write(ByteBuffer src) throws IOException {
                return channel.write(src);
            }

            @Override
            public boolean isOpen() {
                return channel.isOpen();
            }

            @Override
            public void close() throws IOException {
                channel.close();
            }
        };
    }

    @Override
    default @NotNull ReadableByteChannel channelRead() throws IOException {
        return channel();
    }

    @Override
    default @NotNull WritableByteChannel channelWrite() throws IOException {
        return channel();
    }
}
