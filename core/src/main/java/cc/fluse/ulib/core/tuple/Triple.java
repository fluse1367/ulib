package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;

/**
 * An object able to hold 3 values.
 *
 * @param <T> the type of the first value
 * @param <U> the type of the second value
 * @param <V> the type of the third value
 */
@BypassAnnotationEnforcement
public interface Triple<T, U, V> extends Pair<T, U> {
    V getThird();
}
