package cc.fluse.ulib.core.impl.http;

import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.http.HttpUtil;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class RResource implements RemoteResource {

    private final URL url;
    private final ParamFunc<? super URL, ? extends InputStream, ?> executor;

    public RResource(URL url, ParamFunc<? super URL, ? extends InputStream, ?> executor) {
        this.url = url;
        this.executor = executor;
    }

    public RResource(URL url) {
        this(url, $url -> HttpUtil.GET($url.toURI()).body());
    }

    @Override
    public @NotNull URL getRemoteLocation() {
        return url;
    }

    @Override
    public @NotNull InputStream streamRead() {
        return Expect.compute(executor::execute, url).orElseThrow();
    }

    @Override
    public @NotNull ReadableByteChannel channel() throws IOException {
        return Channels.newChannel(streamRead());
    }
}
