package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.io.datachannel.SeekableDataChannelDelegate;
import cc.fluse.ulib.core.io.IOUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;

/**
 * A {@link ReadableDataChannel} that also implements {@link SeekableByteChannel}.
 */
public interface SeekableDataChannel extends SeekableByteChannel, DataChannel {

    /**
     * Wraps a {@link SeekableByteChannel} in a {@link SeekableDataChannel}. If the channel is already a
     * {@link SeekableDataChannel}, it is returned as-is. Otherwise, a new {@link SeekableDataChannel} is created that
     * delegates to the given channel.
     * <p>
     * The returned object is not guaranteed to be thread-safe.
     *
     * @param channel the channel to wrap
     * @return a {@link SeekableDataChannel} that delegates to the given channel
     */
    static @NotNull SeekableDataChannel wrap(@NotNull SeekableByteChannel channel) {
        return channel instanceof SeekableDataChannel dch ? dch : new SeekableDataChannelDelegate(channel);
    }

    @Override
    @NotNull
    default SeekableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    @Override
    @NotNull SeekableDataChannel position(long newPosition) throws IOException;

    @Override
    @NotNull SeekableDataChannel truncate(long size) throws IOException;
}
