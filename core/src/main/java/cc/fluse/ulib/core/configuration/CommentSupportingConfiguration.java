package cc.fluse.ulib.core.configuration;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;

import static cc.fluse.ulib.core.configuration.KeyPath.parse;

public interface CommentSupportingConfiguration extends Configuration {
    /**
     * Returns the comments of a specific key.
     *
     * @param path the key path
     * @return a list with the comment lines, or {@code null} if {@code path} not found
     */
    @NotNull
    Optional<List<String>> getComments(@NotNull KeyPath path);

    /**
     * Returns the comments of a specific key.
     *
     * @param path the key path; elements seperated by {@code .}
     * @return a list with the comment lines, or {@code null} if {@code path} not found
     */
    @NotNull
    default Optional<List<String>> getComments(@NotNull String path) {
        return getComments(parse(path));
    }

    /**
     * Sets the comments of a specific key.
     *
     * @param path  the key path
     * @param lines the comment lines to set
     * @throws IllegalArgumentException if path is not found
     */
    void setComments(@NotNull KeyPath path, @NotNull String... lines) throws IllegalArgumentException;

    /**
     * Sets the comments of a specific key.
     *
     * @param path  the key path; elements seperated by {@code .}
     * @param lines the comment lines to set
     * @throws IllegalArgumentException if path is not found
     */
    default void setComments(@NotNull String path, @NotNull String... lines) throws IllegalArgumentException {
        setComments(parse(path), lines);
    }

    /**
     * Sets the comments of a specific key.
     *
     * @param path  the key path
     * @param lines the comment lines to set
     * @throws IllegalArgumentException if path is not found
     */
    default void setComments(@NotNull KeyPath path, @NotNull List<String> lines) throws IllegalArgumentException {
        setComments(path, lines.toArray(new String[0]));
    }

    /**
     * Sets the comments of a specific key.
     *
     * @param path  the key path; elements seperated by {@code .}
     * @param lines the comment lines to set
     * @throws IllegalArgumentException if path is not found
     */
    default void setComments(@NotNull String path, @NotNull List<String> lines) throws IllegalArgumentException {
        setComments(parse(path), lines);
    }
}
