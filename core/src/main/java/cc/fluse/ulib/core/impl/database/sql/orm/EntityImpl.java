package cc.fluse.ulib.core.impl.database.sql.orm;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.orm.Entity;
import cc.fluse.ulib.core.database.sql.orm.Serializer;
import cc.fluse.ulib.core.database.sql.query.QueryEndpoint;
import cc.fluse.ulib.core.database.sql.query.QueryStart;
import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.impl.Concurrent;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.tuple.BiMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.UnknownNullability;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Phaser;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntityImpl implements Entity {

    private final Bouncer.StateBouncer bouncer = Bouncer.is();

    private final boolean cacheEnabled, pushLazy;
    private final AbstractSqlTable parent;
    private final Function<? super QueryStart, ? extends QueryEndpoint> selector;
    private final Map<Column<?>, Collection<Serializer<?, ?>>> serializers;

    // column -> direct value, serialized value
    private final BiMap<Column<?>, Object, Object> cache;

    // column -> lock
    private final Map<Column<?>, Object> $locks = Concurrent.newThreadsafeMap();

    private final Phaser pendingOps = new Phaser();

    public EntityImpl(AbstractSqlTable parent, Function<? super QueryStart, ? extends QueryEndpoint> selector,
                      Map<String, Collection<Serializer<?, ?>>> serializers,
                      boolean fetchLazy, boolean cacheEnabled, boolean pushLazy) throws SQLException, Serializer.SerializerException {
        this.parent = parent;
        this.selector = selector;
        this.serializers = serializers.entrySet().stream()
                .map(en -> Map.entry(parent.getColumn(en.getKey()).orElseThrow(), en.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        //noinspection AssignmentUsedAsCondition
        this.cache = (this.cacheEnabled = cacheEnabled) ? BiMap.wrap(Concurrent.newThreadsafeMap()) : null;
        this.pushLazy = pushLazy;

        // init locks
        parent.columns().forEach(c -> $locks.put(c, new Object[0]));

        if (fetchLazy) return;

        // !fetchLazy implies cache
        if (!cacheEnabled) throw new IllegalArgumentException("Cannot have disabled cache while fetching eagerly.");

        // fetch all columns now
        try (var res = selector.apply(parent.select("*")).query()) {
            if (!res.next()) throw new IllegalStateException("No rows returned.");
            // iterate over all columns
            for (var col : parent.columns()) {
                var val = res.getObject(col.getName());
                if (val == null) continue;

                // check for serializer
                var ser = findSerializer(col, val, true);
                var deserialized = ser == null ? val : ((Serializer<Object, Object>) ser).deserialize(val);

                // put into cache
                cache.put(col, val, deserialized);
            }
            if (res.next()) throw new IllegalStateException("More than one row returned.");
        }

    }

    @Override
    public @NotNull AbstractSqlTable getTable() {
        return parent;
    }

    /**
     * Gets the corresponding lock for the given column.
     *
     * @param column The column to get the lock for.
     * @return The lock.
     */
    private Object getLock(Column<?> column) {
        bouncer.pass();
        var $lock = $locks.get(column);
        if ($lock == null) throw new IllegalArgumentException("Column not found: " + column.getName());
        return $lock;
    }

    private <T> Serializer<T, ?> findSerializer(Column<T> col, Object value, boolean deserialize) {
        //noinspection unchecked
        return (Serializer<T, ?>) Stream.ofNullable(serializers.get(col)).flatMap(Collection::stream)
                .filter(!deserialize ? ser -> ser.getJavaType().isInstance(value) : ser -> ser.getSerializedType().isInstance(value))
                .findAny().orElse(null);
    }

    @Override
    public <T> @UnknownNullability T getDirect(@NotNull Column<T> col) {
        synchronized (getLock(col)) {
            var val = cache.getFirst(col);
            if (val != null) return col.getType().cast(val);

            try (var res = selector.apply(parent.select(col.getName())).query()) {
                if (!res.next()) throw new SQLException("No rows returned.");
                var obj = col.getType().cast(res.getObject(1));
                if (cacheEnabled) cache.put(col, obj, null);
                return obj;
            } catch (SQLException e) {
                throw new IllegalArgumentException("Column not found: " + col.getName(), e);
            }
        }
    }

    @Override
    public @UnknownNullability Object get(@NotNull Column<?> column) throws Serializer.SerializerException {
        synchronized (getLock(column)) {
            var obj = getDirect(column);
            if (obj == null) return null;

            // check for serializer
            var ser = findSerializer(column, obj, true);
            if (ser == null) return obj; // no serializer, return raw value

            // check for cached deserialized value
            if (cacheEnabled) {
                var deserialized = cache.getSecond(column);
                if (deserialized != null) return deserialized;
            }

            // deserialize
            //noinspection unchecked
            var deserialized = ((Serializer<?, Object>) ser).deserialize(obj);
            if (cacheEnabled) cache.putSecond(column, deserialized);

            return deserialized;
        }
    }

    @Override
    public <T> void setDirect(@NotNull Column<T> column, @UnknownNullability T value) {
        synchronized (getLock(column)) {
            // check value type
            if (value != null && !column.getType().isInstance(value))
                throw new IllegalArgumentException("Value type mismatch: " + value.getClass().getName() + " != " + column.getType().getName());

            // cache if enabled
            if (cacheEnabled) cache.put(column, value, null);

            // push to database
            push(column, value);
        }
    }

    @Override
    public void set(@NotNull Column<?> column, @UnknownNullability Object value) throws Serializer.SerializerException {
        synchronized (getLock(column)) {
            if (value == null) {
                if (column.isNotNull())
                    throw new IllegalArgumentException("Cannot set null to non-null column: " + column.getName());
                setDirect(column, null);
                return;
            }

            // check for serializer
            var ser = findSerializer(column, value, false);
            if (ser == null) {
                setDirect(column.typed(Object.class), value);
                return;
            }
            var serialized = ((Serializer<Object, ?>) ser).serialize(value);
            setDirect(column.typed(Object.class), serialized);

            // cache if enabled
            if (cacheEnabled) cache.putSecond(column, value);

            // push to database
            push(column, serialized);
        }
    }

    /**
     * Pushes a value to the database.
     *
     * @param col column
     * @param val value
     */
    private void push(Column<?> col, Object val) {
        synchronized (getLock(col)) {
            var r = (Runnable) () -> selector.apply(parent.update().setP(col, val)).update();
            if (pushLazy) parent.sql.executor.get().submit(() -> {
                try {
                    pendingOps.register();
                    r.run();
                } finally {
                    pendingOps.arriveAndDeregister();
                }
            });
            else r.run();
        }
    }

    @Override
    public void waitFor() throws InterruptedException {
        pendingOps.register();
        pendingOps.awaitAdvanceInterruptibly(pendingOps.arriveAndDeregister());
    }

    @Override
    public void delete() {
        synchronized (bouncer) {
            bouncer.pass();

            // aquire all locks to prevent modification while deleting
            acquireLocks(List.copyOf($locks.values()).iterator(), () -> {
                // wait for all pending operations to finish, non-interruptibly
                pendingOps.register();
                pendingOps.arriveAndAwaitAdvance();

                // clear and delete
                $locks.clear();
                cache.clear();
                selector.apply(parent.delete()).update();
            });

            bouncer.block();
        }
    }

    /**
     * Acquires and locks on to all locks in the given iterator, then executes the given task.
     *
     * @param iter The iterator
     * @param task The task
     */
    private <X extends Exception> void acquireLocks(Iterator<?> iter, Task<X> task) throws X {
        if (!iter.hasNext()) {
            task.execute();
            return;
        }
        synchronized (iter.next()) {
            acquireLocks(iter, task);
        }
    }
}
