package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.io.transform.WritableTransformChannel;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;

import java.io.Flushable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

import static cc.fluse.ulib.core.util.Semantics.mayfail;

/**
 * A write pipeline that writes to a sink.
 */
public class SinkTransformChannel extends AbstractTransformChannel implements WritableTransformChannel {

    private final WritableByteChannel sink;

    public SinkTransformChannel(WritableByteChannel sink) {
        this.sink = sink;
    }

    @Override
    public int write(@NotNull ByteBuffer src) throws IOException {
        // no bouncer pass as it's already done by super#push()
        int i = super.push(src);
        flush();
        return i;
    }

    @Override
    public void flush() throws IOException {
        super.bouncer.pass();

        // transfer to sink
        try (var e = Pool.BYTE_BUFFER_DEFAULT_POOL.obtain()) {
            var buf = e.get();
            while ((super.pull(buf.clear())) > 0) {
                sink.write(buf.flip());
            }
        }

        // flush
        if (sink instanceof Flushable f) f.flush();
    }

    @Override
    public void close() throws IOException {
        if (!isOpen()) return;

        if (sink.isOpen()) {
            // complete if applicable
            if (!isCompleted()) mayfail(IOTransformException.class, super::complete);
            flush();
            sink.close();
        }

        super.close();
    }
}
