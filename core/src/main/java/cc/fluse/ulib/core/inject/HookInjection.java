package cc.fluse.ulib.core.inject;

import cc.fluse.ulib.core.function.BiParamTask;
import cc.fluse.ulib.core.impl.UnsafeOperations;
import cc.fluse.ulib.core.impl.inject.InjectionConfiguration;
import cc.fluse.ulib.core.impl.inject.InjectionManager;
import cc.fluse.ulib.core.impl.inject.InjectionSupport;
import cc.fluse.ulib.core.impl.reflect.ClsRef;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * A hook injection builder.
 * <p>
 * Example:
 * <pre>{@code
 *  Spec headSpec = InjectUtil.createHookingSpec(HookPoint.HEAD);
 *  HookInjection injection = new HookInjection(targetClazz)
 *      .<ReturnType>addHook("someMethod", spec, (params, cb) -> {
 *          // someMethod(...) was called!
 *      })
 *      .addHook(hookClazz.getMethod("hook_someOtherMethod"), null);
 *  boolean success = !injection.inject().hasCaught();
 * }</pre>
 *
 * @see InjectUtil#createHookingSpec(HookPoint)
 */
public class HookInjection {

    private final ClsRef target;
    private final Map<ClsRef, InjectionConfiguration> instructions = new HashMap<>();
    private final Bouncer<IllegalStateException> bouncer = Bouncer.of(() -> new IllegalStateException("object is locked"));

    /**
     * Constructs a new builder.
     *
     * @param target the global target
     */
    public HookInjection(@NotNull Class<?> target) {
        this.target = new ClsRef(target);
    }

    /**
     * Constructs a new builder.
     *
     * @param jvmClassName the global target, given as the jvm class name
     */
    public HookInjection(@NotNull String jvmClassName) {
        this.target = new ClsRef(jvmClassName);
    }

    /**
     * Constructs a new builder without a global target.
     */
    public HookInjection() {
        this.target = null;
    }

    /**
     * Adds the specified hook to the builder. The presence of a global target is assumed.
     *
     * @param methodDescriptor the target method JNI descriptor
     * @param spec             the hook specification
     * @param call             the callable object
     * @param <R>              the return type of the target method
     * @return this
     * @throws NullPointerException if no global target is present
     * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/types.html" target="_blank">JNI Types</a>
     */
    @NotNull
    @Contract("_, _, _ -> this")
    public synchronized <R> HookInjection addHook(@NotNull String methodDescriptor, @NotNull Spec spec,
                                                  @NotNull BiParamTask<? super Object[], ? super Callback<R>, ?> call) {
        bouncer.pass();
        var cl = Objects.requireNonNull(target, "No global target declared");
        instructions.computeIfAbsent(target, InjectionConfiguration::new)
                .with(methodDescriptor, spec, call);
        return this;
    }

    /**
     * Adds the specified hook to the builder.
     *
     * @param jvmCnTarget      the jvm name representation of the target class
     * @param methodDescriptor the target method JNI descriptor
     * @param spec             the hook specification
     * @param call             the callable object
     * @param <R>              the return type of the target method
     * @return this
     * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/types.html" target="_blank">JNI Types</a>
     */
    @NotNull
    @Contract("_, _, _, _ -> this")
    public synchronized <R> HookInjection addHook(@NotNull String jvmCnTarget, @NotNull String methodDescriptor, @NotNull Spec spec,
                                                  @NotNull BiParamTask<? super Object[], ? super Callback<R>, ?> call) {
        bouncer.pass();
        instructions.computeIfAbsent(new ClsRef(jvmCnTarget), InjectionConfiguration::new)
                    .with(methodDescriptor, spec, call);
        return this;
    }

    /**
     * Adds the specified hook to the builder.
     *
     * @param target           the target class
     * @param methodDescriptor the target method JNI descriptor
     * @param spec             the hook specification
     * @param call             the callable object
     * @param <R>              the return type of the target method
     * @return this
     * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/types.html" target="_blank">JNI Types</a>
     */
    @NotNull
    @Contract("_, _, _, _ -> this")
    public synchronized <R> HookInjection addHook(@NotNull Class<?> target, @NotNull String methodDescriptor, @NotNull Spec spec,
                                                  @NotNull BiParamTask<? super Object[], ? super Callback<R>, ?> call) {
        bouncer.pass();
        instructions.computeIfAbsent(new ClsRef(target), InjectionConfiguration::new)
                    .with(methodDescriptor, spec, call);
        return this;
    }

    /**
     * Adds the specified hook to the builder.
     *
     * @param target the target method that is to be injected
     * @param spec   the hook specification
     * @param call   the callable object
     * @param <R>    the return type of the target method
     * @return this
     */
    @NotNull
    @Contract("_, _, _ -> this")
    public synchronized <R> HookInjection addHook(@NotNull Method target, @NotNull Spec spec,
                                                  @NotNull BiParamTask<? super Object[], ? super Callback<R>, ?> call) {
        bouncer.pass();
        instructions.computeIfAbsent(new ClsRef(target.getDeclaringClass()), InjectionConfiguration::new)
                    .with(InjectionSupport.getSignature(target), spec, call);
        return this;
    }

    /**
     * Adds a hook method (as specified by {@link Hook}) to the builder.
     *
     * @param hook       the hook method
     * @param hookInvoke the method invoke instance ({@code null} for static methods)
     * @return this
     * @throws IllegalArgumentException if the hook method is not valid
     */
    @NotNull
    @Contract("_, _ -> this")
    public synchronized HookInjection addHook(@NotNull Method hook, @Nullable Object hookInvoke) {
        bouncer.pass();
        if (!hook.isAnnotationPresent(Hook.class))
            throw new IllegalArgumentException("Hook annotation not found on " + hook);
        InjectionSupport.checkInvoke(hook, hookInvoke);

        Hook anno = hook.getAnnotation(Hook.class);

        var caller = ReflectUtil.walkStack(st -> st
                .map(StackWalker.StackFrame::getDeclaringClass)
                .dropWhile(clazz -> clazz == HookInjection.class)
                .findFirst()
                .orElseThrow());

        var target = ReflectUtil.tryWithLoaders(l -> InjectionSupport.findTargetClass(anno, hook.getDeclaringClass(), l),
                                                caller.getClassLoader()).orElseThrow();
        var descriptor = InjectionSupport.resolveSignature(anno, target);
        var call = InjectionSupport.buildCall(hook, hookInvoke);

        return addHook(target, descriptor, anno.spec(), call);
    }


    /**
     * Adds a direct (manual) hook to the builder.
     *
     * @param hook       the hook method
     * @param hookInvoke the method invoke instance ({@code null} for static methods)
     * @param target     the target method
     * @param spec       the hook specification
     * @return this
     */
    @NotNull
    @Contract("_, _, _, _ -> this")
    public synchronized HookInjection addHook(@NotNull Method hook, @Nullable Object hookInvoke, @NotNull Method target, @NotNull Spec spec) {
        bouncer.pass();
        InjectionSupport.checkInvoke(hook, hookInvoke);

        var targetClazz = target.getDeclaringClass();
        var descriptor = InjectionSupport.getSignature(target);
        var call = InjectionSupport.buildCall(hook, hookInvoke);

        return addHook(targetClazz, descriptor, spec, call);
    }

    /**
     * Searches the given class for hookable methods and adds the hooks to the builder.
     *
     * @param hook   the class to search for hooks in
     * @param invoke the invoking instance
     * @return this
     */
    @NotNull
    @Contract("_, _ -> this")
    public synchronized HookInjection addHook(@NotNull Class<?> hook, @Nullable Object invoke) {
        var mts = Arrays.stream(hook.getMethods())
                .filter(method -> method.isAnnotationPresent(Hook.class))
                .filter(method -> (invoke == null) == Modifier.isStatic(method.getModifiers()))
                .toList();

        // not using #forEach in stream to preserve caller finding
        for (Method mt : mts) {
            addHook(mt, invoke);
        }

        return this;
    }

    /**
     * Performs the final code injection. May only be called once.
     *
     * @return pairs of the jvm target class representations and the respective transform results
     */
    @Unmodifiable
    @NotNull
    public synchronized Map<String, Optional<? extends Exception>> injectNow() {
        bouncer.pass();
        UnsafeOperations.unsafeAccess("Hook Injection");
        return injectNow_();
    }

    private synchronized Map<String, Optional<? extends Exception>> injectNow_() {
        bouncer.block();
        // do as atomic operation
        synchronized (InjectionManager.getInstance()) {
            var pending = InjectionManager.getInstance().injectionsJoin(instructions);
            try {
                return InjectionManager.getInstance().transformPending(ref -> pending.containsKey(ref.getInternalName()));
            } catch (UnmodifiableClassException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Performs the final code injection and flattens the result into a single expect object. May only be called once.
     */
    public synchronized void injectNowFlat() throws Exception {
        // inject (will also check bouncer and unsafe access)
        var res = injectNow();

        // *flatten result*
        var exceptions = res.values().stream()
                            .flatMap(Optional::stream)
                            .toList();
        Exception ex = null;
        for (Exception exception : exceptions) {
            if (ex == null) {
                ex = exception;
                continue;
            }
            ex.addSuppressed(exception);
        }

        if (ex != null) {
            throw ex;
        }
    }

    /**
     * Adds the injection configuration, but does not perform the injection.
     *
     * @return pairs of the jvm target class representations and the respective future object linked to the transform
     * @see InjectUtil#performPendingInjections()
     */
    @NotNull
    @Unmodifiable
    public synchronized Map<String, CompletableFuture<Void>> queueInjection() {
        bouncer.pass();
        UnsafeOperations.unsafeAccess("Hook Injection");
        bouncer.block();
        return InjectionManager.getInstance().injectionsJoin(instructions);
    }

}
