package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.io.datachannel.ReadableDataChannelDelegate;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Readable;
import cc.fluse.ulib.core.io.VarDataFaucet;
import org.jetbrains.annotations.NotNull;

import java.io.DataInput;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;

/**
 * A {@link ReadableByteChannel} that also implements {@link DataInput} and {@link Readable}.
 * <p>
 * Calls to read methods on this channel will be forwarded to the underlying channel, and calls open channel methods on
 * {@link Readable} will always return the same underlying channel.
 */
public interface ReadableDataChannel extends ReadableByteChannel, VarDataFaucet, Readable {

    /**
     * Wraps a {@link ReadableByteChannel} in a {@link ReadableDataChannel}. If the channel is already a
     * {@link ReadableDataChannel}, it is returned as-is. Otherwise, a new {@link ReadableDataChannel} is created that
     * delegates to the given channel.
     * <p>
     * The returned object is not guaranteed to be thread-safe.
     *
     * @param channel the channel to wrap
     * @return a {@link ReadableDataChannel} that delegates to the given channel
     */
    static @NotNull ReadableDataChannel wrap(@NotNull ReadableByteChannel channel) {
        return channel instanceof ReadableDataChannel dch ? dch : new ReadableDataChannelDelegate(channel);
    }

    /**
     * Returns a new isolated channel that reads from this channel.
     *
     * @return the channel
     * @see IOUtil#isolate(ReadableByteChannel)
     */
    @Override
    @NotNull
    default ReadableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    @Override
    @NotNull
    default ReadableDataChannel channelRead() throws IOException {
        return channel();
    }
}
