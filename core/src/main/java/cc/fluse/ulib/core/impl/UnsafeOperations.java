package cc.fluse.ulib.core.impl;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.*;
import java.security.cert.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public final class UnsafeOperations {

    private static final CertPathValidator VALIDATOR;
    private static final CertPathParameters VALIDATOR_PARAMS;

    static {
        try {
            VALIDATOR = CertPathValidator.getInstance("PKIX");
            var rootCert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(UnsafeOperations.class.getResourceAsStream("/META-INF/root-ca.cer"));
            rootCert.checkValidity();
            var params = new PKIXParameters(Collections.singleton(new TrustAnchor(rootCert, null)));
            params.setRevocationEnabled(false);
            VALIDATOR_PARAMS = params;
        } catch (CertificateException e) {
            throw new SecurityException("root ca cert invalid", e);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new UndefinedStateError(e);
        }
    }

    public static boolean allowed() {
        return Internal.isUnsafeOperations();
    }

    public static boolean comply(boolean bool, String module, String exception, String warning) {
        return comply(bool, module, () -> exception, () -> warning);
    }

    public static boolean comply(boolean bool, String module, Supplier<String> exception, Supplier<String> warning) {
        if (bool) {
            if (allowed()) {
                System.err.printf("(%s) %s (unsafe operations are allowed)%n", module, warning.get());
            } else {
                throw new UnsafeOperationException(String.format("(%s) Cannot comply: %s (allow unsafe operations to bypass this)", module, exception.get()));
            }
        }
        return bool;
    }

    public static void unsafeAccess(@Nullable String what) {
        var checkingClass = new AtomicReference<Class<?>>();
        var accessor = ReflectUtil.walkStack(st -> st
                .map(StackWalker.StackFrame::getDeclaringClass)
                // drop checking class
                .dropWhile(cl ->
                                   // drop at initial update (since #compareAndSet returns true if successful)
                                   checkingClass.compareAndSet(null, cl)
                                   // drop while same class
                                   || checkingClass.get() == cl)
                .findFirst()
                .orElseThrow()
        );

        // ulib classes are allowed regardless of sign state
        if (Internal.isUlibClass(accessor)) {
            return;
        }

        // allow accessors with valid certificate
        var signers = accessor.getProtectionDomain().getCodeSource().getCodeSigners();
        if (signers != null && Arrays.stream(signers)
                                     .filter(Objects::nonNull)
                                     .map(CodeSigner::getSignerCertPath)
                                     .anyMatch(UnsafeOperations::certsValid)) {
            return;
        }

        // unsafe
        var s = what == null ? "Unsafe Access" : "Unsafe Access: " + what;
        UnsafeOperations.comply(true, accessor.getName(), s, s);
    }

    private static boolean certsValid(@NotNull CertPath path) {
        try {
            VALIDATOR.validate(path, VALIDATOR_PARAMS);
            return true;
        } catch (CertPathValidatorException e) {
            return false;
        } catch (InvalidAlgorithmParameterException e) {
            throw new Error(e);
        }
    }
}
