package cc.fluse.ulib.core.http.jsonrpc;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface RpcObject {
    @NotNull
    RpcEndpoint getEndpoint();

    @NotNull
    Optional<String> getId();
}
