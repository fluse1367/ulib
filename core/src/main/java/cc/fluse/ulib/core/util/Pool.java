package cc.fluse.ulib.core.util;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.SynchronizedPool;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A pool of elements.
 *
 * @param <T> the type of elements in the pool
 * @param <X> Type of exception that can be thrown by the {@link #obtain()} method.
 */
public interface Pool<T, X extends Exception> extends Closeable {

    /**
     * A common pool of {@link ByteBuffer}s with a capacity of 1 byte. Releasing a buffer will restore its initial
     * state.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<ByteBuffer, RuntimeException> BYTE_BUFFER_SINGLE_POOL = synchronizedPool(-1, () -> Internal.bufalloc(1), ByteBuffer::clear, null, null, null);

    /**
     * A common pool of {@link ByteBuffer}s with a capacity of 8 bytes. Releasing a buffer will restore its initial
     * state.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<ByteBuffer, RuntimeException> BYTE_BUFFER_8_POOL = synchronizedPool(-1, () -> Internal.bufalloc(8), ByteBuffer::clear, null, null, null);

    /**
     * A common pool of {@link ByteBuffer}s with ulib's default buffer size. Releasing a buffer will restore its initial
     * state.
     * <p>
     * The default buffer size can be manually configured by the user and is 1024 bytes by default. It is guaranteed
     * that the default buffer size is at least 1 byte.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<ByteBuffer, RuntimeException> BYTE_BUFFER_DEFAULT_POOL = synchronizedPool(-1, Internal::bufalloc, ByteBuffer::clear, null, null, null);

    /**
     * This variable points to a pool of {@link ByteBuffer}s with at least a capacity of 8 bytes.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<ByteBuffer, RuntimeException> BYTE_BUFFER_POOL = Internal.getDefaultBufferCapacity() > 8 ? BYTE_BUFFER_DEFAULT_POOL : BYTE_BUFFER_8_POOL;

    /**
     * A common pool of {@code byte[]}s with a length of 1 byte. Releasing a buffer will restore its initial state.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<byte[], RuntimeException> BYTE_ARRAY_SINGLE_POOL = synchronizedPool(-1, () -> new byte[1], buf -> buf[0] = 0, null, null, null);

    /**
     * A common pool of {@code byte[]}s with a length of 8 bytes. Releasing a buffer will restore its initial state.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<byte[], RuntimeException> BYTE_ARRAY_8_POOL = synchronizedPool(-1, () -> new byte[8], buf -> Arrays.fill(buf, (byte) 0), null, null, null);

    /**
     * A common pool of {@code byte[]}s with ulib's default buffer size. Releasing a buffer will restore its initial
     * state.
     * <p>
     * The default buffer size can be manually configured by the user and is 1024 bytes by default. It is guaranteed
     * that the default buffer size is at least 1 byte.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<byte[], RuntimeException> BYTE_ARRAY_DEFAULT_POOL = synchronizedPool(-1, () -> new byte[Internal.getDefaultBufferCapacity()], buf -> Arrays.fill(buf, (byte) 0), null, null, null);

    /**
     * This variable points to a pool of {@code byte[]}s with at least a length of 8 bytes.
     * <p>
     * This pool is thread-safe.
     */
    @NotNull Pool<byte[], RuntimeException> BYTE_ARRAY_POOL = Internal.getDefaultBufferCapacity() > 8 ? BYTE_ARRAY_DEFAULT_POOL : BYTE_ARRAY_8_POOL;

    /**
     * Creates a new pool with the given maximum size, factory, restorer, health checker and cleaner.
     * <p>
     * Health checks are conducted periodically. If an element fails a health check, it is removed from the pool and
     * cleaned up. Health checks are also conducted when an element gets obtained from the pool. Health checks for a
     * specific element are disabled while this element is in use.
     * <p>
     * The new pool is thread-safe.
     *
     * @param maxSize             the maximum number of elements in the pool, or {@code -1} for no limit. May not be
     *                            {@code 0}.
     * @param factory             a factory for creating new elements
     * @param restorer            a function for restoring elements to their initial state when they are released
     * @param healthChecker       a function for checking the health of elements, returning {@code true} if the element
     *                            is healthy, {@code false} otherwise. If {@code null}, no health checks are performed.
     * @param healthCheckInterval the interval between health checks. Must be non-{@code null} if {@code healthChecker}
     *                            is non-{@code null}. Must be {@code null} if {@code healthChecker} is {@code null}.
     * @param cleaner             a function for cleaning up elements when they are removed from the pool, either
     *                            because of a failed health check or because the pool is being closed. May be
     *                            {@code null} to disable cleanup. The first parameter is {@code true} if the element is
     *                            cleaned up because of a failed health check, {@code false} otherwise. The second
     *                            parameter is the element to be cleaned up.
     * @param <T>                 the type of elements in the pool
     * @return the new pool
     */
    static <T, X extends Exception> @NotNull Pool<T, X> synchronizedPool(
            int maxSize,
            @NotNull Func<? extends @Nullable T, ? extends X> factory,
            @NotNull Consumer<? super @NotNull T> restorer,
            @Nullable Predicate<? super @NotNull T> healthChecker,
            @Nullable Duration healthCheckInterval,
            @Nullable BiConsumer<? super @NotNull Boolean, ? super @NotNull T> cleaner) {
        return new SynchronizedPool<>(maxSize, factory, restorer, healthChecker, healthCheckInterval, cleaner);
    }


    /**
     * Container for a pooled element. The element is automatically released when the container is closed.
     *
     * @param <T> the type of element
     */
    interface PoolElement<T> extends AutoCloseable {

        /**
         * Returns the pooled element. The element is only valid until the next call to {@link #release()} or
         * {@link #close()}.
         *
         * @return the element
         */
        @NotNull T get();

        /**
         * Releases the element back into the pool. The element is no longer valid after this call.
         */
        void release();

        /**
         * Releases the element back into the pool. The element is no longer valid after this call.
         * <p>
         * This method is equivalent to {@link #release()}.
         */
        @Override
        void close();
    }

    /**
     * Returns the maximum size of the pool.
     * <p>
     * Contrary to the initial passed in value, this value is guaranteed to be non-negative, and will be
     * {@link Integer#MAX_VALUE} if the initial value was {@code -1}.
     *
     * @return the maximum size of the pool
     */
    int getMaxSize();

    /**
     * Returns the current total number of elements managed by the pool. This includes elements that are currently in
     * use.
     *
     * @return the current total number of elements managed by the pool
     */
    int getSize();

    /**
     * Returns the number of available elements in the pool. This does not include elements that are currently in use
     * nor does it include elements that could theoretically be created by the factory.
     *
     * @return the number of available elements in the pool
     */
    int getAvailable();

    /**
     * Returns the total number of elements that are theoretically currently available. This does not include elements
     * that are currently in use but does include elements that could theoretically be created by the factory.
     *
     * @return the total number of elements that are theoretically currently available
     */
    int getTotalAvailable();

    /**
     * Returns a pooled element. The element must be released back into the pool when it is no longer needed.
     * <p>
     * The returned element is only valid until the next call to {@link PoolElement#release()} or
     * {@link PoolElement#close()}.
     * <p>
     * <b>Note:</b> This method will always wait for pending health checks in case the pool is empty.
     *
     * @param timeout the maximum time to wait for an element to be released in milliseconds. {@code 0} for waiting
     *                indefinitely. {@code -1} for not waiting at all.
     * @return the element
     * @throws InterruptedException   if the thread is interrupted while waiting for an element to be released
     * @throws NoSuchElementException if the pool is empty and no new elements can be created or waiting for an element
     *                                to be released is interrupted or times out
     */
    @NotNull PoolElement<T> obtain(long timeout) throws InterruptedException, NoSuchElementException, X;

    /**
     * Returns a pooled element. The element must be released back into the pool when it is no longer needed.
     * <p>
     * This method is equivalent to {@link #obtain(long)} with a timeout of {@code -1}. This means that this method will
     * never wait for an element to be released and will throw a {@link NoSuchElementException} if the pool is empty.
     * <p>
     * <b>Note:</b> This method will always wait for pending health checks in case the pool is empty.
     *
     * @return the element
     * @throws NoSuchElementException if the pool is empty and no new elements can be created
     */
    default @NotNull PoolElement<T> obtain() throws NoSuchElementException, X {
        try {
            return obtain(-1);
        } catch (InterruptedException e) {
            throw new NoSuchElementException();
        }
    }

    /**
     * Closes the pool. This prevents the pool from creating new elements and obtaining ones. This method will block
     * until all elements are released.
     *
     * @throws IOException if an I/O error occurs while closing the pool
     */
    @Override
    default void close() throws IOException {
        close(t -> {});
    }

    /**
     * Closes the pool. This prevents the pool from creating new elements and obtaining ones. Also removes any internal
     * references to elements in the pool, to allow them to be gc'd.
     * <p>
     * This method will block until all elements are released. The given task will be executed for each element in the
     * pool. The first exception thrown by the task will be rethrown by this method, other exceptions will be
     * suppressed.
     *
     * @param task the task to execute for each element in the pool
     * @throws IOException if an I/O error occurs while closing the pool
     * @throws XX          if the task throws an exception
     */
    <XX extends Exception> void close(@Nullable ParamTask<? super @NotNull T, X> task) throws IOException, XX;
}
