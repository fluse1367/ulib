package cc.fluse.ulib.core.util;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

/**
 * A utility class for handling expected and unexpected exceptions. Useful if known that a certain exception will not be
 * thrown, or if it is known that a certain exception could be thrown.
 */
@BypassAnnotationEnforcement
public final class Semantics {
    /**
     * Runs a given function with the assumption that it will not throw a certain type of exception. If it does, an
     * {@link UndefinedStateError} will be thrown instead. Any other exceptions will be thrown as-is.
     *
     * @param unexpected the type of exception that is assumed to not be thrown
     * @param func       the function to run
     * @return the result of the function
     */
    @SneakyThrows
    public static <T, X extends Exception> T nofail(@NotNull Class<? extends X> unexpected, @NotNull Func<T, X> func) {
        try {
            return func.execute();
        } catch (Exception e) {
            if (unexpected.isInstance(e))
                throw new UndefinedStateError("Exception of type %s was not expected".formatted(unexpected.getSimpleName()), e);
            throw e;
        }
    }

    /**
     * Runs a given function with the assumption that it will only throw a certain type of exception. If it throws any
     * other type of exception, an {@link UndefinedStateError} will be thrown instead.
     *
     * @param expected the type of exception that is expected to be thrown
     * @param func     the function to run
     * @return the result of the function
     * @throws X if the function throws an exception that is not of the expected type
     */
    public static <T, X extends Exception> T mayfail(@NotNull Class<? extends X> expected, @NotNull Func<? extends T, ?> func) throws X {
        try {
            return func.execute();
        } catch (Exception e) {
            if (expected.isInstance(e)) throw expected.cast(e);
            throw new UndefinedStateError("Exception of type %s was not expected".formatted(e.getClass().getSimpleName()), e);
        }
    }

    /**
     * Runs a given task with the assumption that it will not throw a certain type of exception. If it does, an
     * {@link UndefinedStateError} will be thrown instead. Any other exceptions will be thrown as-is.
     *
     * @param unexpected the type of exception that is assumed to not be thrown
     * @param task       the task to run
     */
    @SneakyThrows
    public static <X extends Exception> void nofail(@NotNull Class<? extends X> unexpected, @NotNull Task<X> task) {
        try {
            task.execute();
        } catch (Exception e) {
            if (unexpected.isInstance(e))
                throw new UndefinedStateError("Exception of type %s was not expected".formatted(unexpected.getSimpleName()), e);
            throw e;
        }
    }

    /**
     * Runs a given task with the assumption that it will only throw a certain type of exception. If it throws any other
     * type of exception, an {@link UndefinedStateError} will be thrown instead.
     *
     * @param expected the type of exception that is expected to be thrown
     * @param task     the task to run
     * @throws X the exception thrown by the task
     */
    public static <X extends Exception> void mayfail(@NotNull Class<? extends X> expected, @NotNull Task<?> task) throws X {
        try {
            task.execute();
        } catch (Exception e) {
            if (expected.isInstance(e)) throw expected.cast(e);
            throw new UndefinedStateError(e);
        }
    }

    /**
     * Runs a given function with the assumption that it will not throw any exceptions. If it does, an
     * {@link UndefinedStateError} will be thrown instead.
     *
     * @param func the function to run
     * @return the result of the function
     */
    public static <T> T nofail(@NotNull Func<T, ?> func) {
        try {
            return func.execute();
        } catch (Exception e) {
            throw new UndefinedStateError(e);
        }
    }

    /**
     * Runs a given task with the assumption that it will not throw any exceptions. If it does, an
     * {@link UndefinedStateError} will be thrown instead.
     *
     * @param task the task to run
     */
    public static void nofail(@NotNull Task<?> task) {
        try {
            task.execute();
        } catch (Exception e) {
            throw new UndefinedStateError(e);
        }
    }
}
