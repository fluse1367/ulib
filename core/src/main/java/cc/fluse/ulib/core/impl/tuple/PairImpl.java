package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.Pair;

public class PairImpl<T, U> extends TupleImpl<Object> implements Pair<T, U> {

    public PairImpl(T t, U u) {
        super(t, u);
    }

    protected PairImpl(Object... elements) {
        super(elements);
    }

    @Override
    public T getFirst() {
        return getI(0);
    }

    @Override
    public U getSecond() {
        return getI(1);
    }
}
