package cc.fluse.ulib.core.impl.database.sql;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.ColumnBuilder;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.dependencies.Dependencies;
import cc.fluse.ulib.core.dependencies.Repository;
import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.LazyValue;
import cc.fluse.ulib.core.util.Pool;
import cc.fluse.ulib.core.util.Pool.PoolElement;
import cc.fluse.ulib.core.util.Semantics;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.*;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cc.fluse.ulib.core.io.IOUtil.op;

public abstract class AbstractSqlDatabase implements cc.fluse.ulib.core.database.sql.SqlDatabase {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface DBImpl {
        String[] names();

        String protocol();

        String driverCoords();

        boolean supportsConnectionPooling() default false;

        String quoteIdentifierFormat() default "`%s`";

        String quoteFormat() default "'%s'";
    }

    private static final Collection<ClassLoader> drivers = new LinkedList<>();

    public static Stream<ResultSet> stream(ResultSet rs) {
        return Stream.iterate(rs, res -> Expect.compute(ResultSet::next, res).orElse(false), UnaryOperator.identity())
                // skip first iteration to force ResultSet#next, so an empty ResultSet won't throw an exception
                .skip(1);
    }

    private final DBImpl meta;

    private final String url;
    private final Properties info;
    private final Map<String, Table> tables = new LinkedHashMap<>();
    public final LazyValue<ExecutorService> executor = LazyValue.of(() -> Executors.newCachedThreadPool());

    private final Pool<Connection, SQLException> connectionPool;

    private AbstractSqlDatabase(Connection connection, String url, Properties info, int maxPoolSize) {
        this.meta = UndefinedStateError.ensureNotNull(getClass().getAnnotation(DBImpl.class), "Missing metadata");

        Func<Connection, SQLException> factory;
        if (connection != null) {
            factory = () -> connection;
            this.url = null;
            this.info = null;
        } else {
            this.url = url;
            this.info = info;
            factory = () -> {
                try {
                    return DriverManager.getConnection(url, info);
                } catch (SQLException e) {
                    // only continue if error is driver missing
                    if (!e.getSQLState().equals("08001")) throw new SQLException("Failed to connect to database", e);

                    // try with own class loaders
                    final var ctxCl = Thread.currentThread().getContextClassLoader();
                    try {
                        Connection conn;
                        for (var cl : drivers) {
                            if ((conn = tryConnect(cl)) != null) return conn;
                        }
                        // no previous driver worked, try loading a new one
                        if ((conn = tryConnect(loadDriver())) != null) return conn;
                        throw new SQLException("Driver failure");

                    } catch (SQLException ex) {
                        throw new SQLException("Failed to connect to database", ex);
                    } finally {
                        Thread.currentThread().setContextClassLoader(ctxCl);
                    }
                }
            };
        }

        this.connectionPool = Pool.synchronizedPool(maxPoolSize, factory, c -> {},
                conn -> Expect.compute(() -> !conn.isClosed() && conn.isValid(5)).orElse(false),
                Duration.ofMinutes(1),
                (fail, conn) -> Expect.compute(conn::close)
        );
    }

    protected AbstractSqlDatabase(Connection connection) {
        this(connection, null, null, 1);
        Expect.compute(this::initExistingTables).rethrowRE();
    }

    protected AbstractSqlDatabase(String url, Properties info, int maxPoolSize) {
        this(null, url, info, maxPoolSize == 0 ? 1 : maxPoolSize);
    }

    @SneakyThrows
    @Override
    public boolean isConnected() {
        return connectionPool.getSize() > 0;
    }

    @Override
    public void connect() throws IOException {
        if (isConnected()) throw new IllegalStateException("Database already connected!");
        if (url == null) throw new IllegalArgumentException("Invalid database url: null");

        // obtain one connection to test if's is valid
        try {
            connectionPool.obtain().close();
        } catch (SQLException e) {
            throw new IOException(e);
        }

        // initialize tables
        try {
            initExistingTables();
        } catch (SQLException e) {
            throw new IOException(e);
        }
    }

    /**
     * @return {@code null} if no driver found, otherwise the connection
     * @throws SQLException if driver found but connection failed
     */
    private Connection tryConnect(ClassLoader loader) throws SQLException {
        Thread.currentThread().setContextClassLoader(loader);
        try {
            //noinspection deprecation
            return ReflectUtil.doPrivileged(() -> ReflectUtil.scall(Connection.class, DriverManager.class, "getConnection()",
                    Param.listOf(String.class, url, Properties.class, info, Class.class, /* caller = null*/ null)));
        } catch (ReflectiveOperationException e) {
            if (!(e.getCause() instanceof SQLException se)) throw new UndefinedStateError();

            // return null if no driver found
            if (se.getSQLState().equals("08001") && se.getClass() == SQLException.class) return null;

            throw se;
        }
    }

    @SneakyThrows
    @Override
    public void disconnect() throws IllegalStateException {
        if (!isConnected()) throw new IllegalStateException("Database not connected!");
        if (executor.isPresent()) {
            executor.get().shutdown();
            if (!executor.get().awaitTermination(60, TimeUnit.SECONDS)) {
                // TODO: handle timeout
            }
        }
        connectionPool.close();
    }

    @Override
    public @NotNull PoolElement<Connection> getConnection() throws IllegalStateException {
        try {
            return connectionPool.obtain();
        } catch (Exception e) {
            throw new IllegalStateException("Database not connected!", e);
        }
    }

    @Override
    public @NotNull Collection<Table> getTables() {
        return Collections.unmodifiableCollection(tables.values());
    }

    /**
     * Quotes the given string for use in SQL statements.
     *
     * @param obj        the string to quote
     * @param identifier whether the string is an identifier (e.g. table or column name) or a character constant (e.g. a
     *                   value)
     * @return the quoted string
     */
    public String quote(@Nullable Object obj, boolean identifier) {
        return identifier ? meta.quoteIdentifierFormat().formatted(obj) : meta.quoteFormat().formatted(obj);
    }

    @Override
    @NotNull
    public Optional<Table> getTable(@NotNull String name) {
        // attempt fetching tables if `name` does not occur in the map
        if (!tables.containsKey(name)) {
            // TODO: synchronize
            Expect.compute(this::initExistingTables).rethrowRE();
        }
        return Optional.ofNullable(tables.get(name));
    }

    private Table addTable(String name, Column<?>... columns) {
        if (tables.containsKey(name)) {
            throw new IllegalStateException(String.format("Table %s already added", name));
        }
        var table = newTable(name, List.of(columns));
        tables.put(name, table);
        return table;
    }

    /**
     * Loads all existing tables from the database into this definition.
     */
    private void initExistingTables() throws SQLException {
        try (var e = getConnection()) {
            var meta = e.get().getMetaData();
            try (var res = getTablesInfo(meta)) {
                while (res.next()) {
                    var tName = res.getString("TABLE_NAME");
                    tables.putIfAbsent(tName, newTable(tName, fetchColumns(meta,
                            res.getString("TABLE_CAT"),
                            res.getString("TABLE_SCHEM"),
                            tName)));
                }
            }
        }

    }

    /**
     * Queries the database for all table definitions.
     *
     * @param meta the database metadata
     * @return the result set containing the table definitions
     */
    protected ResultSet getTablesInfo(DatabaseMetaData meta) throws SQLException {
        return meta.getTables(null, null, "%", null);
    }

    // helper method
    @SneakyThrows
    private Collection<Column<?>> fetchColumns(DatabaseMetaData meta, String tableCatalog, String tableSchema, String tableName) {
        // determine primary key
        var primaryCol = op(() -> meta.getPrimaryKeys(tableCatalog, tableSchema, tableName),
                res -> res.next() ? res.getString("COLUMN_NAME") : null, true);

        // determine unique indices
        var unique = op(() -> meta.getIndexInfo(tableCatalog, tableSchema, tableName, false, false), rs -> stream(rs)
                .map(res -> Semantics.nofail(() -> Tuple.of(res.getString("COLUMN_NAME"), !res.getBoolean("NON_UNIQUE"))))
                .filter(Pair::getSecond) // filter unique indices
                .map(Pair::getFirst)
                .toList(), true);

        // fetch columns
        return op(() -> meta.getColumns(tableCatalog, tableSchema, tableName, null), rs -> stream(rs)
                .map(res -> Semantics.nofail(() -> {
                    var colName = res.getString("COLUMN_NAME");
                    DataType type;
                    try {
                        type = DataType.valueOf(res.getString("TYPE_NAME"));
                    } catch (IllegalArgumentException e) {
                        // TODO: skip table instead?
                        type = DataType.$0;
                    }

                    //noinspection unchecked,rawtypes
                    return (Column<?>) new ColumnImpl(type.getClazz(), colName, type,
                            res.getInt("NULLABLE") == ResultSetMetaData.columnNoNulls,
                            res.getString("IS_AUTOINCREMENT").equals("YES"),
                            colName.equals(primaryCol) ? Column.Index.PRIMARY : unique.contains(colName) ? Column.Index.UNIQUE : null,
                            res.getInt("COLUMN_SIZE"),
                            res.getObject("COLUMN_DEF"),
                            null);
                }))
                .collect(Collectors.toUnmodifiableSet()), true);
    }

    /**
     * Constructs a new table object specific to the database implementation.
     *
     * @param name    the name of the table
     * @param columns the columns of the table
     * @return the table
     */
    protected abstract Table newTable(String name, Collection<Column<?>> columns);

    /**
     * Loads a driver. Adds it to {@link #drivers} and returns the class loader on success.
     *
     * @return the class loader of the driver
     * @throws SQLException if driver could not be loaded
     */
    protected ClassLoader loadDriver() throws SQLException {
        // download driver
        var urls = Dependencies.resolve(meta.driverCoords(), Repository.mavenCentral())
                .map(p -> Expect.compute(() -> p.toUri().toURL()).orElseRethrowRE())
                .toArray(URL[]::new);

        var ucl = new URLClassLoader(urls);

        // load driver
        boolean result; // if at least one driver was loaded
        try {
            result = Collections.list(ucl.findResources("META-INF/services/java.sql.Driver")).stream()
                    // collect driver implementations
                    .flatMap(url -> {
                        try (var r = new BufferedReader(new InputStreamReader(url.openStream()))) {
                            return r.lines().toList().stream();
                        } catch (IOException e) {
                            return null;
                        }
                    }).filter(Objects::nonNull)
                    // load driver classes (will self-register)
                    .map(fqcn -> {
                        try {
                            return Class.forName(fqcn, true, ucl);
                        } catch (ClassNotFoundException ignored) {}
                        return null;
                    })
                    .anyMatch(Objects::nonNull);
        } catch (IOException e) {
            throw new SQLException("Failed to load driver", e);
        }

        if (!result) throw new SQLException("No driver loaded");

        drivers.add(ucl);

        return ucl;
    }

    @Override
    public @NotNull Table addTable(@NotNull String name, @NotNull Column<?> column, Column<?>... columns) {
        var cols = new ArrayList<Column<?>>(columns.length + 1);
        cols.add(column);
        cols.addAll(Arrays.asList(columns));
        return addTable(name, cols.toArray(new Column[0]));
    }

    @Override
    public @NotNull Table addTable(@NotNull String name, @NotNull ColumnBuilder<?> builder, ColumnBuilder<?>... builders) {
        List<Column<?>> columns = new ArrayList<>(builders.length);
        for (ColumnBuilder<?> cb : builders) {
            columns.add(cb.build());
        }
        return addTable(name, builder.build(), columns.toArray(new Column[0]));
    }
}
