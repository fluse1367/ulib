package cc.fluse.ulib.core.http.jsonrpc;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface RpcRequest extends RpcObject {
    @NotNull
    String getMethod();

    @NotNull
    Optional<String> getId();

    @NotNull
    Optional<Object> getParameters();
}
