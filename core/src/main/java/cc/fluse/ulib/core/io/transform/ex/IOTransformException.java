package cc.fluse.ulib.core.io.transform.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.experimental.StandardException;

import java.io.IOException;

/**
 * An exception indicating that the transform failed while processing data.
 */
@StandardException
@BypassAnnotationEnforcement
public sealed class IOTransformException extends IOException permits LackOfDataException, InputFormatException {

}
