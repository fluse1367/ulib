package cc.fluse.ulib.core.impl.inject;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.impl.Concurrent;
import cc.fluse.ulib.core.inject.*;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.ThreadedRecursionAccess;

import java.lang.StackWalker.StackFrame;
import java.net.URL;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public final class ClassLoaderDelegationHook {

    public static final Collection<ClassLoaderDelegationHook> HOOKS = Concurrent.newThreadsafeSet();

    // for injection
    private final Class<? extends ClassLoader> targetClazz;
    private final HookInjection injection;

    // marks methods that should be injected additionally to the usual methods
    // (method name) -> (parameter types) -> (mapping function to determine class name to resolve)
    private final Map<String, Map<Class<?>[], Function<Object[], Optional<String>>>> additionalHooks;

    // for hook execution
    private final ClassLoaderDelegation delegation;
    private final Predicate<ClassLoader> filterClassLoader; // check class loader delegated
    private final BiPredicate<Class<?>, String> filterLoadingRequest; // requesting class, requested name

    private final ThreadedRecursionAccess recursionAccess = new ThreadedRecursionAccess();

    public ClassLoaderDelegationHook(Class<? extends ClassLoader> targetClazz,
                                     Map<String, Map<Class<?>[], Function<Object[], Optional<String>>>> additional,
                                     ClassLoaderDelegation delegation,
                                     Predicate<ClassLoader> filterClassLoader,
                                     BiPredicate<Class<?>, String> filterLoadingRequest) {
        this.targetClazz = targetClazz;
        this.additionalHooks = Objects.requireNonNullElse(additional, Collections.emptyMap());
        this.injection = new HookInjection(targetClazz);

        this.delegation = delegation;
        this.filterClassLoader = filterClassLoader;
        this.filterLoadingRequest = filterLoadingRequest;
    }

    public void inject() throws Exception {
        // delegate class finding/loading
        var headSpec = InjectUtil.createHookingSpec(HookPoint.HEAD, null, (Integer[]) null);
        ReflectUtil.findUnderlyingMethod(targetClazz, "findClass", true, String.class)
                   .ifPresent(into -> injection.<Class<?>>addHook(into, headSpec,
                                                                  (p, c) -> hook_findClass((String) p[0], c)
                   ));
        ReflectUtil.findUnderlyingMethod(targetClazz, "findClass", true, String.class, String.class)
                   .ifPresent(into -> injection.<Class<?>>addHook(into, headSpec,
                                                                  (p, c) -> hook_findClass((String) p[0], (String) p[1], c)
                   ));
        ReflectUtil.findUnderlyingMethod(targetClazz, "loadClass", true, String.class, boolean.class)
                   .ifPresent(into -> injection.<Class<?>>addHook(into, headSpec,
                                                                  (p, c) -> hook_loadClass((String) p[0], (boolean) p[1], c)
                   ));

        // delegate any additional hooks
        additionalHooks.forEach((name, map) -> map.forEach((types, mapper) -> ReflectUtil
                .findUnderlyingMethod(targetClazz, name, true, types)
                .ifPresent(into -> injection.<Class<?>>addHook(into, headSpec,
                                                               (p, cb) -> mapper.apply(p).ifPresent(
                                                                       nameToResolve -> this.hook_findClass(nameToResolve, cb)
                                                               )
                ))
        ));

        // delegate resource finding
        ReflectUtil.findUnderlyingMethod(targetClazz, "findResource", true, String.class)
            .ifPresent(into -> injection.<URL>addHook(into, headSpec,
                (p, c) -> hook_findResource((String) p[0], c)
            ));

        ReflectUtil.findUnderlyingMethod(targetClazz, "findResource", true, String.class, String.class)
                .ifPresent(into -> injection.<URL>addHook(into, headSpec,
                        (p, c) -> hook_findResource((String) p[0], (String) p[1], c)
                ));
        ReflectUtil.findUnderlyingMethod(targetClazz, "findResources", true, String.class)
                .ifPresent(into -> injection.<Enumeration<URL>>addHook(into, headSpec,
                        (p, c) -> hook_findResources((String) p[0], c)
                ));

        injection.injectNowFlat();

        HOOKS.add(this);
    }

    // access check

    private Class<?> identifyClassLoadingRequestSource() {
        // walk through stack and find first class that is not a class loader anymore
        var frames = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                                .walk(Stream::toList);

        boolean walkingClassLoaderChain = false;
        for (StackFrame frame : frames) {
            var cl = frame.getDeclaringClass();
            boolean isClassLoader = ClassLoader.class.isAssignableFrom(cl);

            if (isClassLoader && !walkingClassLoaderChain) {
                walkingClassLoaderChain = true;
            } else if (walkingClassLoaderChain && !isClassLoader) {
                // found class that is nod a class loader
                return cl;
            }
        }
        throw new UndefinedStateError(); // a class loader cannot be the source of the class loading request
    }

    public boolean checkCl(Object source) {
        return source instanceof ClassLoader cl && filterClassLoader.test(cl);
    }

    private boolean checkClassRequest(Object source, String name) {
        return checkCl(source)
               && filterLoadingRequest.test(identifyClassLoadingRequestSource(), name);
    }

    // class lookup

    private void putClass(String methodPrefix, List<Param<?>> methodParams, Callback<Class<?>> cb, Supplier<Class<?>> delegate) {
        cb.self()
          // try parent
          .map(ClassLoader.class::cast)
          .map(ClassLoader::getParent)
          .map(parent -> Expect.compute(() -> ReflectUtil.doPrivileged(() -> ReflectUtil.icall(Class.class, parent, methodPrefix + "Class()", methodParams))).orElse(null))

          // try delegate
          .or(() -> Optional.ofNullable(delegate.get()))

          // put if success
          .ifPresent(cb::setReturnValue);
    }


    // - actual hooks -

    // class finding/loading hooks

    private void hook_findClass(String name, Callback<Class<?>> cb) {
        if (!checkClassRequest(cb.self().orElseThrow(), name)) return;

        recursionAccess.access("findClass:" + name, () ->
            putClass("find", Param.single(String.class, name), cb,
                () -> delegation.findClass(name))
        );
    }

    private void hook_findClass(String module, String name, Callback<Class<?>> cb) {
        if (!checkClassRequest(cb.self().orElseThrow(), name)) return;

        recursionAccess.access("findClass:" + module + ":" + name, () ->
            putClass("find", Param.listOf(String.class, module, String.class, name),
                cb, () -> delegation.findClass(module, name))
        );
    }

    private void hook_loadClass(String name, boolean resolve, Callback<Class<?>> cb) {
        if (!checkClassRequest(cb.self().orElseThrow(), name)) return;

        recursionAccess.access("loadClass:" + name + ":" + resolve, () ->
            putClass("load", Param.listOf(String.class, name, boolean.class, resolve),
                cb, () -> delegation.loadClass(name, resolve))
        );
    }

    // resource finding hooks

    private void hook_findResource(String name, Callback<URL> cb) {
        if (!checkCl(cb.self().orElseThrow())) return;

        recursionAccess.access("findResource:" + name, () ->
            Optional.ofNullable(delegation.findResource(name))
                .ifPresent(cb::setReturnValue)
        );
    }

    private void hook_findResource(String module, String name, Callback<URL> cb) {
        if (!checkCl(cb.self().orElseThrow())) return;

        recursionAccess.access("findResource:" + module + ":" + name, () ->
            Optional.ofNullable(delegation.findResource(module, name))
                .ifPresent(cb::setReturnValue)
        );
    }

    private void hook_findResources(String name, Callback<Enumeration<URL>> cb) {
        if (!checkCl(cb.self().orElseThrow())) return;

        recursionAccess.access("findResources:" + name, () ->
            Optional.ofNullable(delegation.findResources(name))
                .ifPresent(cb::setReturnValue)
        );
    }
}
