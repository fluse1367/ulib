package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.function.BiConsumer;

/**
 * A task that takes two parameters, does not return a result and may throw a throwable object.
 *
 * @apiNote only pass this task object as consumer if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface BiParamTask<T, U, X extends Exception> extends BiConsumer<T, U> {

    @Deprecated
    static <T, U, X extends Exception> BiParamTask<T, U, X> as(@NotNull BiParamTask<T, U, ?> func) {
        return func::accept;
    }

    void execute(T t, U u) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default void accept(T t, U u) {
        execute(t, u);
    }
}
