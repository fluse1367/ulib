package cc.fluse.ulib.core.cli;

import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.*;

import java.util.*;

/**
 * A class representing parsed (usually user supplied) options, their respective arguments and the overall parameters.
 */
public interface CliArgs {

    /**
     * @return the options
     */
    @NotNull
    @Unmodifiable
    Map<CliOption, Collection<String>> getOptions();

    /**
     * @return the parameters
     */
    @NotNull
    @Unmodifiable
    Collection<String> getParameters();

    /**
     * Determines if a certain option is present.
     *
     * @param opt the option object
     * @return {@code true} if the option is present, {@code false} otherwise
     */
    boolean hasOption(@NotNull CliOption opt);

    /**
     * Determines if a certain option is present.
     *
     * @param name the option's long name
     * @return {@code true} if the option is present, {@code false} otherwise
     */
    boolean hasOption(@NotNull String name);

    /**
     * Determines if a certain option is present.
     *
     * @param shortName one of the option's short names
     * @return {@code true} if the option is present, {@code false} otherwise
     */
    boolean hasOption(char shortName);

    /**
     * Obtains the option arguments.
     *
     * @param opt the option to obtain the arguments from
     * @return an optional wrapping the arguments, or an empty optional if no arguments are found
     */
    @NotNull
    @Unmodifiable
    Optional<Collection<String>> getOptionArguments(@NotNull CliOption opt);

    /**
     * Retrieves a single option argument and maps it.
     *
     * @param opt    the option
     * @param mapper the mapper, may return null on failure
     * @param i      the index (starting with 0) of the argument
     */
    default <T, X extends Exception>
    @NotNull Expect<T, X> getOptionArgument(@NotNull CliOption opt, @NotNull ParamFunc<? super @NotNull String, @Nullable T, X> mapper, int i) {
        return getOptionArguments(opt)
                // get i-th arg
                .map(coll -> {
                    if (coll instanceof List<String> li) return li.size() > i ? li.get(i) : null;
                    return coll.stream().skip(i).findFirst().orElse(null);
                })
                // map
                .map(str -> Expect.compute(mapper, str))
                .orElseGet(Expect::empty);
    }

    /**
     * Obtains the option arguments.
     *
     * @param name the option's long name
     * @return an optional wrapping the arguments, or an empty optional if no arguments are found
     */
    @NotNull
    @Unmodifiable
    Optional<Collection<String>> getOptionArguments(@NotNull String name);

    /**
     * Retrieves a single option argument and maps it.
     *
     * @param name   the option's long name
     * @param mapper the mapper, may return null on failure
     * @param i      the index (starting with 0) of the argument
     */
    <T, X extends Exception>
    @NotNull Expect<T, X> getOptionArgument(@NotNull String name, @NotNull ParamFunc<? super @NotNull String, @Nullable T, X> mapper, int i);

    /**
     * Obtains the option arguments.
     *
     * @param shortName one of the option's short names
     * @return an optional wrapping the arguments, or an empty optional if no arguments are found
     */
    @NotNull
    @Unmodifiable
    Optional<Collection<String>> getOptionArguments(char shortName);

    /**
     * Retrieves a single option argument and maps it.
     *
     * @param shortName one of the option's short names
     * @param mapper    the mapper, may return null on failure
     * @param i         the index (starting with 0) of the argument
     */
    <T, X extends Exception>
    @NotNull Expect<T, X> getOptionArgument(char shortName, @NotNull ParamFunc<? super @NotNull String, @Nullable T, X> mapper, int i);

}
