package cc.fluse.ulib.core.io.transform.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.experimental.StandardException;

/**
 * An exception indicating that the transform's data (bytes) are invalid for processing.
 */
@StandardException
@BypassAnnotationEnforcement
public final class InputFormatException extends IOTransformException {
}
