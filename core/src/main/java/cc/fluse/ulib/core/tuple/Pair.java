package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;

/**
 * An object able to hold 2 values.
 *
 * @param <T> the type of the first value
 * @param <U> the type of the second value
 */
@BypassAnnotationEnforcement
public interface Pair<T, U> extends Value<T> {
    U getSecond();
}
