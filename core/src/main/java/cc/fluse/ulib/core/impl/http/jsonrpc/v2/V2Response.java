package cc.fluse.ulib.core.impl.http.jsonrpc.v2;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.http.jsonrpc.RpcResponse;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.StringReader;
import java.net.http.HttpResponse;
import java.util.NoSuchElementException;
import java.util.Optional;

final class V2Response implements RpcResponse {

    static V2Response parse(@NotNull V2Endpoint endpoint, @NotNull HttpResponse<String> response) throws IOException {

        var json = JsonConfiguration.loadJson(new StringReader(response.body()));

        // confirm endpoint version
        if (!json.string("jsonrpc")
                 .map(endpoint.getRpcVersion()::equals)
                 .orElse(false)) {
            throw new IllegalArgumentException("endpoint version");
        }

        var id = json.string("id")
                     .orElseThrow(() -> new NoSuchElementException("response id"));

        var result = json.get("result")
                         .orElse(null);

        RpcResponseError error;
        if (result == null) {
            error = new V2Error(
                    json.int32("error.code")
                        .orElseThrow(() -> new NoSuchElementException("error code")),
                    json.string("error.message")
                        .orElseThrow(() -> new NoSuchElementException("error message")),
                    json.get("data").orElse(null)
            );
        } else {
            error = null;
        }

        return new V2Response(endpoint, id, result, error);
    }

    @NotNull
    @Getter
    private final V2Endpoint endpoint;
    @NotNull
    private final String id;
    @Nullable
    private final Object result;
    @Nullable
    private final RpcResponseError error;

    public V2Response(@NotNull V2Endpoint endpoint, @NotNull String id, @Nullable Object result, @Nullable RpcResponseError error) {
        this.endpoint = endpoint;
        this.id = id;

        if (result != null && error != null || result == null && error == null) {
            throw new IllegalArgumentException("result/error mismatch");
        }

        this.result = result;
        this.error = error;
    }

    @Override
    public @NotNull Optional<String> getId() {
        return Optional.of(this.id);
    }

    @Override
    public @NotNull Optional<Object> getResult() {
        return Optional.ofNullable(this.result);
    }

    @Override
    public @NotNull Optional<RpcResponseError> getError() {
        return Optional.ofNullable(this.error);
    }

    @Override
    public String toString() {
        return "RpcResponse{" +
               "endpoint=" + endpoint +
               ", id='" + id + '\'' +
               ", result=" + result +
               ", error=" + error +
               '}';
    }

    final static class V2Error implements RpcResponseError {

        @Getter
        private final int code;
        @Getter
        @NotNull
        private final String message;
        @Nullable
        private final Object data;

        public V2Error(int code, @NotNull String message, @Nullable Object data) {
            this.code = code;
            this.message = message;
            this.data = data;
        }

        @Override
        public @NotNull Optional<Object> getData() {
            return Optional.ofNullable(this.data);
        }

        @Override
        public String toString() {
            return asSpecStandard()
                    .map(e -> e.toString("%s {%s}".formatted(message, data)))
                    .orElseGet(() ->
                                       "RpcResponseError{" +
                                       "code=" + code +
                                       ", message='" + message + '\'' +
                                       ", data=" + data +
                                       '}');
        }
    }
}
