package cc.fluse.ulib.core.impl;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A thread-safe {@link Pool} implementation.
 *
 * @param <T> the type of elements in the pool
 */
public class SynchronizedPool<T, X extends Exception> implements Pool<T, X> {

    final class Element implements PoolElement<T> {

        private ScheduledFuture<?> healthCheckFuture;
        private Instant lastHealthCheck = Instant.now();

        private final T element;
        private boolean released = true; // true means element is still in pool, false means element is in use

        Element(T element) {
            this.element = element;
            startHealthCheck();
        }

        /**
         * Starts a recurring health check on this element. If a health check is already running, scheduled or no health
         * checker is present, this method does nothing.
         * <p>
         * If health check ran previously, the initial delay is calculated so, that the next health check is run after
         * the specified interval after the last health check. If this delay is negative, the health check is run
         * immediately.
         */
        private synchronized void startHealthCheck() {
            if (healthChecker == null || (healthCheckFuture != null && !healthCheckFuture.isCancelled())) return;
            UndefinedStateError.ensureNotNull(healthCheckInterval, "healthCheckInterval");

            var now = Instant.now();
            var next = lastHealthCheck == null ? now.plus(healthCheckInterval) : lastHealthCheck.plus(healthCheckInterval);
            var delay = next.isBefore(now) ? 0L : now.until(next, ChronoUnit.MILLIS);

            this.healthCheckFuture = Concurrent.SCHEDULER.scheduleAtFixedRate(this::healthCheck,
                    delay, healthCheckInterval.toMillis(), TimeUnit.MILLISECONDS);
        }

        /**
         * Stops future health checks on this element. If no health check is scheduled, this method does nothing.
         */
        private synchronized void stopHealthCheck() {
            if (healthCheckFuture == null || healthCheckFuture.isCancelled()) return;
            healthCheckFuture.cancel(false);
            this.healthCheckFuture = null;
        }

        /**
         * Conducts a health check on this element. If this element is currently in use, or no health checker is
         * present, this method does nothing. If the element is invalid, it is cleaned up and removed from the pool.
         */
        private synchronized void healthCheck() {
            synchronized (pool) {
                if (!released || healthChecker == null) return;
                this.lastHealthCheck = Instant.now();

                // remove from pool to prevent obtaining this element while health check is running
                pool.remove(this);
                inUse++;
                inHealthCheck++;
            }

            // check health
            if (Expect.compute(healthChecker::test, element).orElse(false)) {
                // health check successful, add back to pool
                synchronized (pool) {
                    pool.add(this);
                    inUse--;
                    inHealthCheck--;
                    pool.notifyAll();
                }
                return;
            }
            // health check failed, *do not* add back to pool
            healthCheckFuture.cancel(false);
            this.healthCheckFuture = null;
            clean(true);
        }

        private synchronized void clean(boolean hc) {
            synchronized (pool) {
                inUse--;
                size--;
                if (hc) inHealthCheck--;
            }
            if (cleaner != null) cleaner.accept(true, element);
        }

        @Override
        public synchronized @NotNull T get() {
            if (released) throw new IllegalStateException("Element is released");
            return element;
        }

        @Override
        public synchronized void release() {
            if (released) throw new IllegalStateException("Element already released");
            released = true;

            // restore element
            restorer.accept(element);

            // add element back to pool
            synchronized (pool) {
                pool.add(this);
                inUse--;
                pool.notifyAll();
            }

            // start health check again
            startHealthCheck();
        }

        @Override
        public synchronized void close() {
            release();
        }
    }

    private final Bouncer.StateBouncer bouncer = Bouncer.is();

    private final int maxSize;
    private final Func<? extends @Nullable T, ? extends X> factory;
    private final Consumer<? super T> restorer;
    private final @Nullable BiConsumer<? super Boolean, ? super T> cleaner;

    private final @Nullable Predicate<? super T> healthChecker;
    private final @Nullable Duration healthCheckInterval; // non-null if healthChecker is non-null, null if healthChecker is null

    private final LinkedList<Element> pool = new LinkedList<>();
    private volatile int inUse = 0, size = 0, inHealthCheck = 0;

    /**
     * @param maxSize             the maximum number of elements in the pool, or {@code -1} for no limit
     * @param factory             a factory for creating new elements
     * @param restorer            a function for restoring elements to their initial state when they are released
     * @param healthChecker       a function for checking the health of elements, returning {@code true} if the element
     *                            is healthy, {@code false} otherwise. If {@code null}, no health checks are performed.
     * @param healthCheckInterval the interval between health checks. Must be non-{@code null} if {@code healthChecker}
     *                            is non-{@code null}. Must be {@code null} if {@code healthChecker} is {@code null}.
     * @param cleaner             a function for cleaning up elements when they are removed from the pool, either
     *                            because of a failed health check or because the pool is being closed. May be
     *                            {@code null} to disable cleanup. The first parameter is {@code true} if the element is
     *                            cleaned up because of a failed health check, {@code false} otherwise. The second
     *                            parameter is the element to be cleaned up.
     */
    public SynchronizedPool(int maxSize,
                            @NotNull Func<? extends @Nullable T, ? extends X> factory,
                            @NotNull Consumer<? super @NotNull T> restorer,
                            @Nullable Predicate<? super @NotNull T> healthChecker,
                            @Nullable Duration healthCheckInterval,
                            @Nullable BiConsumer<? super @NotNull Boolean, ? super @NotNull T> cleaner) {
        if (maxSize == 0) throw new IllegalArgumentException("empty pool"); // TODO: rather dedicated EmptyPool class?
        if (healthChecker == null && healthCheckInterval != null)
            throw new IllegalArgumentException("interval for health check without health checker");
        if (healthCheckInterval == null && healthChecker != null)
            throw new IllegalArgumentException("health checker without interval for health check");

        this.maxSize = maxSize > 0 ? maxSize : Integer.MAX_VALUE;
        this.factory = factory;
        this.restorer = restorer;
        this.healthChecker = healthChecker;
        this.healthCheckInterval = healthCheckInterval;
        this.cleaner = cleaner;
    }

    @Override
    public int getMaxSize() {
        return bouncer.canPass() ? maxSize : 0;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getAvailable() {
        return size - inUse;
    }

    @Override
    public int getTotalAvailable() {
        return maxSize - inUse;
    }

    public <XX extends Exception> void close(ParamTask<? super T, X> task) throws IOException, XX {
        synchronized (pool) {
            bouncer.pass();
            bouncer.block(); // block new elements from being obtained

            // move all elements to a new list
            pool.forEach(Element::stopHealthCheck);
            var elements = new LinkedList<>(pool);
            pool.clear();

            // wait for all elements to be released
            while (inUse > 0) {
                try {
                    pool.wait();

                    // move newly released elements to the list
                    pool.forEach(Element::stopHealthCheck);
                    elements.addAll(pool);
                    pool.clear();

                } catch (InterruptedException e) {
                    throw new IOException(e);
                } finally {
                    // restore
                    bouncer.unblock();
                    pool.addAll(elements);
                    pool.forEach(Element::startHealthCheck);
                }
            }


            Exception ex = null;

            // run cleaner on all elements
            if (cleaner != null)
                for (var element : elements) {
                    try {
                        cleaner.accept(false, element.element);
                    } catch (Exception e) {
                        if (ex == null) ex = e;
                        else ex.addSuppressed(e);
                    }
                }

            // run task on all elements
            if (task != null)
                for (var element : elements) {
                    try {
                        task.execute(element.element);
                    } catch (Exception e) {
                        if (ex == null) ex = e;
                        else ex.addSuppressed(e);
                    }
                }

            size = 0;

            if (ex == null) return;
            if (ex instanceof RuntimeException re) throw re;
            throw (XX) ex;
        }
    }

    @Override
    public @NotNull PoolElement<T> obtain(long timeout) throws InterruptedException, X {
        synchronized (pool) {
            bouncer.pass();

            // try to get element
            var element = tryGet();
            if (element != null) return element;

            // try to create element or wait for one to be released
            if (!createElement()) {
                // always wait for health check
                if (inHealthCheck == 0 && timeout < 0) throw new NoSuchElementException();
                pool.wait(inHealthCheck > 0 && timeout < 0 ? 0 : timeout);
            }

            return Optional.ofNullable(tryGet()).orElseThrow(NoSuchElementException::new);
        }
    }

    private @Nullable Element tryGet() {
        Element element;
        synchronized (pool) {
            element = pool.pollFirst();
            if (element == null) return null;
            element.released = false; // mark as used
            inUse++;
            inHealthCheck++;
            // prevent health check from running while element is in use
            element.stopHealthCheck();
        }

        // check health before returning
        if (healthChecker == null || healthChecker.test(element.element)) return element;

        // health check failed
        element.clean(true);
        return null;
    }

    /**
     * Creates a new element and adds it to the pool. If a health checker is present, the element is only added if it
     * passes the health check.
     *
     * @return {@code true} if an element was created and added to the pool, {@code false} otherwise
     */
    private boolean createElement() throws X {
        synchronized (pool) {
            if (size >= maxSize) return false;
            var element = factory.execute();
            if (element == null) return false;

            // check health
            if (healthChecker != null && !healthChecker.test(element)) return false;

            pool.add(new Element(element));
            size++;
            return true;
        }
    }
}
