package cc.fluse.ulib.core.impl.cli;

import cc.fluse.ulib.core.cli.CliArgs;
import cc.fluse.ulib.core.cli.CliOption;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.util.Expect;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ParsedArgs implements CliArgs {
    private final Options parent;
    @Getter
    private final Map<CliOption, Collection<String>> options;
    @Getter
    private final Collection<String> parameters;

    @Override
    public boolean hasOption(@NotNull CliOption opt) {
        return options.containsKey(opt);
    }

    @Override
    public boolean hasOption(@NotNull String name) {
        return parent.getOption(name)
                     .map(this::hasOption)
                     .orElse(false);
    }

    @Override
    public boolean hasOption(char shortName) {
        return parent.getOption(shortName)
                     .map(this::hasOption)
                     .orElse(false);
    }

    @Override
    public @NotNull Optional<Collection<String>> getOptionArguments(@NotNull CliOption opt) {
        if (!hasOption(opt)) {
            return Optional.empty();
        }
        return Optional.ofNullable(options.get(opt));
    }

    @Override
    public @NotNull Optional<Collection<String>> getOptionArguments(@NotNull String name) {
        return parent.getOption(name)
                     .flatMap(this::getOptionArguments);
    }

    @Override
    public @NotNull Optional<Collection<String>> getOptionArguments(char shortName) {
        return parent.getOption(shortName)
                     .flatMap(this::getOptionArguments);
    }

    @Override
    public <T, X extends Exception> @NotNull Expect<T, X> getOptionArgument(@NotNull String name, @NotNull ParamFunc<? super @NotNull String, @Nullable T, X> mapper, int i) {
        return parent.getOption(name)
                     .map(opt -> getOptionArgument(opt, mapper, i))
                     .orElseGet(Expect::empty);
    }

    @Override
    public <T, X extends Exception> Expect<T, X> getOptionArgument(char shortName, ParamFunc<? super @NotNull String, @Nullable T, X> mapper, int i) {
        return parent.getOption(shortName)
                     .map(opt -> getOptionArgument(opt, mapper, i))
                     .orElseGet(Expect::empty);
    }
}
