package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.SeekableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * Abstract base class for {@link SeekableDataChannel}s.
 *
 * @implSpec Implementations must override {@link #isOpen()} and {@link #close()}.
 */
public abstract class AbstractSeekableDataChannel extends AbstractDataChannel implements SeekableDataChannel {
    protected AbstractSeekableDataChannel() {
    }

    @Override
    public @NotNull SeekableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }
}
