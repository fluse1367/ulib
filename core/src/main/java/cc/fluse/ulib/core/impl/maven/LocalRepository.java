package cc.fluse.ulib.core.impl.maven;

import cc.fluse.ulib.core.dependencies.Dependencies;
import cc.fluse.ulib.core.dependencies.Repository;
import cc.fluse.ulib.core.impl.Internal;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.Exclusion;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.RepositoryPolicy;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Local maven repository manager.
 */
public class LocalRepository {
    private static final RepositorySystem REPOSITORY;
    private static final DefaultRepositorySystemSession SESSION;

    static {
        // create thingy that downloads artifacts
        DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
        locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
        locator.addService(TransporterFactory.class, HttpTransporterFactory.class);

        // create actual repository
        REPOSITORY = locator.getService(RepositorySystem.class);
        SESSION = MavenRepositorySystemUtils.newSession();

        // get sure artifacts are valid
        SESSION.setChecksumPolicy(Internal.isAllowMavenChecksumMismatch() ? RepositoryPolicy.CHECKSUM_POLICY_WARN
                                                                          : RepositoryPolicy.CHECKSUM_POLICY_FAIL);


        org.eclipse.aether.repository.LocalRepository lr = new org.eclipse.aether.repository.LocalRepository(Internal.getLocalMavenDir().toFile());
        SESSION.setLocalRepositoryManager(REPOSITORY.newLocalRepositoryManager(SESSION, lr));

        // lock session
        SESSION.setReadOnly();
    }

    /**
     * @see Dependencies#resolve(String, Collection, Collection)
     */
    public static Stream<Path> resolve(String coords, Collection<Repository> repos, Collection<String> exclusions) {
        var excl = exclusions.stream()
                .map(str -> {
                    var parts = str.split(":");
                    return switch (parts.length) {
                        case 1 -> new Exclusion(parts[0], null, null, null);
                        case 2 -> new Exclusion(parts[0], parts[1], null, null);
                        case 3 -> new Exclusion(parts[0], parts[1], null, parts[2]);
                        case 4 -> new Exclusion(parts[0], parts[1], parts[2], parts[3]);
                        default ->
                                throw new IllegalArgumentException("'%s' not in format 'groupId:artifactId[:classifier[:extension]]'".formatted(str));
                    };
                })
                .collect(Collectors.toSet());

        Dependency dependency = new Dependency(new DefaultArtifact(coords), "compile", false, excl);


        var req = new CollectRequest(dependency, repos.stream()
                .map(RepositoryImpl::convertFrom)
                .map(RepositoryImpl::getRepository)
                .toList());
        // resolve dependencies
        DependencyResult res;
        try {
            res = REPOSITORY.resolveDependencies(SESSION, new DependencyRequest(req, null));
        } catch (Exception e) {
            throw new RuntimeException("Dependency resolution failed", e);
        }

        return res.getArtifactResults().stream()
                .map(ArtifactResult::getArtifact)
                .filter(Objects::nonNull) // filter out unresolved
                .map(Artifact::getFile)
                .map(File::toPath);
    }
}
