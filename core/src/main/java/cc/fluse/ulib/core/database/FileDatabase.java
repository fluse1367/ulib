package cc.fluse.ulib.core.database;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

public interface FileDatabase extends Database {
    /**
     * Returns the path of the sqlite database file.
     *
     * @return the path of the sqlite database file
     */
    @NotNull
    Path getPath();
}
