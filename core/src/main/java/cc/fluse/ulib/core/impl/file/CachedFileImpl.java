package cc.fluse.ulib.core.impl.file;

import cc.fluse.ulib.core.file.CachedFile;
import cc.fluse.ulib.core.file.FSUtil;
import cc.fluse.ulib.core.function.*;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.io.SyntheticChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.channels.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class CachedFileImpl implements CachedFile {

    protected final Path path;
    protected final ParamTask<OutputStream, ?> task;

    /**
     * @see CachedFile#of(Path, ParamTask)
     */
    public CachedFileImpl(@Nullable String pfx, @NotNull Path path, @NotNull ParamTask<@NotNull OutputStream, ?> creationTask) {
        this.path = Internal.getCacheDir().resolve(pfx == null ? "__" : pfx).resolve(path);
        this.task = creationTask;
    }

    @Override
    public @NotNull Path getLocation() {
        return path;
    }

    @Override
    public synchronized long getSize() {
        if (!readReady()) {
            return -1;
        }
        try {
            return Files.size(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public @NotNull ByteChannel channel() {
        return new SyntheticChannel(channelRead(), channelWrite());
    }

    // - direct read -

    @Override
    public @NotNull InputStream streamRead() {
        return get(false).thenApply((ParamFunc<Path, InputStream, ?>) Files::newInputStream).join();
    }

    @Override
    public @NotNull <R> CompletableFuture<R> streamRead(@NotNull ParamFunc<? super @NotNull InputStream, R, ?> reader) {
        return CompletableFuture.supplyAsync(this::streamRead).thenApplyAsync(reader);
    }

    @Override
    public @NotNull ReadableByteChannel channelRead() {
        return get(false).thenApply((ParamFunc<Path, ? extends ReadableByteChannel, ?>) Files::newByteChannel).join();
    }

    @Override
    public @NotNull <R> CompletableFuture<R> channelRead(@NotNull ParamFunc<? super @NotNull ReadableByteChannel, R, ?> reader) {
        return CompletableFuture.supplyAsync(this::channelRead).thenApplyAsync(reader);
    }


    // - direct write -

    @Override
    public @NotNull OutputStream streamWrite() {
        return get(false).thenApply((ParamFunc<Path, OutputStream, ?>) Files::newOutputStream)
                         .join();
    }

    @Override
    public @NotNull CompletableFuture<Void> streamWrite(@NotNull ParamTask<? super @NotNull OutputStream, ?> writer) {
        return CompletableFuture.supplyAsync(this::streamWrite).thenAcceptAsync(writer);
    }

    @Override
    public @NotNull WritableByteChannel channelWrite() {
        return get(false).thenApply((ParamFunc<Path, WritableByteChannel, ?>) Files::newByteChannel)
                         .join();
    }

    @Override
    public @NotNull CompletableFuture<Void> channelWrite(@NotNull ParamTask<? super @NotNull WritableByteChannel, ?> writer) {
        return CompletableFuture.supplyAsync(this::channelWrite).thenAcceptAsync(writer);
    }

    // - caching -

    public synchronized @NotNull CompletableFuture<Path> get(boolean recreate) {
        if (!recreate && readReady()) {
            return CompletableFuture.completedFuture(path);
        }
        return populate().thenApply(__ -> path);
    }

    public synchronized @NotNull CompletableFuture<Void> populate() {
        return CompletableFuture.runAsync(Task.as(() -> {
            FSUtil.mkParent(path);
            try (var out = Files.newOutputStream(path)) {
                task.execute(out);
            }
        }));
    }

    @Override
    public @NotNull CompletableFuture<Void> purge() {
        return CompletableFuture.runAsync((Task<?>) () -> Files.deleteIfExists(path));
    }

    protected boolean readReady() {
        return Files.exists(path);
    }
}
