package cc.fluse.ulib.core.impl.http.jsonrpc.v2;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.http.HttpRequestExecutor;
import cc.fluse.ulib.core.http.jsonrpc.*;
import cc.fluse.ulib.core.impl.Concurrent;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

public final class V2Endpoint implements RpcEndpoint {

    @Getter
    private final String rpcVersion = "2.0";
    @Getter
    private final URI endpointUri;

    private Monitor monitor;
    private final HttpRequestExecutor httpExecutor;

    public V2Endpoint(@NotNull URI endpointUri, HttpRequestExecutor httpExecutor) {
        this.endpointUri = endpointUri;
        this.httpExecutor = httpExecutor;
    }

    @Override
    public RpcEndpoint.@NotNull Monitor startMonitor(@NotNull Function<RpcEndpoint, RpcRequest> dummyRequestSupplier,
                                                     int invalidationThreshold,
                                                     @NotNull Consumer<RpcEndpoint.Monitor> preInit) {
        if (monitor != null) {
            return monitor;
        }
        return monitor = new Monitor(dummyRequestSupplier, invalidationThreshold, preInit);
    }

    @NotNull
    @Override
    public Optional<RpcEndpoint.Monitor> getMonitor() {
        return Optional.ofNullable(monitor);
    }

    private void valdiate() {
        if (monitor != null) {
            monitor.validate();
        }
    }

    @Override
    public @NotNull RpcRequest createRequest(@NotNull String method, @Nullable Object parameters) {
        valdiate();
        return new V2Request(this, method, "0", parameters);
    }

    @Override
    public @NotNull CompletableFuture<RpcResponse> sendRequest(@NotNull RpcRequest request) {
        valdiate();

        if (request.getEndpoint() != this || !(request instanceof V2Request v2request)) {
            throw new IllegalArgumentException("unsuitable request object");
        }

        // build request object
        var requestObject = HttpRequest.newBuilder(endpointUri)
                                       .header("Content-Type", "application/json")
                                       .POST(HttpRequest.BodyPublishers.ofString(v2request.buildBody()))
                                       .build();


        // send request async
        return this.httpExecutor.sendAsync(requestObject, HttpResponse.BodyHandlers.ofString())
                                .thenApplyAsync(response -> {
                                    UndefinedStateError.ensureNotNull(response);
                                    if (response.statusCode() != 200) {
                                        throw new IllegalStateException("Request failed with code " + response.statusCode());
                                    }
                                    if (request.getId().isEmpty()) throw new UndefinedStateError("request without id");

                                    try {
                                        return V2Response.parse(this, response);
                                    } catch (IOException e) {
                                        throw new RuntimeException("Malformed json response", e);
                                    }
                                });
    }

    @Override
    public String toString() {
        return "RpcEndpoint{" +
               "rpcVersion='" + rpcVersion + '\'' +
               ", endpointUri=" + endpointUri +
               ", executor=" + httpExecutor +
               '}';
    }

    private final class Monitor implements RpcEndpoint.Monitor {

        private final Object $lock = new Object[0];

        private final ScheduledFuture<?> future;
        private volatile boolean skipNext;

        private final Function<RpcEndpoint, RpcRequest> dummyRequestSupplier;
        private final Collection<Consumer<RpcEndpoint.Monitor>> invalidationHandlers = new LinkedList<>();
        private final Collection<Consumer<RpcEndpoint.Monitor>> responseHandlers = new LinkedList<>();
        private final Collection<BiConsumer<RpcRequest, RpcEndpoint.Monitor>> failureHandlers = new LinkedList<>();

        private boolean valid = true;

        private Instant lastRequestAt;
        private Duration responseTime;
        private RpcResponse lastResponse;
        private int subsequentFails;

        private Monitor(Function<RpcEndpoint, RpcRequest> dummyRequestSupplier, int invalidationThreshold, Consumer<RpcEndpoint.Monitor> preInit) {
            this.dummyRequestSupplier = dummyRequestSupplier;

            if (invalidationThreshold > 0) {
                onResponse(m -> subsequentFails = 0);
                onFailure((r, m) -> {
                    if (++subsequentFails >= invalidationThreshold) {
                        invalidate();
                    }
                });
            }

            preInit.accept(this);

            checkNow(false);
            this.future = Concurrent.SCHEDULER.schedule(() -> checkNow(true), 5, TimeUnit.SECONDS);
        }

        private void checkNow(boolean respectSkip) {
            if (!respectSkip) {
                skipNext = false;
            } else if (skipNext) {
                skipNext = false;
                return;
            }

            validate();

            synchronized ($lock) {

                var request = dummyRequestSupplier.apply(getEndpoint());
                lastRequestAt = Instant.now();
                try {
                    lastResponse = sendRequest(request).join();
                    responseTime = Duration.between(lastRequestAt, Instant.now());
                    onResponse();
                } catch (Exception e) {
                    lastResponse = null;
                    responseTime = Duration.between(lastRequestAt, Instant.now());
                    onFailure(request);
                }

            }
        }

        private void validate() {
            if (!valid) {
                throw new IllegalStateException("Invalidated");
            }
        }

        @Override
        public void invalidate() {
            if (!valid) {
                return;
            }
            synchronized ($lock) {
                valid = false;
                future.cancel(false);
                responseHandlers.clear();
                failureHandlers.clear();

                try {
                    invalidationHandlers.forEach(c -> c.accept(this));
                } catch (Exception e) {
                    // ignored
                }
                invalidationHandlers.clear();
            }
        }

        @Override
        public void check() {
            validate();

            Concurrent.run(() -> {
                checkNow(false);
                skipNext = true;
            });
        }

        @Override
        public @NotNull RpcEndpoint getEndpoint() {
            return V2Endpoint.this;
        }

        @Override
        public @NotNull Instant getLastCheck() {
            synchronized ($lock) {
                return lastRequestAt;
            }
        }

        @Override
        public @NotNull Duration getResponseTime() {
            synchronized ($lock) {
                return responseTime;
            }
        }

        @Override
        public @Nullable RpcResponse getResponse() {
            synchronized ($lock) {
                return lastResponse;
            }
        }

        @Override
        public boolean wasOnline() {
            synchronized ($lock) {
                return lastResponse != null;
            }
        }

        @Override
        public void onResponse(@NotNull Consumer<RpcEndpoint.Monitor> eventHandler) {
            validate();
            Concurrent.run(() -> {
                synchronized ($lock) {
                    responseHandlers.add(eventHandler);
                }
            });
        }

        private void onResponse() {
            responseHandlers.forEach(c -> c.accept(this));
        }

        @Override
        public void onFailure(@NotNull BiConsumer<RpcRequest, RpcEndpoint.Monitor> eventHandler) {
            validate();
            Concurrent.run(() -> {
                synchronized ($lock) {
                    failureHandlers.add(eventHandler);
                }
            });
        }

        private void onFailure(RpcRequest req) {
            failureHandlers.forEach(c -> c.accept(req, this));
        }

        @Override
        public void onInvalidation(@NotNull Consumer<RpcEndpoint.Monitor> eventHandler) {
            validate();
            Concurrent.run(() -> {
                synchronized ($lock) {
                    invalidationHandlers.add(eventHandler);
                }
            });
        }
    }
}
