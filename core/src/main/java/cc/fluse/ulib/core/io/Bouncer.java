package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.impl.io.BouncerImpl;
import cc.fluse.ulib.core.impl.io.BouncerImpl.ChBouncerImpl;
import cc.fluse.ulib.core.impl.io.BouncerImpl.IOBouncerImpl;
import cc.fluse.ulib.core.impl.io.BouncerImpl.StateBouncerImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.function.Supplier;

/**
 * A guard-like object that is acts like a bouncer for closable objects.
 */
public interface Bouncer<X extends Throwable> extends Closeable {

    /**
     * Creates a new bouncer which throws {@link IOException IOExceptions}.
     *
     * @return the newly created object
     */
    @NotNull
    static IOBouncer io() {
        return io((Object) null);
    }

    /**
     * Creates a new bouncer which throws {@link IOException IOExceptions} with the given message.
     *
     * @param message the message to use
     * @return the newly created object
     */
    static @NotNull IOBouncer io(@NotNull String message) {
        return io(null, message);
    }

    /**
     * Creates a new bouncer which throws {@link IOException IOExceptions}.
     *
     * @param $lock the object to synchronize on, on any action
     * @return the newly created object
     */
    @NotNull
    static IOBouncer io(@Nullable Object $lock) {
        return new IOBouncerImpl($lock);
    }

    /**
     * Creates a new bouncer which throws {@link IOException IOExceptions} with the given message.
     *
     * @param $lock   the object to synchronize on, on any action. {@code null} to synchronize on the bouncer itself
     * @param message the message to use
     * @return the newly created object
     */
    static @NotNull IOBouncer io(@Nullable Object $lock, @NotNull String message) {
        return new IOBouncerImpl($lock, message);
    }

    /**
     * Creates a new bouncer which throws {@link ClosedChannelException ClosedChannelExceptions}.
     *
     * @return the newly created object
     */
    @NotNull
    static ChBouncer ch() {
        return ch(null);
    }

    /**
     * Creates a new bouncer which throws {@link ClosedChannelException ClosedChannelExceptions}.
     *
     * @param $lock the object to synchronize on, on any action. {@code null} to synchronize on the bouncer itself
     * @return the newly created object
     */
    @NotNull
    static ChBouncer ch(@Nullable Object $lock) {
        return new ChBouncerImpl($lock);
    }

    /**
     * Creates a new bouncer which throws {@link IllegalStateException IllegalStateExceptions}.
     *
     * @return the newly created object
     */
    @NotNull
    static StateBouncer is() {
        return is((Object) null);
    }

    /**
     * Creates a new bouncer which throws {@link IllegalStateException IllegalStateExceptions} with the given message.
     *
     * @param message the message to use
     * @return the newly created object
     */
    static @NotNull StateBouncer is(@NotNull String message) {
        return is(null, message);
    }

    /**
     * Creates a new bouncer which throws {@link IllegalStateException IllegalStateExceptions}.
     *
     * @param $lock the object to synchronize on, on any action. {@code null} to synchronize on the bouncer itself
     * @return the newly created object
     */
    @NotNull
    static StateBouncer is(@Nullable Object $lock) {
        return new StateBouncerImpl($lock);
    }

    /**
     * Creates a new bouncer which throws {@link IllegalStateException IllegalStateExceptions} with the given message.
     *
     * @param $lock   the object to synchronize on, on any action. {@code null} to synchronize on the bouncer itself
     * @param message the message to use
     * @return the newly created object
     */
    static @NotNull StateBouncer is(@Nullable Object $lock, @NotNull String message) {
        return new StateBouncerImpl($lock, message);
    }

    /**
     * Creates a new bouncer.
     *
     * @return the newly created object
     */
    @NotNull
    static <X extends Throwable> Bouncer<X> of(@NotNull Supplier<? extends @NotNull X> exceptionSupplier) {
        return of(exceptionSupplier, null);
    }

    /**
     * Creates a new bouncer.
     *
     * @param $lock the object to synchronize on, on any action
     * @return the newly created object
     */
    @NotNull
    static <X extends Throwable> Bouncer<X> of(@NotNull Supplier<? extends @NotNull X> exceptionSupplier,
                                               @Nullable Object $lock) {
        return new BouncerImpl<>(exceptionSupplier, $lock);
    }

    /**
     * @return {@code true} if the bouncer lets you pass, {@code false} otherwise
     */
    boolean canPass();

    /**
     * Instructs the bouncer to not let you pass anymore.
     *
     * @implSpec This method must not have any additional effects when called multiple times.
     */
    void block();

    /**
     * Instructs the bouncer to let you pass again.
     *
     * @throws UnsupportedOperationException in case the operation is not supported by the implementation
     */
    void unblock() throws UnsupportedOperationException;

    /**
     * Throws an exception provided by a supplier if the bouncer is closed.
     *
     * @param exceptionSupplier a stateless function supplying the throwable object.
     */
    <XX extends Throwable> void pass(@NotNull Supplier<? extends XX> exceptionSupplier) throws XX;

    /**
     * Throws an exception if the bouncer is closed.
     */
    void pass() throws X;

    @Override
    default void close() {
        if (!canPass()) block();
    }

    interface IOBouncer extends Bouncer<IOException> {}

    interface ChBouncer extends Bouncer<ClosedChannelException> {}

    interface StateBouncer extends Bouncer<IllegalStateException> {}

}
