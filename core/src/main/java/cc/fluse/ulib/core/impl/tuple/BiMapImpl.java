package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.BiMap;
import cc.fluse.ulib.core.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class BiMapImpl<K, V1, V2> implements BiMap<K, V1, V2> {
    private final Map<K, Pair<V1, V2>> source;

    public BiMapImpl(@NotNull Map<K, Pair<V1, V2>> source) {
        if (source instanceof BiMap) throw new IllegalArgumentException("Source map is already a BiMap");
        this.source = source;
    }

    // - delegate methods -

    @Override
    public int size() {return source.size();}

    @Override
    public boolean isEmpty() {return source.isEmpty();}

    @Override
    public boolean containsKey(Object key) {return source.containsKey(key);}

    @Override
    public boolean containsValue(Object value) {return source.containsValue(value);}

    @Override
    public Pair<V1, V2> get(Object key) {return source.get(key);}

    @Nullable
    @Override
    public Pair<V1, V2> put(K key, Pair<V1, V2> value) {return source.put(key, value);}

    @Override
    public Pair<V1, V2> remove(Object key) {return source.remove(key);}

    @Override
    public void putAll(@NotNull Map<? extends K, ? extends Pair<V1, V2>> m) {source.putAll(m);}

    @Override
    public void clear() {source.clear();}

    @NotNull
    @Override
    public Set<K> keySet() {return source.keySet();}

    @NotNull
    @Override
    public Collection<Pair<V1, V2>> values() {return source.values();}

    @NotNull
    @Override
    public Set<Entry<K, Pair<V1, V2>>> entrySet() {return source.entrySet();}

    @Override
    public boolean equals(Object o) {return source.equals(o);}

    @Override
    public int hashCode() {return source.hashCode();}

    @Override
    public Pair<V1, V2> getOrDefault(Object key, Pair<V1, V2> defaultValue) {return source.getOrDefault(key, defaultValue);}

    @Override
    public void forEach(BiConsumer<? super K, ? super Pair<V1, V2>> action) {source.forEach(action);}

    @Override
    public void replaceAll(BiFunction<? super K, ? super Pair<V1, V2>, ? extends Pair<V1, V2>> function) {source.replaceAll(function);}

    @Nullable
    @Override
    public Pair<V1, V2> putIfAbsent(K key, Pair<V1, V2> value) {return source.putIfAbsent(key, value);}

    @Override
    public boolean remove(Object key, Object value) {return source.remove(key, value);}

    @Override
    public boolean replace(K key, Pair<V1, V2> oldValue, Pair<V1, V2> newValue) {return source.replace(key, oldValue, newValue);}

    @Nullable
    @Override
    public Pair<V1, V2> replace(K key, Pair<V1, V2> value) {return source.replace(key, value);}

    @Override
    public Pair<V1, V2> computeIfAbsent(K key, @NotNull Function<? super K, ? extends Pair<V1, V2>> mappingFunction) {return source.computeIfAbsent(key, mappingFunction);}

    @Override
    public Pair<V1, V2> computeIfPresent(K key, @NotNull BiFunction<? super K, ? super Pair<V1, V2>, ? extends Pair<V1, V2>> remappingFunction) {return source.computeIfPresent(key, remappingFunction);}

    @Override
    public Pair<V1, V2> compute(K key, @NotNull BiFunction<? super K, ? super Pair<V1, V2>, ? extends Pair<V1, V2>> remappingFunction) {return source.compute(key, remappingFunction);}

    @Override
    public Pair<V1, V2> merge(K key, @NotNull Pair<V1, V2> value, @NotNull BiFunction<? super Pair<V1, V2>, ? super Pair<V1, V2>, ? extends Pair<V1, V2>> remappingFunction) {return source.merge(key, value, remappingFunction);}
}
