package cc.fluse.ulib.core.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.util.Conditions;
import lombok.experimental.StandardException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Similar to {@link IllegalStateException} this error indicates the application is in an undefined state (which should
 * usually not happen). An undefined state is such a state that should not be possible to reach (ensured by semantics),
 * but has been reached anyway. If there is a possibility that the state is reached, use {@link IllegalStateException}.
 * Usually the application cannot recover from an undefined state, and the error should be propagated to the user (this
 * is why this class is an {@link Error} and not an {@link Exception}).
 */
@StandardException
@BypassAnnotationEnforcement
public class UndefinedStateError extends Error {

    public static void ensureExpected(@Nullable Object expected, @Nullable Object actual,
                                      @Nullable String expectedStr, @Nullable String actualStr) {
        if (Objects.equals(actual, expected)) return;
        throw new UndefinedStateError("expected %s, got %s".formatted(
                expectedStr != null ? expectedStr : expected, actualStr != null ? actualStr : actual));
    }

    public static void ensureNot(@Nullable Object unexpected, @Nullable Object actual, @Nullable String str) {
        if (!Objects.equals(actual, unexpected)) return;
        throw new UndefinedStateError("got unexpected %s".formatted(str != null ? str : unexpected));
    }

    /**
     * Throws an {@link UndefinedStateError} if one of the supplied parameters is {@code null}.
     */
    @Contract("null, _ -> fail; _, null -> fail")
    public static void ensureNotNull(@Nullable Object obj, @Nullable Object @Nullable ... objs) {
        if (obj == null || objs == null || !Conditions.nNil(objs)) throw new UndefinedStateError("null");
    }

    /**
     * Throws an {@link UndefinedStateError} if the supplied parameter is {@code null}.
     *
     * @param obj the object to check
     * @param str the string to use in the error message
     * @return the object if it is not {@code null}
     */
    public static <T> @NotNull T ensureNotNull(@Nullable T obj, @Nullable String str) {
        if (obj == null) throw new UndefinedStateError("null: %s".formatted(str));
        return obj;
    }

    /**
     * Throws an {@link UndefinedStateError} if one of the supplied parameters is not {@code null}.
     */
    @Contract("!null, _ -> fail; _, !null -> fail")
    public static void ensureNull(@Nullable Object obj, @Nullable Object @Nullable ... objs) {
        if (obj == null && (objs == null || Conditions.nil(objs))) return;
        throw new UndefinedStateError("not null");
    }

    /**
     * Throws an {@link UndefinedStateError} if the value is not {@code true}.
     */
    @Contract("false -> fail")
    public static void ensureTrue(boolean value) {
        ensureExpected(true, value, null, null);
    }

    /**
     * Throws an {@link UndefinedStateError} if the value is not {@code false}.
     */
    @Contract("false -> fail")
    public static void ensureFalse(boolean value) {
        ensureExpected(false, value, null, null);
    }

    /**
     * Throws an {@link UndefinedStateError} if the value is not {@code true}.
     *
     * @param condition the condition to check
     * @param message   the message to use in the error
     */
    public static void check(boolean condition, @Nullable String message) {
        if (!condition) throw new UndefinedStateError(message);
    }
}
