package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.impl.io.transform.FaucetTransformChannel;
import cc.fluse.ulib.core.impl.io.transform.PipeTransformChannel;
import cc.fluse.ulib.core.impl.io.transform.SinkTransformChannel;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.DataChannel;
import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import cc.fluse.ulib.core.io.channel.WritableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * A {@link TransformerChainChannel} that can be written to and read from.
 */
public interface TransformChannel extends ReadableTransformChannel, WritableTransformChannel, ByteTransformer {


    /**
     * Constructs a pipeline like channel, that transforms written data and makes it available for reading.
     *
     * @return the newly constructed transform channel
     */
    @NotNull
    static TransformChannel create() {
        return new PipeTransformChannel();
    }

    /**
     * Constructs a writable transform channel that writes the transformed data to the given sink.
     *
     * @param sink the underlying channel to ultimately write bytes to
     * @return the newly constructed transform channel
     */
    @NotNull
    static WritableTransformChannel create(@NotNull WritableByteChannel sink) {
        return new SinkTransformChannel(sink);
    }

    /**
     * Constructs a writable transform channel that writes the transformed data to the given sink.
     *
     * @param sink the underlying stream to ultimately write bytes to
     * @return the newly constructed transform channel
     */
    static @NotNull WritableTransformChannel create(@NotNull OutputStream sink) {
        return create(Channels.newChannel(sink));
    }

    /**
     * Constructs a readable transform channel that reads data from the given faucet, transforms it and makes it
     * available for reading.
     *
     * @param faucet the underlying channel to pull bytes from
     * @return the newly constructed transform channel
     */
    @NotNull
    static ReadableTransformChannel create(@NotNull ReadableByteChannel faucet) {
        return new FaucetTransformChannel(faucet);
    }

    /**
     * Constructs a readable transform channel that reads data from the given faucet, transforms it and makes it
     * available for reading.
     *
     * @param faucet the underlying stream to pull bytes from
     * @return the newly constructed transform channel
     */
    static @NotNull ReadableTransformChannel create(@NotNull InputStream faucet) {
        return create(Channels.newChannel(faucet));
    }

    @Override
    default @NotNull DataChannel channel() {
        return IOUtil.isolate((DataChannel) this);
    }

    @Override
    default @NotNull ReadableDataChannel channelRead() {
        return channel();
    }

    @Override
    default @NotNull WritableDataChannel channelWrite() {
        return channel();
    }
}
