package cc.fluse.ulib.core.impl.database.sql.mariadb;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.MariaDBDatabase;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase.DBImpl;
import cc.fluse.ulib.core.impl.database.sql.mysql.MySQLTable;

import java.sql.Connection;
import java.util.Collection;
import java.util.Properties;

@DBImpl(names = "mariadb",
        driverCoords = "{{maven.mariadb}}",
        protocol = "jdbc:mariadb://",
        supportsConnectionPooling = true)
public class MariaDBDatabaseImpl extends AbstractSqlDatabase implements MariaDBDatabase {
    protected MariaDBDatabaseImpl(Connection connection) {
        super(connection);
    }

    protected MariaDBDatabaseImpl(String url, Properties info, int maxPoolSize) {
        super(url, info, maxPoolSize);
    }

    @Override
    protected Table newTable(String name, Collection<Column<?>> columns) {
        // mysql is client compatible with maraidb
        // see https://mariadb.com/kb/en/mariadb-vs-mysql-compatibility/
        return new MySQLTable(this, name, columns);
    }
}
