package cc.fluse.ulib.core.impl.configuration.json;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.impl.configuration.ConfigurationBase;
import com.github.cliftonlabs.json_simple.JsonException;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.Map;

public class JsonDocument extends ConfigurationBase<JsonDocument> implements JsonConfiguration {

    private final JsonSerializer serializer;

    // construct as empty root
    JsonDocument(JsonSerializer serializer) {
        super();
        this.serializer = serializer;
    }

    // construct as sub
    JsonDocument(JsonSerializer serializer, JsonDocument root, JsonDocument parent, String key) {
        super(root, parent, key);
        this.serializer = serializer;
    }

    @Override
    protected JsonDocument constructSub(String key) {
        return new JsonDocument(serializer, getRoot(), this, key);
    }

    // IO

    @Override
    public void reload(@NotNull Reader reader) throws IOException {
        clear();
        try {
            serializer.deserialize(reader, this);
        } catch (JsonException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void dump(@NotNull Writer writer) throws IOException {
        serializer.serialize(writer, this);
    }

    public void clear() {
        children.clear();
    }

    // serializer access

    Map<String, Object> children() {
        return children;
    }

    void put(String key, Object val) {
        children.put(key, val);
    }

}
