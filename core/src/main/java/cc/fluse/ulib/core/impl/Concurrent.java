package cc.fluse.ulib.core.impl;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.util.ArrayUtil;
import lombok.SneakyThrows;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;

public final class Concurrent {
    static class DaemonThread extends Thread {
        public DaemonThread(Runnable target) {
            super(target);
            setDaemon(true);
        }
    }

    public static final ThreadFactory DAEMON_FACTORY = DaemonThread::new;
    public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(8, DAEMON_FACTORY);

    @SafeVarargs
    public static <T> List<T> await(Func<T, ?> task, Func<T, ?>... tasks) {
        var st = Arrays.stream(ArrayUtil.concat(task, tasks));

        if (Internal.isForceSync()) {
            // skip executor
            return st.map(Concurrent::catching).toList();
        }

        return st
                .map(Concurrent::run)
                .map(Concurrent::get)
                .toList();
    }

    @SneakyThrows
    private static <T> T get(Future<T> fut) {
        return fut.get();
    }

    public static <T> CompletableFuture<T> run(Func<T, ?> task) {
        return CompletableFuture.supplyAsync(() -> catching(task));
    }

    public static CompletableFuture<Void> run(Task<?> task) {
        return CompletableFuture.runAsync(() -> catching(task));
    }

    public static <T> CompletableFuture<T> run(Supplier<T> task) {
        return CompletableFuture.supplyAsync(task);
    }

    public static CompletableFuture<Void> run(Runnable task) {
        return CompletableFuture.runAsync(task);
    }

    private static void catching(Task<?> r) {
        try {
            r.execute();
        } catch (Throwable thr) {
            System.err.println("An error occurred while executing a task.");
            thr.printStackTrace();
        }
    }

    @SneakyThrows
    private static <R> R catching(Callable<R> c) {
        try {
            return c.call();
        } catch (Throwable thr) {
            System.err.println("An error occurred while executing a task.");
            thr.printStackTrace();
            throw thr;
        }
    }

    public static <K, V> Map<K, V> newThreadsafeMap() {
        return Internal.isForceSync() ? Collections.synchronizedMap(new HashMap<>()) : new ConcurrentHashMap<>();
    }

    public static <T> Collection<T> newThreadsafeCollection() {
        return Internal.isForceSync() ? Collections.synchronizedCollection(new LinkedList<>())
                : new ConcurrentLinkedDeque<>();
    }

    public static <T> Set<T> newThreadsafeSet() {
        return Internal.isForceSync() ? Collections.synchronizedSet(new HashSet<>())
                : ConcurrentHashMap.newKeySet();
    }
}
