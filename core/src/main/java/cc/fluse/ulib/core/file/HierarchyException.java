package cc.fluse.ulib.core.file;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.experimental.StandardException;

@StandardException
@BypassAnnotationEnforcement
public class HierarchyException extends RuntimeException {}
