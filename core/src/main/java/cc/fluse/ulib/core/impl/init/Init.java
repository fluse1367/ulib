package cc.fluse.ulib.core.impl.init;

import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.UnsafeOperations;
import lombok.Synchronized;

import java.lang.instrument.Instrumentation;

public class Init {
    @Synchronized
    public static void init(Object inst) {
        UnsafeOperations.unsafeAccess("Init");
        Internal.agentInit((Instrumentation) inst);
    }
}
