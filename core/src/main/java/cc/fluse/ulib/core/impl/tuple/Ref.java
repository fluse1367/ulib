package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.Value;

public class Ref<T> extends TupleImpl<Object> implements Value<T> {

    public Ref(T t) {
        super(t);
    }

    @Override
    public T getFirst() {
        return getI(0);
    }
}
