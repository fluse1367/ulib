/**
 * Extended functional interfaces (in addition to {@link java.util.function}).
 */
package cc.fluse.ulib.core.function;