package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.transform.ReadableTransformChannel;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class FaucetTransformChannel extends AbstractTransformChannel implements ReadableTransformChannel {

    private final ReadableByteChannel faucet;

    public FaucetTransformChannel(ReadableByteChannel faucet) {
        this.faucet = faucet;
    }

    @Override
    public int read(@NotNull ByteBuffer dst) throws IOException {
        super.bouncer.pass();

        // try read from finished processing
        int i;
        if ((i = super.pull(dst)) != 0) {
            // read success or EOF (EOF can only happen if faucet EOF was reached previously and the underlying pipe was closed subsequently)
            return i;
        }

        // feed pipe from faucet until read success or EOF

        try (var e = Pool.BYTE_BUFFER_DEFAULT_POOL.obtain()) {
            var buf = e.get();
            do {
                if (faucet.read(buf.clear()) != -1) {
                    // read success
                    super.push(buf.flip());
                } else {
                    // EOF
                    UndefinedStateError.check(!isCompleted(), "faucet EOF reached but processing already completed");
                    IOUtil.closeQuietly(faucet);
                    super.complete(); // finishes off processing
                    return super.pull(dst);
                }
            } while ((i = super.pull(dst)) == 0);
            return i;
        }
    }

    @Override
    public synchronized void close() throws IOException {
        if (!isOpen()) return;
        if (faucet.isOpen()) faucet.close();
        super.close();
    }
}
