package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Readable;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class ByteArrayDataInputStream extends DataInputStream implements Readable {

    /**
     * @see ByteArrayInputStream#ByteArrayInputStream(byte[]) ByteArrayInputStream(byte[])
     */
    public ByteArrayDataInputStream(byte[] buf) {
        super(new ByteArrayInputStream(buf));
    }

    @Override
    public @NotNull InputStream streamRead() {
        return IOUtil.isolate(this);
    }

    @Override
    public @NotNull ReadableByteChannel channel() throws IOException {
        return IOUtil.isolate(Channels.newChannel(this));
    }
}
