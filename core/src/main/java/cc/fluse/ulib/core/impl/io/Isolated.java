package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.impl.io.datachannel.AbstractDataChannel;
import cc.fluse.ulib.core.impl.io.datachannel.AbstractReadableDataChannel;
import cc.fluse.ulib.core.impl.io.datachannel.AbstractSeekableDataChannel;
import cc.fluse.ulib.core.impl.io.datachannel.AbstractWritableDataChannel;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.Bouncer.IOBouncer;
import cc.fluse.ulib.core.util.ArrayUtil;
import cc.fluse.ulib.core.util.Conditions;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.nio.ByteBuffer;
import java.util.stream.Stream;

public class Isolated {

    public static class Universal<T extends AutoCloseable> implements InvocationHandler {

        public static <T extends AutoCloseable> T of(@NotNull T t, boolean blockAll) {
            //noinspection unchecked
            return (T) Proxy.newProxyInstance(
                t.getClass().getClassLoader(),
                t.getClass().isInterface() ? ArrayUtil.concat(t.getClass(), t.getClass().getInterfaces()) : t.getClass().getInterfaces(),
                new Universal<>(t, blockAll)
            );
        }

        private final T delegate;
        private final boolean blockAll; // whether to block all methods or only #close()
        private volatile boolean closed;

        public Universal(T delegate, boolean blockAll) {
            this.delegate = delegate;
            this.blockAll = blockAll;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            if (method.getName().equals("close")) {
                closed = true;
                return null;
            }

            if (closed && blockAll
                && !Conditions.inIC(method.getName(), "hashCode", "equals", "toString") // <- never block these methods
            ) {
                // check if method may throw IOException, if not, throw an IllegalStateException instead

                if (Stream.of(method.getExceptionTypes()).anyMatch(IOException.class::isAssignableFrom)) {
                    throw new IOException("closed");
                }

                throw new IllegalStateException("closed");
            }

            // delegate to the original method

            return delegate.getClass().getMethod(method.getName(), method.getParameterTypes()).invoke(delegate, args);
        }
    }

    public static class InputStream extends java.io.InputStream {
        private final IOBouncer bouncer = Bouncer.io();
        private final java.io.InputStream delegate;

        public InputStream(java.io.InputStream delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            bouncer.close();
        }

        @Override
        public int read() throws IOException {
            bouncer.pass();
            return delegate.read();
        }

        @Override
        public int read(@NotNull byte[] b) throws IOException {
            bouncer.pass();
            return delegate.read(b);
        }

        @Override
        public int read(@NotNull byte[] b, int off, int len) throws IOException {
            bouncer.pass();
            return delegate.read(b, off, len);
        }

        @Override
        public byte[] readAllBytes() throws IOException {
            bouncer.pass();
            return delegate.readAllBytes();
        }

        @Override
        public byte[] readNBytes(int len) throws IOException {
            bouncer.pass();
            return delegate.readNBytes(len);
        }

        @Override
        public int readNBytes(byte[] b, int off, int len) throws IOException {
            bouncer.pass();
            return delegate.readNBytes(b, off, len);
        }

        @Override
        public long skip(long n) throws IOException {
            bouncer.pass();
            return delegate.skip(n);
        }

        @Override
        public void skipNBytes(long n) throws IOException {
            bouncer.pass();
            delegate.skipNBytes(n);
        }

        @Override
        public int available() throws IOException {
            bouncer.pass();
            return delegate.available();
        }

        @Override
        public void mark(int readlimit) {
            if (bouncer.canPass()) delegate.mark(readlimit);
        }

        @Override
        public void reset() throws IOException {
            bouncer.pass();
            delegate.reset();
        }

        @Override
        public boolean markSupported() {
            return bouncer.canPass() && delegate.markSupported();
        }

        @Override
        public long transferTo(java.io.OutputStream out) throws IOException {
            bouncer.pass();
            return delegate.transferTo(out);
        }
    }

    public static class OutputStream extends java.io.OutputStream {
        private final IOBouncer bouncer = Bouncer.io();
        private final java.io.OutputStream delegate;

        public OutputStream(java.io.OutputStream delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            if (!bouncer.canPass()) return;
            bouncer.close();
            delegate.flush();
        }

        @Override
        public void write(int b) throws IOException {
            bouncer.pass();
            delegate.write(b);
        }

        @Override
        public void write(@NotNull byte[] b) throws IOException {
            bouncer.pass();
            delegate.write(b);
        }

        @Override
        public void write(@NotNull byte[] b, int off, int len) throws IOException {
            bouncer.pass();
            delegate.write(b, off, len);
        }

        @Override
        public void flush() throws IOException {
            bouncer.pass();
            delegate.flush();
        }
    }


    public static class ReadableByteChannel implements java.nio.channels.ReadableByteChannel {
        private final ChBouncer bouncer = Bouncer.ch();
        private final java.nio.channels.ReadableByteChannel delegate;

        public ReadableByteChannel(java.nio.channels.ReadableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            bouncer.close();
        }

        @Override
        public int read(ByteBuffer dst) throws IOException {
            bouncer.pass();
            return delegate.read(dst);
        }

        @Override
        public boolean isOpen() {
            return bouncer.canPass() && delegate.isOpen();
        }
    }

    public static class ReadableDataChannel extends AbstractReadableDataChannel {
        private final java.nio.channels.ReadableByteChannel delegate;

        public ReadableDataChannel(java.nio.channels.ReadableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public int read(@NotNull ByteBuffer dst) throws IOException {
            super.bouncer.pass();
            return delegate.read(dst);
        }
    }

    public static class WritableByteChannel implements java.nio.channels.WritableByteChannel {
        private final ChBouncer bouncer = Bouncer.ch();
        private final java.nio.channels.WritableByteChannel delegate;

        public WritableByteChannel(java.nio.channels.WritableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            bouncer.close();
        }

        @Override
        public int write(ByteBuffer src) throws IOException {
            bouncer.pass();
            return delegate.write(src);
        }

        @Override
        public boolean isOpen() {
            return bouncer.canPass() && delegate.isOpen();
        }
    }

    public static class WritableDataChannel extends AbstractWritableDataChannel {
        private final java.nio.channels.WritableByteChannel delegate;

        public WritableDataChannel(java.nio.channels.WritableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public int write(@NotNull ByteBuffer src) throws IOException {
            super.bouncer.pass();
            return delegate.write(src);
        }
    }

    public static class ByteChannel implements java.nio.channels.ByteChannel {
        private final ChBouncer bouncer = Bouncer.ch();
        private final java.nio.channels.ByteChannel delegate;

        public ByteChannel(java.nio.channels.ByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            bouncer.close();
        }

        @Override
        public int read(ByteBuffer dst) throws IOException {
            bouncer.pass();
            return delegate.read(dst);
        }

        @Override
        public int write(ByteBuffer src) throws IOException {
            bouncer.pass();
            return delegate.write(src);
        }

        @Override
        public boolean isOpen() {
            return bouncer.canPass() && delegate.isOpen();
        }
    }

    public static class DataChannel extends AbstractDataChannel {
        private final java.nio.channels.ByteChannel delegate;

        public DataChannel(java.nio.channels.ByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public int read(@NotNull ByteBuffer dst) throws IOException {
            super.bouncer.pass();
            return delegate.read(dst);
        }

        @Override
        public int write(@NotNull ByteBuffer src) throws IOException {
            super.bouncer.pass();
            return delegate.write(src);
        }
    }

    public static class SeekableByteChannel implements java.nio.channels.SeekableByteChannel {
        private final ChBouncer bouncer = Bouncer.ch();
        private final java.nio.channels.SeekableByteChannel delegate;

        public SeekableByteChannel(java.nio.channels.SeekableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public void close() throws IOException {
            bouncer.close();
        }

        @Override
        public int read(ByteBuffer dst) throws IOException {
            bouncer.pass();
            return delegate.read(dst);
        }

        @Override
        public int write(ByteBuffer src) throws IOException {
            bouncer.pass();
            return delegate.write(src);
        }

        @Override
        public long position() throws IOException {
            bouncer.pass();
            return delegate.position();
        }

        @Override
        public java.nio.channels.SeekableByteChannel position(long newPosition) throws IOException {
            bouncer.pass();
            return delegate.position(newPosition);
        }

        @Override
        public long size() throws IOException {
            bouncer.pass();
            return delegate.size();
        }

        @Override
        public java.nio.channels.SeekableByteChannel truncate(long size) throws IOException {
            bouncer.pass();
            return delegate.truncate(size);
        }

        @Override
        public boolean isOpen() {
            return bouncer.canPass() && delegate.isOpen();
        }
    }

    public static class SeekableDataChannel extends AbstractSeekableDataChannel {
        private final java.nio.channels.SeekableByteChannel delegate;

        public SeekableDataChannel(java.nio.channels.SeekableByteChannel delegate) {
            this.delegate = delegate;
        }

        @Override
        public long position() throws IOException {
            super.bouncer.pass();
            return delegate.position();
        }

        @Override
        public cc.fluse.ulib.core.io.channel.@NotNull SeekableDataChannel position(long newPosition) throws IOException {
            super.bouncer.pass();
            delegate.position(newPosition);
            return this;
        }

        @Override
        public long size() throws IOException {
            bouncer.pass();
            return delegate.size();
        }

        @Override
        public cc.fluse.ulib.core.io.channel.@NotNull SeekableDataChannel truncate(long size) throws IOException {
            super.bouncer.pass();
            delegate.truncate(size);
            return this;
        }

        @Override
        public int read(@NotNull ByteBuffer dst) throws IOException {
            super.bouncer.pass();
            return delegate.read(dst);
        }

        @Override
        public int write(@NotNull ByteBuffer src) throws IOException {
            super.bouncer.pass();
            return delegate.write(src);
        }
    }

}
