package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

/**
 * A task that takes three parameters, does not return a result and may throw a throwable object.
 *
 * @apiNote only pass this task object as consumer if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface TriParamTask<T, U, V, X extends Exception> extends TriConsumer<T, U, V> {

    @Deprecated
    static <T, U, V, X extends Exception> TriParamTask<T, U, V, X> as(@NotNull TriParamTask<T, U, V, ?> func) {
        return func::apply;
    }


    void execute(T t, U u, V v) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default void apply(T t, U u, V v) {
        execute(t, u, v);
    }
}
