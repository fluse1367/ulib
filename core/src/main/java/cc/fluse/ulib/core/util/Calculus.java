package cc.fluse.ulib.core.util;


/**
 * A collection of useful math functions.
 */
public final class Calculus {

    /**
     * Converts a double to a long, throwing an exception if the double has a fractional part.
     *
     * @param d the double
     * @return the long
     */
    public static long toLongExact(double d) {
        return Conversions.safecast(d).orElseThrow(() -> new ArithmeticException("fractional part"));
    }

    /**
     * Converts a float to an int, throwing an exception if the float has a fractional part.
     *
     * @param f the float
     * @return the int
     */
    public static int toIntExact(float f) {
        return Conversions.safecast(f).orElseThrow(() -> new ArithmeticException("fractional part"));
    }

    /**
     * Limits a long to the range of an int. If the long is too large, {@link Integer#MAX_VALUE} is returned. If the
     * long is too small, {@link Integer#MIN_VALUE} is returned.
     *
     * @param l the long
     * @return the int
     */
    public static int limitToInt(long l) {
        return (int) clamp(l, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Returns the next or previous multiple of a given step from a given value.
     *
     * @param x    the value
     * @param step the step size
     * @param up   if {@code true} the next multiple is returned, otherwise the previous one
     * @return the result
     */
    public static double step(double x, double step, boolean up) {
        var m = x / step;
        return (up ? Math.ceil(m) : Math.floor(m)) * step;
    }

    /**
     * Returns the next or previous multiple of a given step from a given value.
     *
     * @param x    the value
     * @param step the step size
     * @param up   if {@code true} the next multiple is returned, otherwise the previous one
     * @return the result
     */
    public static long step(long x, long step, boolean up) {
        var m = x / (double) step;
        return toLongExact((up ? Math.ceil(m) : Math.floor(m)) * step);
    }

    /**
     * Returns the next or previous multiple of a given step from a given value.
     *
     * @param x    the value
     * @param step the step size
     * @param up   if {@code true} the next multiple is returned, otherwise the previous one
     * @return the result
     */
    public static float step(float x, float step, boolean up) {
        var m = x / step;
        return (up ? (float) Math.ceil(m) : (float) Math.floor(m)) * step;
    }

    /**
     * Returns the next or previous multiple of a given step from a given value.
     *
     * @param x    the value
     * @param step the step size
     * @param up   if {@code true} the next multiple is returned, otherwise the previous one
     * @return the result
     */
    public static int step(int x, int step, boolean up) {
        var m = x / (double) step;
        return Math.toIntExact(toLongExact((up ? Math.ceil(m) : Math.floor(m))) * step);
    }

    /**
     * Clamps a value between two bounds. The bounds may be given in any order.
     * <p>
     * Clamping is a mathematical operation that limits a value to a given range. If the value is smaller than the lower
     * bound, the lower bound is returned. If the value is greater than the upper bound, the upper bound is returned.
     * Otherwise, the value itself is returned.
     *
     * @param x the value
     * @param b the first bound
     * @param d the second bound
     * @return the clamped value
     * @see #clamp(long, long, long)
     * @see #clamp(float, float, float)
     * @see #clamp(int, int, int)
     */
    public static double clamp(double x, double b, double d) {
        double min, max;
        if (b < d) {
            min = b;
            max = d;
        } else {
            min = d;
            max = b;
        }
        return Math.min(Math.max(x, min), max);
    }

    /**
     * Clamps a value between two bounds. The bounds may be given in any order.
     * <p>
     * Clamping is a mathematical operation that limits a value to a given range. If the value is smaller than the lower
     * bound, the lower bound is returned. If the value is greater than the upper bound, the upper bound is returned.
     * Otherwise, the value itself is returned.
     *
     * @param x the value
     * @param b the first bound
     * @param d the second bound
     * @return the clamped value
     * @see #clamp(double, double, double)
     * @see #clamp(float, float, float)
     * @see #clamp(int, int, int)
     */
    public static long clamp(long x, long b, long d) {
        long min, max;
        if (b < d) {
            min = b;
            max = d;
        } else {
            min = d;
            max = b;
        }
        return Math.min(Math.max(x, min), max);
    }

    /**
     * Clamps a value between two bounds. The bounds may be given in any order.
     * <p>
     * Clamping is a mathematical operation that limits a value to a given range. If the value is smaller than the lower
     * bound, the lower bound is returned. If the value is greater than the upper bound, the upper bound is returned.
     * Otherwise, the value itself is returned.
     *
     * @param x the value
     * @param b the first bound
     * @param d the second bound
     * @return the clamped value
     * @see #clamp(double, double, double)
     * @see #clamp(long, long, long)
     * @see #clamp(int, int, int)
     */
    public static float clamp(float x, float b, float d) {
        float min, max;
        if (b < d) {
            min = b;
            max = d;
        } else {
            min = d;
            max = b;
        }
        return Math.min(Math.max(x, min), max);
    }

    /**
     * Clamps a value between two bounds. The bounds may be given in any order.
     * <p>
     * Clamping is a mathematical operation that limits a value to a given range. If the value is smaller than the lower
     * bound, the lower bound is returned. If the value is greater than the upper bound, the upper bound is returned.
     * Otherwise, the value itself is returned.
     *
     * @param x the value
     * @param b the first bound
     * @param d the second bound
     * @return the clamped value
     * @see #clamp(double, double, double)
     * @see #clamp(long, long, long)
     * @see #clamp(float, float, float)
     */
    public static int clamp(int x, int b, int d) {
        int min, max;
        if (b < d) {
            min = b;
            max = d;
        } else {
            min = d;
            max = b;
        }
        return Math.min(Math.max(x, min), max);
    }

    /**
     * Linearly interpolates between two values. The {@code t} parameter is clamped to the range {@code [0, 1]}.
     * <p>
     * Linear interpolation is a mathematical operation that finds a value between two values. If {@code t} is 0, the
     * first value is returned. If {@code t} is 1, the second value is returned. If {@code t} is 0.5, the average of the
     * two values is returned. If {@code t} is 0.25, a quarter of the way between the two values is returned. And so
     * on.
     *
     * @param b the first value
     * @param d the second value
     * @param t the interpolation parameter
     * @return the interpolated value
     */
    public static float lerp(float b, float d, float t) {
        return b + (d - b) * clamp(t, 0, 1);
    }

    /**
     * Linearly interpolates between two values. The {@code t} parameter is clamped to the range {@code [0, 1]}.
     * <p>
     * Linear interpolation is a mathematical operation that finds a value between two values. If {@code t} is 0, the
     * first value is returned. If {@code t} is 1, the second value is returned. If {@code t} is 0.5, the average of the
     * two values is returned. If {@code t} is 0.25, a quarter of the way between the two values is returned. And so
     * on.
     *
     * @param b the first value
     * @param d the second value
     * @param t the interpolation parameter
     * @return the interpolated value
     */
    public static double lerp(double b, double d, double t) {
        return b + (d - b) * clamp(t, 0, 1);
    }

}
