package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.BiParamFunc;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.io.datachannel.AbstractSeekableDataChannel;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.MemoryBufferChannel;
import cc.fluse.ulib.core.io.channel.SeekableDataChannel;
import cc.fluse.ulib.core.util.Calculus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.Map;
import java.util.TreeSet;

import static cc.fluse.ulib.core.io.channel.MemoryBufferChannel.UndefinedRegionReadStrategy.*;

public class MemoryBufferChannelImpl extends AbstractSeekableDataChannel implements MemoryBufferChannel {

    // - static helpers -
    /**
     * Maximum size a region can have. This is the next lower step of the internal buffer capacity. This is necessary to
     * ensure regions will always have a capacity that is a multiple of the internal buffer capacity.
     */
    private static final int MAX_REGION_SIZE = Calculus.step(Integer.MAX_VALUE, Internal.getDefaultBufferCapacity(), false);

    private interface Region extends Comparable<Region> {
        /**
         * The absolute start position of the region (inclusive).
         *
         * @return the start position
         */
        long start();

        /**
         * The absolute end position of the region (inclusive) for the defined (written) or undefined (not written)
         * bytes.
         *
         * @param defined {@code true} for the defined (written) bytes, {@code false} for the undefined (not written)
         * @return the end position
         */
        default long end(boolean defined) {
            return start() + size(defined);
        }

        /**
         * The size of the region in bytes for the defined (written) or undefined (not written) bytes.
         *
         * @param defined {@code true} for the defined (written) bytes, {@code false} for the undefined (not written)
         * @return the size
         */
        default long size(boolean defined) {
            return (defined ? buffer().limit() : buffer().capacity());
        }

        /**
         * The underlying buffer of the region. The buffer's limit is set to the end of the defined (written) bytes. The
         * position of the buffer is undefined and must not be assumed to be anything sensible.
         *
         * @return the buffer
         */
        @NotNull ByteBuffer buffer();

        /**
         * Checks if this region contains the given absolute position for the defined (written) or undefined (not
         * written) bytes.
         *
         * @param position the position
         * @param defined  {@code true} for the defined (written) bytes, {@code false} for the undefined (not written)
         * @return {@code true} if the position is contained, {@code false} otherwise
         */
        default boolean contains(long position, boolean defined) {
            return position >= start() && position <= end(defined);
        }

        @Override
        default int compareTo(@NotNull MemoryBufferChannelImpl.Region o) {
            return Long.compare(start(), o.start());
        }
    }

    /**
     * Immutable region.
     */
    private record MemoryRegion(long start, ByteBuffer buffer) implements Region {
    }

    /**
     * Mutable dummy region that does not contain any data. It is used for navigation.
     */
    private static class DummyRegion implements Region {
        private long start;
        private long capacity;

        private DummyRegion(long start, long capacity) {
            this.start = start;
            this.capacity = capacity;
        }

        public DummyRegion update(long start, long capacity) {
            this.start = start;
            this.capacity = capacity;
            return this;
        }

        public DummyRegion pos(long start) {
            this.start = start;
            return this;
        }

        @Override
        public long start() {
            return start;
        }

        @Override
        public long end(boolean defined) {
            return start + capacity; // `defined` is ignored
        }

        @Override
        public @NotNull ByteBuffer buffer() {
            throw new UnsupportedOperationException();
        }
    }

    private static final Map<UndefinedRegionReadStrategy, BiParamFunc<ByteBuffer, MemoryBufferChannelImpl, Integer, ClosedChannelException>> ADAPTERS = Map.of(
        EOF, (dst, ch) -> -1,
        ZERO, (dst, ch) -> 0,
        THROW, (dst, ch) -> {
            throw new UnsupportedOperationException("Undefined region for position " + ch.position);
        },
        SIMULATE_DEFINED, (dst, ch) -> { // fill the buffer with zeros
            var pos = dst.position();
            while (dst.hasRemaining()) {
                dst.put((byte) 0);
            }
            var read = dst.position() - pos;
            ch.position(ch.position + read);
            return read;
        },
        EOF_SKIP, (dst, ch) -> { // skip to the next defined region
            ch.skipToNextRegion(0);
            return -1;
        },
        ZERO_SKIP, (dst, ch) -> { // skip to the next defined region
            ch.skipToNextRegion(0);
            return 0;
        },
        SKIP_N, (dst, ch) -> Math.toIntExact(ch.skipToNextRegion(Integer.MAX_VALUE)),
        SKIP, (dst, ch) -> {
            var skipped = ch.skipToNextRegion(0);
            return skipped <= Integer.MAX_VALUE ? Math.toIntExact(skipped) : Integer.MIN_VALUE;
        },
        THROW_SKIP, (dst, ch) -> {
            ch.skipToNextRegion(0);
            throw new UnsupportedOperationException("Undefined region for position " + ch.position);
        }
    );

    // - end of static helpers -

    private final UndefinedRegionReadStrategy undefinedRegionReadStrategy;
    /**
     * Tree of regions, sorted by start position. The general contract is that each regions buffer has its limit set to
     * the end of the defined (written) bytes. The position of the buffer is undefined and must not be assumed to be
     * anything sensible.
     */
    private final TreeSet<Region> regions = new TreeSet<>();
    private final DummyRegion dummyRegion = new DummyRegion(0, 0); // used for navigating the regions

    private Region currentRegion = null;
    private long limit; // max size
    private long position; // current position
    private long size; // current size, always <= limit, < 0 if not calculated
    private long memory; // real occupied memory, < 0 if not calculated

    public MemoryBufferChannelImpl(long limit, UndefinedRegionReadStrategy undefinedRegionReadStrategy) {
        this.limit = limit > 0 ? limit : Long.MAX_VALUE;
        this.undefinedRegionReadStrategy = undefinedRegionReadStrategy;
    }

    @Override
    public @NotNull SeekableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    /**
     * Changes the current position to the next defined region.
     *
     * @param limit the limit to stop at, of >= 0 for no limit
     * @return the number of bytes skipped
     * @throws ClosedChannelException if the channel is closed
     */
    private long skipToNextRegion(long limit) throws ClosedChannelException {
        var next = regions.higher(dummyRegion.pos(position));
        var pos = position;
        position(Math.min(next == null ? size() : next.start(), limit > 0 ? limit : Long.MAX_VALUE));
        return position - pos;
    }

    /**
     * Calculates the capacity for an allocation. The capacity is the next largest multiple of the default buffer
     * capacity that is at least the default buffer capacity and at most the remaining space. However, the returned
     * capacity is will be never larger than the bytes to the next region boundary.
     * <p>
     * <b>Note:</b> This method is dependent on the current position.
     *
     * @param desired the desired capacity
     * @return the capacity
     */
    private int capacity(int desired) {
        var nextLargestCapacity = Calculus.limitToInt(Calculus.step(desired, Internal.getDefaultBufferCapacity(), true));
        var nextRegion = regions.higher(dummyRegion.update(position, 0));
        // alloc up to the next region boundary
        var allocSize = nextRegion == null ? nextLargestCapacity : (int) Math.min(nextRegion.start() - position, nextLargestCapacity);
        // alloc up to the max alloc cap
        return Math.min(Math.min(allocSize, MAX_REGION_SIZE), /*max alloc cap*/ (int) Math.min(limit - position, Integer.MAX_VALUE));
    }

    /**
     * Creates a new region at the current position with the given additional intended capacity. The current data is
     * copied to the new region. The given region will be replaced within the regions tree. Even though this method
     * attempts to create the new region with at least the old capacity plus the intended additional capacity, the new
     * region may have less new capacity. The actual additional capacity can be determined by comparing the old region
     * object with the new one.
     * <p>
     * This method may fail to allocate any addition buffer, thus the returned region may be the same as the given
     * region.
     * <p>
     * <b>Note:</b> This method is dependent on the current position.
     *
     * @param region the region to enlarge
     * @param intent the intended amount of bytes to enlarge the region by
     * @return the new region
     */
    private @NotNull Region enlarge(@NotNull Region region, int intent) {
        var newCapacity = capacity(Calculus.limitToInt(region.size(false) + intent));
        if (newCapacity == region.buffer().capacity()) return region; // no need to enlarge
        if (newCapacity < region.buffer().capacity())
            throw new UndefinedStateError("New capacity %d is smaller than old capacity %d".formatted(newCapacity, region.buffer().capacity()));

        var buf = Internal.bufalloc(newCapacity);
        // copy the data
        region.buffer().position(0);
        buf.put(region.buffer());
        buf.limit(buf.position()); // set the limit to the end of the defined bytes

        // replace the region
        regions.remove(region);
        var newRegion = new MemoryRegion(region.start(), buf);
        regions.add(newRegion);

        // TODO: check for adjacent regions and merge them
        // tryMerge(region);

        // invalidate occupied memory
        this.memory = -1;

        return newRegion;
    }

    /**
     * Gets the region that contains the current position. If the intent is > 0, this method will attempt to create a
     * new region or enlarge an existing region to fit the intent. If the intent is <= 0, this method will return
     * {@code null} if there is no region that contains the current position.
     * <p>
     * The only case where this method returns a {@code null} value, is when the intent is <= 0 and there is no region
     * that contains the current position or when the intent is > 0 and no space is left for allocation of a new
     * region.
     * <p>
     * <b>Note:</b> This method is dependent on the current position.
     *
     * @param intent the intended amount of bytes to write, <= 0 for a read intent
     */
    private @Nullable Region getRegion(int intent) {
        var region = currentRegion != null && currentRegion.contains(position, false)
            ? currentRegion
            : (currentRegion = regions.floor(dummyRegion.pos(position)));

        if (intent <= 0) {
            // read intent
            if (region != null && region.contains(position, true)) return region;
            return null; // no region with readable bytes found
        }
        // write intent

        // check if position is within defined area
        if (region != null && region.contains(position, true)) {
            // position is within defined area
            // check if region is large enough for write intent

            if (region.contains(position + intent, false)) {
                // region is large enough for write intent
                return region;
            }

            return this.currentRegion = enlarge(region, intent);
        }

        // position is not within defined area, create new region
        var cap = capacity(intent);
        if (cap == 0) return null; // no space left
        var newRegion = new MemoryRegion(position, Internal.bufalloc(cap).limit(0));
        regions.add(newRegion);

        // invalidate occupied memory
        this.memory = -1;

        return this.currentRegion = newRegion;
    }

    @Override
    public int read(@NotNull ByteBuffer dst) throws ClosedChannelException {
        bouncer.pass();
        // edge cases
        if (position >= size) return -1;
        if (!dst.hasRemaining()) return 0;

        // get the region
        var region = getRegion(0);
        if (region == null)
            return UndefinedStateError.ensureNotNull(ADAPTERS.get(undefinedRegionReadStrategy), "No adapter for undefined region read strategy %s".formatted(undefinedRegionReadStrategy))
                .execute(dst, this); // undefined region

        // get the slice
        var buffer = region.buffer();
        var bufferOffset = Math.toIntExact(position - region.start());
        var slice = buffer.slice(bufferOffset, Math.min(buffer.limit() - bufferOffset, dst.remaining()));

        // read from the region
        dst.put(slice);
        var read = slice.position();
        position(position + read);
        return read;
    }

    @Override
    public int write(@NotNull ByteBuffer src) throws ClosedChannelException {
        bouncer.pass();
        // edge cases
        if (!src.hasRemaining()) return 0;


        Region region;
        if (position >= limit || (region = getRegion(src.remaining())) == null) {
            // silently discard
            src.position(src.limit());
            return 0;
        }

        // prepare to write
        var buffer = region.buffer();
        var bufferOffset = Math.toIntExact(position - region.start());
        var toWrite = Math.min(src.remaining(), buffer.capacity() - bufferOffset);
        if (buffer.limit() - bufferOffset < toWrite) // enlarge limit if necessary to fit the bytes to be written
            buffer.limit(bufferOffset + toWrite);
        var slice = buffer.slice(bufferOffset, toWrite);

        // write
        slice.put(src);
        var written = slice.position();
        position(position + written);

        buffer.limit(bufferOffset + written); // update defined bytes
        this.size = -1; // invalidate size

        return written;
    }

    @Override
    public @NotNull MemoryBufferChannel truncate(long size) throws ClosedChannelException {
        bouncer.pass();
        if (size < 0 || size == limit) return this; // ignore


        if (size < limit) {
            // discard regions/data
            Region region;
            while ((region = regions.last()) != null && region.end(false) > size) {
                // check if regions needs to be shrunk or discarded completely
                if (region.start() >= size) {
                    // discard region
                    regions.remove(region);
                    continue;
                }

                // create shrunken buf and copy data
                // no need to check for MAX_REGION_SIZE because we already checked for size < limit and this is
                // guaranteed to be always smaller than MAX_REGION_SIZE
                var buf = Internal.bufalloc(Math.toIntExact(size - region.start()));
                region.buffer().position(0);
                buf.put(region.buffer().slice(0, buf.capacity()));

                // replace region
                regions.remove(region);
                regions.add(new MemoryRegion(region.start(), buf));
                break; // when shrinking, we can stop here because there won't be more regions to discard

            }

            this.size = -1; // invalidate size
            this.memory = -1; // invalidate memory
        }

        this.limit = size;
        if (position > limit) position(limit);

        return this;
    }

    @Override
    public long size() throws ClosedChannelException {
        bouncer.pass();
        if (size >= 0) return size; // cached
        // re-calculate and cache
        return this.size = regions.last().end(true);
    }

    @Override
    public long memory() throws ClosedChannelException {
        bouncer.pass();
        if (memory >= 0) return memory; // cached
        // re-calculate and cache
        return this.memory = regions.stream().mapToLong(r -> r.buffer().capacity()).sum();
    }

    @Override
    public long position() throws ClosedChannelException {
        bouncer.pass();
        return position;
    }

    @Override
    public @NotNull MemoryBufferChannel position(long newPosition) throws ClosedChannelException {
        bouncer.pass();
        if (newPosition >= 0) {
            this.position = newPosition;
            // invalidate current region if necessary
            if (currentRegion != null && !currentRegion.contains(newPosition, false)) this.currentRegion = null;
        }
        return this;
    }


    @Override
    public void close() throws IOException {
        if (!bouncer.canPass()) return;
        super.close();
        regions.clear();
    }


}
