package cc.fluse.ulib.core.database.sql.query;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.Table;
import org.jetbrains.annotations.NotNull;

/**
 * The start of a query that requires additional limitation.
 *
 * @see Table#select(String, String...)
 * @see Table#selectDistinct(String, String...)
 */
public interface QueryStart {
    /**
     * Limits the query using the sql {@code WHERE} keyword.
     *
     * @param column the column
     * @return the condition builder to complete the condition
     */
    @NotNull
    Condition<Where> where(@NotNull Column<?> column);

    /**
     * Limits the query using the sql {@code WHERE} keyword.
     *
     * @param column the column
     * @return the condition builder to complete the condition
     */
    @NotNull
    Condition<Where> where(@NotNull String column);

    /**
     * Limits the query using the sql {@code WHERE} keyword.
     *
     * @param condition the whole condition string, e.g. "column_1 = 0"
     * @return this
     */
    @NotNull
    Where whereRaw(@NotNull String condition);
}
