package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.WritableDataChannel;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ClosedChannelException;

/**
 * Abstract base class for {@link WritableDataChannel}s.
 *
 * @implSpec Implementations must override {@link #isOpen()} and {@link #close()}.
 */
public abstract class AbstractWritableDataChannel implements WritableDataChannel {

    protected final ChBouncer bouncer = Bouncer.ch();
    protected final DataOutputStream dout;

    protected AbstractWritableDataChannel() {
        this.dout = new DataOutputStream(Channels.newOutputStream(this));
    }

    @Override
    @MustBeInvokedByOverriders
    public boolean isOpen() {
        return bouncer.canPass();
    }

    @Override
    @MustBeInvokedByOverriders
    public void close() throws IOException {
        if (!bouncer.canPass()) return;
        bouncer.close();
        dout.flush();
    }

    @Override
    public @NotNull OutputStream streamWrite() throws ClosedChannelException {
        bouncer.pass();
        return IOUtil.isolate(dout);
    }

    @Override
    public @NotNull WritableDataChannel channelWrite() throws IOException {
        bouncer.pass();
        return WritableDataChannel.super.channelWrite();
    }

    @Override
    public @NotNull WritableDataChannel channel() throws IOException {
        bouncer.pass();
        return WritableDataChannel.super.channel();
    }

    // delegate DataOutput

    @Override
    public void write(int b) throws IOException {
        bouncer.pass();
        dout.write(b);
    }

    @Override
    public void write(@NotNull byte[] b) throws IOException {
        bouncer.pass();
        dout.write(b);
    }

    @Override
    public void write(@NotNull byte[] b, int off, int len) throws IOException {
        bouncer.pass();
        dout.write(b, off, len);
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {
        bouncer.pass();
        dout.writeBoolean(v);
    }

    @Override
    public void writeByte(int v) throws IOException {
        bouncer.pass();
        dout.writeByte(v);
    }

    @Override
    public void writeShort(int v) throws IOException {
        bouncer.pass();
        dout.writeShort(v);
    }

    @Override
    public void writeChar(int v) throws IOException {
        bouncer.pass();
        dout.writeChar(v);
    }

    @Override
    public void writeInt(int v) throws IOException {
        bouncer.pass();
        dout.writeInt(v);
    }

    @Override
    public void writeLong(long v) throws IOException {
        bouncer.pass();
        dout.writeLong(v);
    }

    @Override
    public void writeFloat(float v) throws IOException {
        bouncer.pass();
        dout.writeFloat(v);
    }

    @Override
    public void writeDouble(double v) throws IOException {
        bouncer.pass();
        dout.writeDouble(v);
    }

    @Override
    public void writeBytes(@NotNull String s) throws IOException {
        bouncer.pass();
        dout.writeBytes(s);
    }

    @Override
    public void writeChars(@NotNull String s) throws IOException {
        bouncer.pass();
        dout.writeChars(s);
    }

    @Override
    public void writeUTF(@NotNull String s) throws IOException {
        bouncer.pass();
        dout.writeUTF(s);
    }
}
