package cc.fluse.ulib.core.cli;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Defines how many arguments an option accepts.
 */
public record ArgNum(int min, int max) {
    /**
     * No arguments at all allowed.
     */
    public static final ArgNum NO_ARGS = new ArgNum(0, 0);

    /**
     * At most one optional argument may be supplied.
     */
    public static final ArgNum OPT_ARG = new ArgNum(0, 1);

    /**
     * Any number of optional arguments may be supplied.
     */
    public static final ArgNum OPT_ARGS = new ArgNum(0, Integer.MAX_VALUE);

    /**
     * Exactly one argument must be supplied.
     */
    public static final ArgNum REQ_ARG = new ArgNum(1, 1);

    /**
     * At least one argument must be supplied.
     */
    public static final ArgNum REQ_ARG1 = new ArgNum(1, Integer.MAX_VALUE);
    /**
     * At least two arguments must be supplied.
     */
    public static final ArgNum REQ_ARG2 = new ArgNum(2, Integer.MAX_VALUE);

    public ArgNum {
        if (min < 0) throw new IllegalArgumentException("min < 0: %d < 0 ".formatted(min));
        if (max < min) throw new IllegalArgumentException("max < min: %d < %d".formatted(max, min));
    }

    @NotNull
    public ArgProps withDefaults(@NotNull String @NotNull ... defaults) {
        return new ArgProps(this, List.of(defaults));
    }
}
