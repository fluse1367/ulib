package cc.fluse.ulib.core.impl.database.sql.postgres;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.PostgreSQLDatabase;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase.DBImpl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Properties;

@DBImpl(names = {"postgresql", "postgres", "pgsql", "pg"},
        driverCoords = "{{maven.postgres}}",
        protocol = "jdbc:postgresql://",
        supportsConnectionPooling = true,
        quoteIdentifierFormat = "\"%s\"")
public class PostgreSQLDatabaseImpl extends AbstractSqlDatabase implements PostgreSQLDatabase {

    public PostgreSQLDatabaseImpl(Connection connection) {
        super(connection);
    }

    public PostgreSQLDatabaseImpl(String url, Properties info, int maxPoolSize) {
        super(url, info, maxPoolSize);
    }

    @Override
    protected Table newTable(String name, Collection<Column<?>> columns) {
        return new PostgreSQLTable(this, name, columns);
    }
}
