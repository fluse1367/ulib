package cc.fluse.ulib.core.impl.configuration;

import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.util.Conditions;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KeySections implements KeyPath {

    private final List<String> sections;

    public KeySections(List<String> sections) {
        this.sections = Collections.unmodifiableList(sections);
    }

    public KeySections(String path, String delimiter, String escapeSequence) {
        int dLen = delimiter.length();
        var sections = new LinkedList<String>();

        for (int start = 0, searchIndex = 0, end, len = path.length();
             (end = path.indexOf(delimiter, searchIndex)) <= len;
             searchIndex = end + dLen) {

            if (end < 0) {
                // no more delimiter occurrences found, add rest to a new section and break loop
                sections.add(path.substring(start, len));
                break;
            }

            // found delimiter sequence at position `end`
            if (Conditions.isEscaped(path, end, escapeSequence)) {
                // delimiter is escaped, just move on
                continue;
            }

            // delimiter not escaped!
            sections.add(path.substring(start, end));
            start = end + dLen;
        }

        this.sections = List.copyOf(sections);
    }

    @Override
    public @NotNull Iterator<String> iterator() {
        return sections.iterator();
    }

    @Override
    public Spliterator<String> spliterator() {
        return sections.spliterator();
    }

    @Override
    public @NotNull List<String> getSections() {
        return this.sections;
    }

    @Override
    public @NotNull KeyPath chain(@NotNull KeyPath @NotNull ... others) {

        // resolve sections
        var sections = Stream.of(others)
                             .map(KeyPath::getSections)
                             .flatMap(List::stream)
                             .toList();


        return new KeySections(sections);
    }

    @Override
    public @NotNull String toLegacyString(@NotNull String delimiter, @NotNull String escapeSequence) {
        return sections.stream()
                       .map(section -> section
                               .replace(escapeSequence, escapeSequence + escapeSequence)
                               .replace(delimiter, escapeSequence + delimiter)
                       )
                       .collect(Collectors.joining(delimiter));
    }

    @Override
    public String toString() {
        return "KeyPath=".concat(Arrays.toString(this.sections.toArray()));
    }
}
