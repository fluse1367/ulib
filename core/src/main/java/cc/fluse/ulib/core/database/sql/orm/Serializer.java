package cc.fluse.ulib.core.database.sql.orm;

import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.impl.database.sql.orm.StringSerializer;
import lombok.experimental.StandardException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

/**
 * A serializer that can serialize and deserialize objects of type {@code T} to objects of type {@code S}.
 *
 * @param <T> The type of the object to serialize.
 * @param <S> The type of the serialized object, usually a primitive type or {@link String}.
 */
public interface Serializer<T, S> {

    /**
     * Creates a new serializer that serializes and deserializes objects of the given type to and from strings using the
     * given serializer and deserializer functions.
     *
     * @param type         The type of the object to serialize.
     * @param serializer   The serializer function. Will be called with a non-null object of type {@code T} and must
     *                     return a non-null object of type {@code S}.
     * @param deserializer The deserializer function. Will be called with a non-null object of type {@code S} and must
     *                     return a non-null object of type {@code T}.
     * @param <T>          The type of the object to serialize.
     * @return The new serializer.
     */
    static <T> @NotNull Serializer<T, String> create(@NotNull Class<T> type,
                                                     @NotNull ParamFunc<? super @NotNull T, ? extends @NotNull String, ? extends SerializerException> serializer,
                                                     @NotNull ParamFunc<? super @NotNull String, ? extends @NotNull T, ? extends SerializerException> deserializer) {
        return new StringSerializer<>(type, serializer, deserializer);
    }


    /**
     * Gets the type of the object to serialize.
     *
     * @return The type of the object to serialize.
     */
    @NotNull Class<T> getJavaType();

    /**
     * Gets the type of the serialized object.
     *
     * @return The type of the serialized object.
     */
    @NotNull Class<S> getSerializedType();

    /**
     * Serializes the given object. The object must be of the type returned by {@link #getJavaType()}. The returned
     * object must be of the type returned by {@link #getSerializedType()}.
     * <p>
     * The general contract of this method is that {@code deserialize(serialize(object))} must return an object that is
     * equal to {@code object} and vice versa. Also passing in a {@code null} object must return {@code null}, and
     * passing in a non-null object must return a non-null object.
     *
     * @param object The object to serialize.
     * @return The serialized object.
     * @throws SerializerException If an error occurs while serializing the object.
     */
    @Contract("null -> null; !null -> !null")
    @Nullable S serialize(@Nullable T object) throws SerializerException;

    /**
     * Deserializes the given object. The object must be of the type returned by {@link #getSerializedType()}. The
     * returned object must be of the type returned by {@link #getJavaType()}.
     * <p>
     * The general contract of this method is that {@code serialize(deserialize(serialized))} must return an object that
     * is equal to {@code serialized} and vice versa. Also passing in a {@code null} object must return {@code null},
     * and passing in a non-null object must return a non-null object.
     *
     * @param serialized The serialized object.
     * @return The deserialized object.
     * @throws SerializerException If an error occurs while deserializing the object.
     */
    @Contract("null -> null; !null -> !null")
    @Nullable T deserialize(@Nullable S serialized) throws SerializerException;

    @StandardException
    @BypassAnnotationEnforcement
    class SerializerException extends IOException {}
}
