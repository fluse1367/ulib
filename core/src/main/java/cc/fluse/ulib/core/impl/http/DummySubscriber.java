package cc.fluse.ulib.core.impl.http;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Flow;

public class DummySubscriber<T> implements Flow.Subscriber<T> {

    private final CompletableFuture<Queue<T>> future = new CompletableFuture<>();
    private final Queue<T> items = new LinkedList<>();

    public CompletableFuture<Queue<T>> get() {
        return future;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        // request all data
        subscription.request(Long.MAX_VALUE);
    }

    @Override
    public void onNext(T item) {
        items.add(item);
    }

    @Override
    public void onError(Throwable throwable) {
        future.completeExceptionally(throwable);
    }

    @Override
    public void onComplete() {
        future.complete(items);
    }
}
