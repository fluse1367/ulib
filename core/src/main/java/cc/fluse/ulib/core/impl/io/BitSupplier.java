package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.function.Func;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.EOFException;
import java.util.Optional;

public final class BitSupplier<X extends Exception> implements Func<Boolean, X> {

    private final Func<Integer, X> nextByteSupplier;
    private Byte word;
    private int cursor;

    public BitSupplier(@NotNull Func<Integer, X> nextByteSupplier) {
        this.nextByteSupplier = nextByteSupplier;
    }

    public boolean isNextBitSet() throws X, EOFException {
        return Optional.ofNullable(getNextBit()).orElseThrow(EOFException::new);
    }

    @Nullable
    public Boolean getNextBit() throws X {
        // obtain next word
        if (!hasRemaining()) {
            var b = (int) nextByteSupplier.execute();
            if (b == -1) {
                return null;
            }
            word = (byte) b;
            cursor = 0;
        }

        return (word & 1 << cursor++) != 0;
    }

    public boolean hasRemaining() {
        return word != null && cursor < 8;
    }

    @Override
    @Nullable
    public Boolean execute() throws X {
        return getNextBit();
    }
}
