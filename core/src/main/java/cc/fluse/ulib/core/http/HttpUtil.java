package cc.fluse.ulib.core.http;

import cc.fluse.ulib.core.impl.http.DummySubscriber;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.URI;
import java.net.http.*;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.*;

import static java.net.URLEncoder.encode;
import static java.net.http.HttpResponse.BodyHandlers.ofInputStream;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * A class to quickly send simple POST and GET requests.
 */
public final class HttpUtil {
    private static final HttpRequestExecutor DEFAULT_EXECUTOR = HttpRequestExecutor.nop(HttpClient.newHttpClient());

    /**
     * Sends a {@code application/x-www-form-urlencoded} POST request using the default http client. The key-value
     * fields will be appended to in an url encoded format.
     *
     * @param uri    the uri
     * @param fields the key-value fields to send
     * @return the response
     *
     * @see #POST(URI, Map, HttpRequestExecutor)
     * @see Map#of(Object, Object)
     */
    public static @NotNull HttpResponse<InputStream> POST(@NotNull URI uri, @NotNull Map<String, String> fields)
    throws IOException, InterruptedException {
        return POST(uri, fields, DEFAULT_EXECUTOR);
    }

    /**
     * Sends a {@code application/x-www-form-urlencoded} POST request using the given http client. The key-value fields
     * will be appended to in an url encoded format.
     *
     * @param uri    the uri
     * @param fields the key-value fields to send
     * @param client the client to send the request with
     * @return the response
     *
     * @see Map#of(Object, Object)
     */
    @NotNull
    public static HttpResponse<InputStream> POST(@NotNull URI uri, @NotNull Map<String, String> fields,
                                                 @NotNull HttpRequestExecutor client)
    throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder(uri)
                                 .POST(HttpRequest.BodyPublishers.ofByteArray(formUrlEncode(fields).getBytes(UTF_8)))
                                 .header("Content-Type", "application/x-www-form-urlencoded")
                                 .build();

        return client.send(request, ofInputStream());
    }

    /**
     * Sends a GET request using the default http client. The key-value fields will be appended to the base uri in an
     * url encoded format.
     *
     * @param baseUri the base uri
     * @param fields  the key-value fields to send.
     * @return the response
     *
     * @see Map#of(Object, Object)
     */
    public static @NotNull HttpResponse<InputStream> GET(@NotNull URI baseUri, @NotNull Map<String, String> fields)
    throws IOException, InterruptedException {
        return GET(baseUri, fields, DEFAULT_EXECUTOR);
    }

    /**
     * Sends a GET request using the given http client. The key-value fields will be appended to the base uri in an url
     * encoded format.
     *
     * @param baseUri the base uri
     * @param fields  the key-value fields to send.
     * @param client  the client to send the request with
     * @return the response
     *
     * @see Map#of(Object, Object)
     */
    public static @NotNull HttpResponse<InputStream> GET(@NotNull URI baseUri, @NotNull Map<String, String> fields,
                                                         @NotNull HttpRequestExecutor client)
    throws IOException, InterruptedException {
        var connect = baseUri.toString().endsWith("/") ? "?" : "/?";
        var uri = baseUri.resolve(connect.concat(formUrlEncode(fields)));
        return GET(uri, client);
    }

    /**
     * Sends a GET request using the default http client.
     *
     * @param uri the uri
     * @return the response
     *
     * @see #GET(URI, HttpRequestExecutor)
     */
    public static @NotNull HttpResponse<InputStream> GET(@NotNull URI uri)
    throws IOException, InterruptedException {
        return GET(uri, DEFAULT_EXECUTOR);
    }

    /**
     * Sends a GET request using the given http client.
     *
     * @param uri    the uri
     * @param client the client to send the request with
     * @return the response
     */
    public static @NotNull HttpResponse<InputStream> GET(@NotNull URI uri,
                                                         @NotNull HttpRequestExecutor client)
    throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder(uri).GET().build();
        return client.send(request, ofInputStream());
    }

    /**
     * Encodes a key-value map into the {@code x-www-form-urlencoded} standard.
     *
     * @param fields the key-value map
     * @return the url encoded string
     *
     * @see <a href="https://url.spec.whatwg.org/#concept-urlencoded">URL Standard reference</a>
     */
    @NotNull
    public static String formUrlEncode(@NotNull Map<String, String> fields) {
        var sj = new StringJoiner("&");
        fields.forEach((k, v) -> sj.add("%s=%s".formatted(encode(k, UTF_8), encode(v, UTF_8))));
        return sj.toString();
    }

    /**
     * Fetches the raw body data from a http request.
     *
     * @param request the http request to fetch the body from
     * @return the body
     */
    @NotNull
    public static Optional<byte[]> obtainRequestBody(@NotNull HttpRequest request) {
        return request.bodyPublisher()
                      .map(bp -> {
                          var dummy = new DummySubscriber<ByteBuffer>();
                          bp.subscribe(dummy);

                          var items = dummy.get().join();

                          return items.stream()
                                      .collect(ByteArrayOutputStream::new, (out, buf) -> {
                                          while (buf.hasRemaining()) {
                                              out.write(buf.get());
                                          }
                                      }, (out, out2) -> out.writeBytes(out2.toByteArray()))
                                      .toByteArray();
                      });
    }

    /**
     * Reads the body data from a http request and writes it to a channel-
     *
     * @param request the request to read the body from
     * @param channel the channel to write the body to
     * @throws IOException see {@link WritableByteChannel#write(ByteBuffer) }
     */
    public static void obtainRequestBody(@NotNull HttpRequest request, @NotNull WritableByteChannel channel)
    throws IOException {
        var pub = request.bodyPublisher().orElse(null);
        if (pub == null) return;

        var dummy = new DummySubscriber<ByteBuffer>();
        pub.subscribe(dummy);

        for (ByteBuffer buf : dummy.get().join()) {
            channel.write(buf);
        }
    }
}
