package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

/**
 * A {@link ReadableDataChannel} that delegates all methods to a source channel.
 * <p>
 * This class is useful for wrapping a channel that does not implement the {@link ReadableDataChannel} interface.
 * <p>
 * This class is not thread-safe.
 */
public class ReadableDataChannelDelegate extends AbstractReadableDataChannel {

    private final ReadableByteChannel channel;

    public ReadableDataChannelDelegate(ReadableByteChannel channel) {
        this.channel = channel;
    }

    @Override
    public int read(@NotNull ByteBuffer dst) throws IOException {
        bouncer.pass();
        return channel.read(dst);
    }

    @Override
    public boolean isOpen() {
        return channel.isOpen() && super.isOpen();
    }

    @Override
    public void close() throws IOException {
        channel.close();
        super.close();
    }
}
