package cc.fluse.ulib.core.impl.database.sql.postgres;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.*;

public class PostgreSQLTable extends AbstractSqlTable {

    PostgreSQLTable(AbstractSqlDatabase sql, String name, Collection<Column<?>> columns) {
        super(sql, name, columns);
    }

    @Override
    public boolean exists() {
        try (var e = sql.getConnection();
             var st = e.get().prepareStatement("select exists (select from pg_tables where tablename = ?)")) {
            st.setString(1, name);
            var res = st.executeQuery();
            return res.next() && res.getBoolean("exists");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void create() throws SQLException {
        var dataTypeOverwrites = new HashMap<Column<?>, String>();
        var columnPropertiesOverwrites = new HashMap<Column<?>, Map<String, Object>>();

        // create enum types
        for (var col : columns.values()) {
            if (col.getDataType() != DataType.ENUM) continue;

            // overwrite properties so that the column is not treated as enum
            columnPropertiesOverwrites.computeIfAbsent(col, $ -> new HashMap<>()).put("isEnum", false);

            var typeName = "enum_%s".formatted(col.getName());
            dataTypeOverwrites.put(col, typeName);

            var values = new StringJoiner(", ", "(", ")");
            values.setEmptyValue("");
            for (var value : col.getAcceptable()) {
                values.add(sql.quote(value, false));
            }

            try (var e = sql.getConnection();
                 var st = e.get().prepareStatement("create type %s as enum %s".formatted(sql.quote(typeName, true), values))) {
                st.execute();
            }
        }

        // map auto increment columns to serial types
        for (var col : columns.values()) {
            if (!col.isAutoIncrement()) continue;

            dataTypeOverwrites.put(col, col.getDataType() == DataType.BIGINT ? "bigserial" : "serial");
            columnPropertiesOverwrites.computeIfAbsent(col, $ -> new HashMap<>()).put("isAutoIncrement", false);
        }

        super.create(Map.of(
                DataType.TINYBLOB, "bytea",
                DataType.BLOB, "bytea",
                DataType.MEDIUMBLOB, "bytea",
                DataType.LONGBLOB, "bytea"
        ), dataTypeOverwrites, Collections.emptyMap(), columnPropertiesOverwrites);
    }

    @Override
    protected String insert_query_template(boolean allValues) {
        return super.insert_query_template(allValues)
                + getPrimaryKey()
                .map(pk -> " returning %s".formatted(sql.quote(pk.getName(), true)))
                .orElse("");
    }

    @Override
    public boolean insert(@NotNull Object v, Object... vs) {
        return super.insert(v, vs);
    }
}
