package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import org.jetbrains.annotations.NotNull;

import java.nio.channels.Channel;
import java.nio.channels.ClosedChannelException;

/**
 * A class representing a channel with transforming capabilities by chaining {@link ByteTransformer}s.
 */
public interface TransformerChainChannel extends Channel {

    /**
     * Adds a transformer to the chain.
     *
     * @param transformer the transformer
     */
    void addTransformer(@NotNull ByteTransformer transformer) throws ClosedChannelException;

    /**
     * Removes a transformer from the chain.
     * <p>
     * <b>Note:</b> This can cause inconsistencies in the chain if it already has processed some data.
     *
     * @param transformer the transformer
     */
    void removeTransformer(@NotNull ByteTransformer transformer) throws ClosedChannelException;

    /**
     * Determines if a certain transformer is present in the chain.
     *
     * @param transformer the transformer
     * @return {@code true} if present, {@code false} otherwise
     */
    boolean hasTransformer(@NotNull ByteTransformer transformer) throws ClosedChannelException;

    /**
     * Attempts to complete the channel. Each individual transformer of the chain is completed in the order they were
     * added.
     *
     * @throws IOTransformException   if a transformer throws an exception
     * @throws ClosedChannelException if the channel is closed
     * @see ByteTransformer#complete()
     */
    void complete() throws IOTransformException, ClosedChannelException;

    /**
     * Determines if the channel is completed.
     *
     * @return {@code true} if completed, {@code false} otherwise
     */
    boolean isCompleted();
}
