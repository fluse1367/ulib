package cc.fluse.ulib.core.impl.reflect;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.reflect.CallFrame;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Pattern;

import static cc.fluse.ulib.core.util.Semantics.nofail;

public final class ReflectSupport {
    public static Method accessible(Method method) {
        return ReflectUtil.doPrivileged(() -> {
            method.setAccessible(true);
            return method;
        });
    }

    public static Method accessible(Method method, Object invoke) {
        return ReflectUtil.doPrivileged(() -> {
            if (!method.canAccess(invoke)) method.setAccessible(true);
            return method;
        });
    }

    public static Field accessible(Field field) {
        return ReflectUtil.doPrivileged(() -> {
            field.setAccessible(true);
            return field;
        });
    }

    public static Field accessible(Field field, Object invoke) {
        return ReflectUtil.doPrivileged(() -> {
            if (!field.canAccess(invoke)) field.setAccessible(true);
            return field;
        });
    }

    public static Class<?>[] toParameterTypes(List<Param<?>> params) {
        return params.stream()
                .map(Param::getClazz)
                .toArray(Class[]::new);
    }

    public static Object[] toParameterObjects(List<Param<?>> params) {
        return params.stream()
                .map(Param::getValue)
                .toArray();
    }

    public static Pair<Class<?>, Object> frameCall(CallFrame frame, Class<?> clazz, Object invoke)
    throws ReflectiveOperationException {
        var name = frame.getName();
        var params = frame.getParams();

        return frame.isField() ? frameCallField(clazz, invoke, name, params)
                               : frameCallMethod(clazz, invoke, name, params);
    }

    private static Pair<Class<?>, Object> frameCallMethod(Class<?> anchor, Object invoke, String identifier, List<Param<?>> params)
    throws ReflectiveOperationException {
        // gather information
        var types = toParameterTypes(params);
        var method = ReflectUtil.findUnderlyingMethod(anchor, identifier, true, types)
                                .orElseThrow(() -> new NoSuchMethodException("%s(%s) in %s".formatted(identifier, Arrays.toString(types), anchor)));

        // make method accessible
        accessible(method, invoke);

        // execute
        var result = method.invoke(invoke, toParameterObjects(params));
        return Tuple.of(method.getReturnType(), result);
    }


    private static Pair<Class<?>, Object> frameCallField(Class<?> anchor, Object invoke, String identifier, List<Param<?>> params)
    throws ReflectiveOperationException {
        var field = ReflectUtil.findUnderlyingField(anchor, identifier, true)
                               .orElseThrow(() -> new NoSuchFieldException("%s in %s".formatted(identifier, anchor)));

        // make field accessible
        accessible(field, invoke);

        // put value?
        if (!params.isEmpty()) {
            var param = params.get(0);
            if (param.getClazz() == field.getType()) {
                // update the value
                field.set(invoke, params.get(0).getValue());
            }
        }

        // obtain value
        return Tuple.of(field.getType(), field.get(invoke));
    }

    public static CallFrame[] buildFramePath(String call, List<Param<?>>[] params) {
        var frames = call.split(Pattern.quote("."));
        CallFrame[] path = new CallFrame[frames.length];

        for (int i = 0; i < frames.length; i++) {
            String frame = frames[i];
            List<Param<?>> frameParams = Optional.ofNullable(
                    params.length > i ? params[i] : null).orElseGet(Collections::emptyList);

            CallFrame callFrame;
            if (frame.endsWith("()")) {
                callFrame = new CallFrame(frame.substring(0, frame.length() - 2), false, frameParams);
            } else {
                callFrame = new CallFrame(frame, true, frameParams);
            }

            path[i] = callFrame;
        }

        return path;
    }

    @SneakyThrows
    public static boolean deepEquals(@NotNull Object a, @NotNull Object b) {
        if (!Internal.isSudoThread()) {
            return ReflectUtil.doPrivileged(() -> deepEquals(a, b));
        }

        // compare all fields
        var clazz = a.getClass();
        do {
            for (Field field : clazz.getDeclaredFields()) {
                final int mod = field.getModifiers();
                if (field.isSynthetic()
                    || Modifier.isTransient(mod)
                    || Modifier.isStatic(mod)) {
                    continue; // skip on synthetic, transient or static fields
                }

                accessible(field);

                Object someFieldObj = field.get(a);
                Object otherFieldObj = field.get(b);

                if (!Objects.equals(someFieldObj, otherFieldObj)) {
                    return false; // fields are not equal
                }
            }

        } while ((clazz = clazz.getSuperclass()) != null && !clazz.isPrimitive() && !clazz.isInterface()
                 && clazz != Object.class);

        // no object found that is not equal
        return true;
    }

    public static int deepHash(@NotNull final Object obj, @NotNull final Class<?> anchor, boolean useImpl) {
        // attempt using implementation
        if (useImpl) {
            try {
                return (int) accessible(anchor.getMethod("hashCode"), obj).invoke(obj);
            } catch (InvocationTargetException e) {
                var cause = e.getTargetException();
                if (cause instanceof RuntimeException re) {
                    throw re; // rethrow
                }

                // definition in Object#hashCode() prevents declaration of check exceptions
                throw new UndefinedStateError("Unexpected checked exception", e);
            } catch (IllegalAccessException | ClassCastException e) {
                throw new UndefinedStateError(e);
            } catch (NoSuchMethodException e) {
                // fallthrough
            }
        }

        // auto compute hash
        if (!Internal.isSudoThread()) {
            return ReflectUtil.doPrivileged(() -> deepHash(obj, anchor, false));
        }

        var fields = Arrays.stream(anchor.getDeclaredFields())
                           .filter(f -> !f.isSynthetic())
                           .filter(f -> {
                               int mod = f.getModifiers();
                               return !Modifier.isStatic(mod) && !Modifier.isTransient(mod);
                           })
                           .toArray(Field[]::new);
        List<Object> objs = new ArrayList<>(fields.length + 1);

        // collect subsequent object for hash computation
        for (Field declaredField : fields) {
            accessible(declaredField);
            nofail(IllegalAccessException.class, () -> objs.add(declaredField.get(obj)));
        }

        // superclass hash?
        Class<?> superClazz = anchor.getSuperclass();
        if (superClazz != null && !superClazz.isPrimitive() && !superClazz.isInterface()
            && superClazz != Object.class) {
            objs.add(deepHash(obj, superClazz, true));
        }

        return Objects.hash(objs.toArray());
    }
}
