package cc.fluse.ulib.core.file;

import cc.fluse.ulib.core.ex.SoftException;
import cc.fluse.ulib.core.ex.SoftIOException;
import cc.fluse.ulib.core.io.IOUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;

/**
 * Util for filesystem operations.
 */
public final class FSUtil {


    /**
     * Checks if the specified path is empty.
     * <p>
     * If the object at the specified path is a directory, this method returns {@code true} if the directory is empty;
     * if it is a file, this method returns {@code true} if the file size is 0. Otherwise, if the object at the
     * specified path does not exist, this method returns {@code true}.
     *
     * @param path the path to check
     * @return {@code true} if the specified path is empty, {@code false} otherwise
     */
    public static boolean isEmpty(@NotNull Path path) {
        if (!Files.exists(path)) return true;

        try {
            // check if file is empty
            if (Files.isRegularFile(path)) return Files.size(path) == 0;

            // check if dir is empty
            if (Files.isDirectory(path)) {
                return IOUtil.op(() -> Files.list(path), stream -> stream.findAny().isEmpty(), true);
            }
        } catch (IOException e) {
            throw new SoftIOException(e);
        }

        return false;
    }


    /**
     * Flags for {@link #mk(Path, int)}.
     */
    public static final int
            MK_DIR = 0x01,
            MK_ONLY_PARENT = 0x02,
            MK_NO_CREATE_PARENT_DIRS = 0x04,
            MK_THROW = 0x08;

    /**
     * Creates a new directory (default) or file with default permissions.
     * <p>
     * This method does not fail if a directory or file (in case of {@link #MK_DIR}) already exists at the specified
     * path.
     *
     * @param path  the path to create the object at
     * @param flags bitmask:
     *              <ul>
     *                  <li>{@link #MK_DIR} create a directory instead of a file</li>
     *                  <li>{@link #MK_ONLY_PARENT} create the direct parent of the path instead of the path itself</li>
     *                  <li>{@link #MK_NO_CREATE_PARENT_DIRS} do not create any parent directories</li>
     *                  <li>{@link #MK_THROW} throw an exception instead of returning {@code false}</li>
     *              </ul>
     * @return {@code true} in case the operation was successful, {@code false} otherwise
     *
     * @throws MissingParentException in case parent directories must be created, but it's not permitted
     * @throws SoftIOException        in case any underlying {@link IOException} occurs
     */
    public static boolean mk(@NotNull Path path, int flags) {
        var child = (flags & MK_ONLY_PARENT) != 0
                    ? Objects.requireNonNullElseGet(path.toAbsolutePath().getParent(), () -> Path.of("/"))
                    : path;
        @Nullable var parent = child.getParent();

        try {
            // create parent
            if (parent != null && !Files.exists(parent)) {
                // allowed?
                if ((flags & MK_NO_CREATE_PARENT_DIRS) != 0) {
                    if ((flags & MK_THROW) != 0) throw new MissingParentException(child);
                    return false;
                }

                Files.createDirectories(parent);
            }


            // create child
            if ((flags & MK_DIR) != 0) {
                if (!Files.isDirectory(child)) Files.createDirectory(child);
            } else {
                if (!Files.isRegularFile(child)) Files.createFile(child);
            }
        } catch (IOException e) {
            if ((flags & MK_THROW) != 0) throw new SoftException(e);
            return false;
        }

        return true;
    }


    /**
     * Creates a directory (and if necessary, its parents) with default permissions. This method does not fail if a
     * directory already exists at the specified path.
     *
     * @param path the directory path to create
     * @see #mk(Path, int)
     */
    public static void mkDirs(@NotNull Path path) throws IOException {
        try {
            mk(path, MK_DIR | MK_THROW);
        } catch (MissingParentException e) {
            throw new IOException(e);
        } catch (SoftIOException e) {
            throw e.getCause();
        }
    }

    /**
     * Creates the parent directories of the specified path.
     *
     * @param path the path of whose parents to create
     * @see #mk(Path, int)
     */
    public static void mkParent(@NotNull Path path) throws IOException {
        try {
            mk(path, MK_ONLY_PARENT | MK_DIR | MK_THROW);
        } catch (MissingParentException e) {
            throw new IOException(e);
        } catch (SoftIOException e) {
            throw e.getCause();
        }
    }

    /**
     * Creates a file (and if necessary, its parent directories) with default permissions . This method does not fail if
     * a file already exists at the specified path.
     *
     * @param path the file path to create
     * @see #mk(Path, int)
     */
    public static void mkFile(@NotNull Path path) throws IOException {
        try {
            mk(path, MK_THROW);
        } catch (MissingParentException e) {
            throw new IOException(e);
        } catch (SoftIOException e) {
            throw e.getCause();
        }
    }


    /**
     * Flags for {@link #rm(Path, int)}.
     */
    public static final int
            RM_NO_RECURSIVE = 0x01,
            RM_THROW = 0x02;

    /**
     * Removes a file or directory.
     *
     * @param path  the path of the object to remove
     * @param flags bitmask:
     *              <ul>
     *              <li>{@link #RM_NO_RECURSIVE} do not remove directories recursively</li>
     *              <li>{@link #RM_THROW} throw an exception instead of returning {@code false}</li>
     *              </ul>
     * @return {@code true} if the operation was successful, {@code false} otherwise
     *
     * @throws SoftIOException in case any underlying {@link IOException} occurs
     */
    public static boolean rm(@NotNull Path path, int flags) {
        if (!Files.exists(path)) return true;

        try {
            if (Files.isRegularFile(path)) {
                Files.delete(path);
                return true;
            }

            // directly delete dir if empty
            if (IOUtil.op(() -> Files.list(path), p -> p.findAny().isEmpty(), true)) {
                Files.delete(path);
                return true;
            }

            // recursive deletion
            if ((flags & RM_NO_RECURSIVE) != 0) {
                // TODO: throw a form of RuntimeException
                if ((flags & RM_THROW) != 0) throw new DirectoryNotEmptyException(path.toString());
                return false;
            }


            Files.walkFileTree(path, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                    if (e != null) throw e;
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            if ((flags & RM_THROW) != 0) throw new SoftIOException(e);
            return false;
        }
        return true;
    }


    /**
     * Flags for {@link #purge(Path, int)}.
     */
    public static final int
            PURGE_NO_RECURSIVE = 0x01,
            PURGE_NO_FILES = 0x02,
            PURGE_THROW = 0x04;

    /**
     * Removes empty files and directories recursively inside the specified directory. By default, this method will
     * purge the objects recursively.
     *
     * @param path  the path of the directory to purge
     * @param flags bitmask:
     *              <ul>
     *              <li>{@link #PURGE_NO_FILES} do not remove empty files</li>
     *              <li>{@link #PURGE_THROW} throw an exception instead of returning {@code false}</li>
     *              </ul>
     * @return {@code true} if the operation was successful, {@code false} otherwise
     */
    public static boolean purge(@NotNull Path path, int flags) {
        if (!Files.exists(path)) return true;

        try {
            // check if path is a directory
            if (!Files.isDirectory(path)) {
                if ((flags & PURGE_THROW) != 0) throw new NotDirectoryException(path.toString());
                return false;
            }

            // directly return if dir is empty
            if (IOUtil.op(() -> Files.list(path), p -> p.findAny().isEmpty(), true)) return true;

            // flat purge
            if ((flags & PURGE_NO_RECURSIVE) != 0) {
                Files.list(path)
                        // filter out files if necessary
                        .filter((flags & PURGE_NO_FILES) != 0 ? Files::isDirectory : p -> true)
                        // filter out non-empty dirs/files
                        .filter(FSUtil::isEmpty)
                        // delete
                        .forEach(p -> rm(p, RM_NO_RECURSIVE | ((flags & PURGE_THROW) != 0 ? RM_THROW : 0)));
            }

            // recursive purge
            Files.walkFileTree(path, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if ((flags & PURGE_NO_FILES) != 0) return FileVisitResult.CONTINUE;
                    if (isEmpty(file)) Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                    if (e != null) throw e;
                    // only delete dir if it is not the root dir
                    if (!Files.isSameFile(path, dir) && isEmpty(dir)) Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });


        } catch (IOException e) {
            if ((flags & PURGE_THROW) != 0) throw new SoftIOException(e);
            return false;
        }

        return true;
    }

}
