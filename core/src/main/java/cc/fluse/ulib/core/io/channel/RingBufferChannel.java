package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.io.datachannel.AbstractDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A byte channel backed by a ring buffer. The internal buffer is fixed in size and will not be enlarged (unlike
 * {@link PipeBufferChannel}). When the buffer is full, the {@link #write(ByteBuffer)} method will overwrite the oldest
 * bytes in the buffer, if {@link #overwrite} is set to {@code true}. Otherwise, the {@link #write(ByteBuffer)} method
 * will return 0.
 *
 * @see PipeBufferChannel
 */
public class RingBufferChannel extends AbstractDataChannel {

    private final ByteBuffer buf;
    private final boolean overwrite;

    private int readPos = 0;
    private int writePos = 0;

    /**
     * Constructs a new ring buffer channel with the given capacity.
     *
     * @param capacity  capacity of the ring buffer
     * @param overwrite whether to overwrite non-read bytes when buffer is full
     */
    public RingBufferChannel(int capacity, boolean overwrite) {
        if (capacity <= 0)
            throw new IllegalArgumentException("Capacity must be greater than 0 (%d)".formatted(capacity));
        this.buf = Internal.bufalloc(capacity);
        this.overwrite = overwrite;
    }

    /**
     * Returns the number of bytes that can be read from this channel.
     *
     * @return the number of bytes that can be read from this channel
     */
    public int remaining() {
        return writePos >= readPos ? writePos - readPos : buf.capacity() - readPos + writePos;
    }

    @Override
    public int read(@NotNull ByteBuffer dst) throws IOException {
        bouncer.pass();
        // edge cases
        if (!dst.hasRemaining()) return 0;
        if (remaining() == 0) return -1;

        // read
        var read = Math.min(dst.remaining(), remaining());
        for (int i = 0; i < read; i++) {
            dst.put(buf.get(readPos++));
            // wrap around?
            if (readPos == buf.capacity()) this.readPos = 0;
        }

        return read;
    }

    @Override
    public int write(@NotNull ByteBuffer src) throws IOException {
        bouncer.pass();
        // edge cases
        if (!src.hasRemaining()) return 0;

        // write
        int written = 0;
        while (src.hasRemaining()) {

            // handle non-wrapped buffer
            if (writePos >= readPos) {
                buf.put(writePos++, src.get());
                written++;
                // wrap around?
                if (writePos == buf.capacity()) writePos = 0;
                continue;
            }

            // handle wrapped buffer
            if (readPos - 1 > writePos) {
                // writePos is behind readPos and would not overwrite readPos
                buf.put(writePos++, src.get());
                written++;
            } else if (overwrite) {
                buf.put(writePos++, src.get());
                written++;
                readPos++;
                // wrap around?
                if (writePos == buf.capacity()) writePos = 0;
            } else {
                return written;
            }

        }
        return written;
    }

}
