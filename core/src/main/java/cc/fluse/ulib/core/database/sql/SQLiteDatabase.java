package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.database.FileDatabase;

/**
 * Representation of sqlite type databases.
 */
public interface SQLiteDatabase extends FileDatabase, SqlDatabase {}
