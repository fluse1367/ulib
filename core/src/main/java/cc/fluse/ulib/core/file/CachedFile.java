package cc.fluse.ulib.core.file;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.file.CachedFileImpl;
import cc.fluse.ulib.core.impl.file.ChecksumFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.concurrent.CompletableFuture;

/**
 * Represents a file that is cached locally. The file is created on demand and cached until it is deleted.
 * <p>
 * Each opened read channel or stream reads from the same file independently of each other. Each opened write channel or
 * stream overwrites the underlying file independently of each other. Simultaneous read and write operations are not
 * defined and may result in undefined behavior.
 */
public interface CachedFile extends FilesystemResource {

    static @NotNull Path getDefaultCacheDir() {
        return Internal.getCacheDir();
    }

    /**
     * The default digest provider. Uses SHA-512.
     */
    @NotNull Func<@NotNull MessageDigest, ?> DEFAULT_DIGEST_PROVIDER = () -> MessageDigest.getInstance("SHA-512");

    /**
     * Creates a new cached file without integrity checks.
     *
     * @param path         the cached file location; relative to ulib's cache directory by default, absolute if
     *                     {@link Path#toAbsolutePath()} is used
     * @param creationTask a task capable of writing the data for the cached file; <b>must not</b> handle closure of the
     *                     stream
     */
    @NotNull
    static CachedFile of(@NotNull Path path, @NotNull ParamTask<@NotNull OutputStream, ?> creationTask) {
        return new CachedFileImpl(null, path, creationTask);
    }

    /**
     * Creates a new cached file with integrity checks.
     *
     * @param path           the cached file location; relative to ulib's cache directory by default, absolute if
     *                       {@link Path#toAbsolutePath()} is used
     * @param creationTask   a task capable of writing the data for the cached file; <b>must not</b> handle closure of
     *                       the stream
     * @param digestProvider a stateless function supplying a new message digest instance on demand
     */
    @NotNull
    static CachedFile of(@NotNull Path path, @NotNull ParamTask<@NotNull OutputStream, ?> creationTask,
                         @NotNull Func<@NotNull MessageDigest, ?> digestProvider, byte @Nullable [] expectedDigest) {
        return new ChecksumFile(null, path, creationTask, digestProvider,
                                expectedDigest != null ? ByteBuffer.wrap(expectedDigest) : null);
    }

    /**
     * Creates a new cached file without integrity checks backed by a remote resource.
     *
     * @param resource the remote resource
     */
    @NotNull
    static CachedFile fromRemote(@NotNull RemoteResource resource) {
        var path = Path.of(resource.getRemoteLocation().getHost(), resource.getRemoteLocation().getPath());
        return new CachedFileImpl("http", path, out -> {
            try (var in = resource.streamRead()) {
                in.transferTo(out);
            }
        });
    }


    /**
     * Creates a new cached file with integrity checks backed by a remote resource.
     *
     * @param resource       the remote resource
     * @param digestProvider a stateless function supplying a new message digest instance on demand
     */
    @NotNull
    static CachedFile fromRemote(@NotNull RemoteResource resource,
                                 @NotNull Func<@NotNull MessageDigest, ?> digestProvider, byte @Nullable [] expectedDigest) {
        var path = Path.of(resource.getRemoteLocation().getHost(), resource.getRemoteLocation().getPath());
        return new ChecksumFile("http", path, out -> {
            try (var in = resource.streamRead()) {
                in.transferTo(out);
            }
        }, digestProvider, expectedDigest != null ? ByteBuffer.wrap(expectedDigest) : null);
    }

    /**
     * Ensures the file is ready.
     *
     * @param recreate if the file should be re-created if it exists beforehand
     * @return a future object wrapping underlying file's location
     */
    @NotNull
    CompletableFuture<Path> get(boolean recreate);

    /**
     * Attempts to (re-)create the underlying file at the specified path by using the file creation task
     *
     * @return a future object representing the task status
     */
    @NotNull
    CompletableFuture<Void> populate();

    /**
     * Deletes the file.
     *
     * @return a future object wrapping the result
     */
    @NotNull
    CompletableFuture<Void> purge();

}
