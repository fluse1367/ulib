package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.DataChannel;
import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import cc.fluse.ulib.core.io.channel.WritableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;

/**
 * A {@link DataChannel} that delegates all read operations to a {@link ReadableDataChannel} and all write operations to a
 * {@link WritableDataChannel}.
 * <p>
 * This class is useful for wrapping a channel that does not implement the {@link DataChannel} interface.
 * <p>
 * This class is not thread-safe.
 */
public class DataChannelDelegate extends ReadableDataChannelDelegate implements DataChannel {

    private final WritableDataChannel writeDelegate;

    /**
     * Creates a new {@link DataChannelDelegate} that delegates all read operations to the given channel and all write
     *
     * @param channel the channel to delegate to
     */
    public DataChannelDelegate(@NotNull ByteChannel channel) {
        super(channel);
        this.writeDelegate = WritableDataChannel.wrap(channel);
    }

    @Override
    public @NotNull OutputStream streamWrite() throws IOException {
        bouncer.pass();
        return IOUtil.isolate(DataChannel.super.streamWrite());
    }

    @Override
    public @NotNull WritableDataChannel channelWrite() throws IOException {
        bouncer.pass();
        return DataChannel.super.channelWrite();
    }

    @Override
    public @NotNull DataChannel channel() throws IOException {
        bouncer.pass();
        return IOUtil.isolate(this);
    }

    // delegate methods


    @Override
    public void write(int b) throws IOException {
        bouncer.pass();
        writeDelegate.write(b);
    }

    @Override
    public void write(@NotNull byte[] b) throws IOException {
        bouncer.pass();
        writeDelegate.write(b);
    }

    @Override
    public void write(@NotNull byte[] b, int off, int len) throws IOException {
        bouncer.pass();
        writeDelegate.write(b, off, len);
    }

    @Override
    public int write(@NotNull ByteBuffer src) throws IOException {
        return writeDelegate.write(src);
    }

    @Override
    public boolean isOpen() {
        return writeDelegate.isOpen();
    }

    @Override
    public void close() throws IOException {
        writeDelegate.close();
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {
        writeDelegate.writeBoolean(v);
    }

    @Override
    public void writeByte(int v) throws IOException {
        writeDelegate.writeByte(v);
    }

    @Override
    public void writeShort(int v) throws IOException {
        writeDelegate.writeShort(v);
    }

    @Override
    public void writeChar(int v) throws IOException {
        writeDelegate.writeChar(v);
    }

    @Override
    public void writeInt(int v) throws IOException {
        writeDelegate.writeInt(v);
    }

    @Override
    public void writeLong(long v) throws IOException {
        writeDelegate.writeLong(v);
    }

    @Override
    public void writeFloat(float v) throws IOException {
        writeDelegate.writeFloat(v);
    }

    @Override
    public void writeDouble(double v) throws IOException {
        writeDelegate.writeDouble(v);
    }

    @Override
    public void writeBytes(@NotNull String s) throws IOException {
        writeDelegate.writeBytes(s);
    }

    @Override
    public void writeChars(@NotNull String s) throws IOException {
        writeDelegate.writeChars(s);
    }

    @Override
    public void writeUTF(@NotNull String s) throws IOException {
        writeDelegate.writeUTF(s);
    }

}
