package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.io.MemoryBufferChannelImpl;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

/**
 * An in-memory buffer channel that can be read and written to. The channel can be seeked to any position. This channel
 * can buffer up to a theoretical maximum of {@code 2^63 - 1} bytes (7 EiB), however the actual maximum depends on the
 * available memory.
 * <p>
 * This channel may have undefined regions, which are regions that have not been written to yet. When reading from an
 * undefined region, the behavior is determined by the {@link UndefinedRegionReadStrategy}. Writing to an undefined
 * region will define the region. Reading from the last defined region of the channel will return {@code -1} (EOF).
 */
public interface MemoryBufferChannel extends SeekableDataChannel {

    /**
     * Defines the strategy to use when reading from an undefined region of the channel. This enum only applies if a new
     * read operation starts at an undefined region of the channel. Other read operations that start at defined regions
     * will always return the data in the channel up to the end of the region or the end of the buffer, whichever comes
     * first. Meaning this enum does not affect the behavior of read operations that start at defined regions.
     */
    enum UndefinedRegionReadStrategy {
        /**
         * Returns {@code -1}, indicating an EOF. The current position is not changed.
         */
        EOF,
        /**
         * Returns {@code -1}, indicating an EOF. The current position is set to the beginning of the next defined
         * region, if there is one, otherwise the current position is set to the end of the channel.
         * <p>
         * This strategy is the same as {@link #EOF}, except that the current position is changed.
         */
        EOF_SKIP,

        /**
         * Returns {@code 0}, indicating that no bytes were transferred. The current position is not changed.
         */
        ZERO,
        /**
         * Returns {@code 0}, indicating that no bytes were transferred. The current position is set to the beginning of
         * the next defined region.
         * <p>
         * This strategy is the same as {@link #ZERO}, except that the current position is changed.
         */
        ZERO_SKIP,

        /**
         * The current position is incremented to the next defined region, but only up to
         * {@link Integer#MAX_VALUE 2^31 - 1} bytes (the integer limit) or the end of the channel, whichever comes
         * first. Returns the number of bytes skipped.
         */
        SKIP_N,

        /**
         * The current position is incremented to the next defined region. Returns the number of bytes skipped. If the
         * number of bytes skipped exceeds {@link Integer#MAX_VALUE the integer limit (2^31-1)},
         * {@link Integer#MIN_VALUE} is returned instead.
         * <p>
         * This strategy is the same as {@link #SKIP_N}, except there is no limit on the number of bytes skipped.
         */
        SKIP,

        /**
         * Throws an {@link UnsupportedOperationException}. The current position is not changed.
         */
        THROW,
        /**
         * Throws an {@link UnsupportedOperationException}. The current position is set to the beginning of the next
         * defined region.
         * <p>
         * This strategy is the same as {@link #THROW}, except that the current position is changed.
         */
        THROW_SKIP,

        /**
         * Behaves like a regular read operation, the dst buffer is filled with {@code 0x0} bytes. The current position
         * will be incremented by the number of bytes read. Returns the number of bytes read.
         */
        SIMULATE_DEFINED,
    }

    /**
     * Creates a new memory channel with no limit and
     * {@link UndefinedRegionReadStrategy#SIMULATE_DEFINED SIMULATE_DEFINED} as the undefined region read strategy.
     * <p>
     * <b>Note:</b> The returned channel is not guaranteed to be thread-safe. If multiple threads access the channel
     * concurrently, it must be synchronized externally.
     *
     * @return the new memory channel
     */
    static @NotNull MemoryBufferChannel create() {
        return create(-1L, UndefinedRegionReadStrategy.SIMULATE_DEFINED);
    }

    /**
     * Creates a new memory channel with the specified memory limit and initial capacity.
     * <p>
     * <b>Note:</b> The returned channel is not guaranteed to be thread-safe. If multiple threads access the channel
     * concurrently, it must be synchronized externally.
     *
     * @param limit    the channel limit, or {@code -1} for no limit (see {@link #truncate(long)})
     * @param strategy the strategy to use when reading from an undefined region
     * @return the new memory channel
     */
    static @NotNull MemoryBufferChannel create(long limit, @NotNull UndefinedRegionReadStrategy strategy) {
        return new MemoryBufferChannelImpl(limit, strategy);
    }

    /**
     * Returns the simulated size of this channel. This counts in all bytes that have been written to the channel and
     * all undefined regions. This is not necessarily the same as the real occupied memory of the channel.
     *
     * @return the simulated size of this channel
     * @throws ClosedChannelException if the channel is closed
     */
    @Override
    long size() throws ClosedChannelException;

    /**
     * Returns the real occupied memory of this channel. This is the amount of memory that is actually used (or
     * reserved) by this channel. This is not necessarily the same as the size of the channel.
     *
     * @return the real occupied memory of this channel
     * @throws ClosedChannelException if the channel is closed
     */
    long memory() throws ClosedChannelException;

    // redefine exceptions to be more specific, as a memory channel is not bound to system I/O errors

    @Override
    long position() throws ClosedChannelException;

    @Override
    @Contract("_ -> this")
    @NotNull
    MemoryBufferChannel position(long newPosition) throws ClosedChannelException;

    @Override
    @Contract("_ -> this")
    @NotNull MemoryBufferChannel truncate(long size) throws ClosedChannelException;

    @Override
    int read(@NotNull ByteBuffer dst) throws ClosedChannelException;

    @Override
    int write(@NotNull ByteBuffer src) throws ClosedChannelException;
}
