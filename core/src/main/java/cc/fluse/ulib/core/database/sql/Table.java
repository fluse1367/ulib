package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.common.Keyable;
import cc.fluse.ulib.core.database.sql.orm.Entity;
import cc.fluse.ulib.core.database.sql.orm.Serializer;
import cc.fluse.ulib.core.database.sql.query.*;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a sql Table.
 */
public interface Table extends Keyable<String> {

    /**
     * Obtains an already existing entity from this table.
     *
     * @param primaryKeyValue the primary key value
     * @param serializers     the serializers
     * @param fetchLazy       whether to fetch the columns lazily
     * @param cache           whether to cache the data
     * @param pushLazy        whether to push data to the database lazily
     * @return the entity
     */
    @NotNull Expect<Entity, ?> getEntity(@NotNull Object primaryKeyValue,
                                         @NotNull Map<String, Collection<Serializer<?, ?>>> serializers,
                                         boolean fetchLazy, boolean cache, boolean pushLazy);

    /**
     * Obtains already existing entities from this table.
     *
     * @param keyValueSelectors the key value selectors, column -> selector, the selector is a function that takes a
     *                          {@link Condition} and returns a {@link Where} object, mutating the condition which
     *                          narrows down the selection of entities. See {@link #genSelectors(Map)} for a helper
     *                          method to generate this map with selectors selecting equal values.
     * @param serializers       the serializers
     * @param fetchLazy         whether to fetch the columns lazily
     * @param cache             whether to cache the data
     * @param pushLazy          whether to push data to the database lazily
     * @return the entities
     */
    @NotNull Collection<Entity> getEntities(@NotNull Map<String, Function<? super Condition<Where>, Where>> keyValueSelectors,
                                            @NotNull Map<String, Collection<Serializer<?, ?>>> serializers,
                                            boolean fetchLazy, boolean cache, boolean pushLazy);

    /**
     * Generates a map of selectors selecting equal values.
     *
     * @param keyValueMap the key value map, column -> value
     * @return the selectors
     */
    static @NotNull Map<String, Function<? super Condition<Where>, Where>> genSelectors(@NotNull Map<String, ?> keyValueMap) {
        return keyValueMap.entrySet().stream()
                .map(en -> Tuple.of(en.getKey(), (Function<Condition<Where>, Where>) c -> c.isEqualToP(en.getValue())))
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
    }

    /**
     * Creates a new entity in this table.
     *
     * @param values      the values
     * @param serializers the serializers
     * @param fetchLazy   whether to fetch the columns lazily
     * @param cache       whether to cache the data
     * @param pushLazy    whether to push data to the database lazily
     * @return the entity
     */
    @NotNull Entity createEntity(@NotNull Map<String, Object> values,
                                 @NotNull Map<String, Collection<Serializer<?, ?>>> serializers,
                                 boolean fetchLazy, boolean cache, boolean pushLazy);

    /**
     * Gets the primary key of this table, if it has one.
     *
     * @return the primary key
     */
    @NotNull Optional<Column<?>> getPrimaryKey();

    /**
     * Returns the columns this table has.
     *
     * @return the columns
     */
    @NotNull
    Column<?>[] getColumns();

    /**
     * Searches for a column within this table.
     *
     * @param name the column name
     * @return the column instance, or {@code null} if not found
     */
    @NotNull
    Optional<Column<?>> getColumn(@NotNull String name);

    @NotNull
    String getName();

    @Override
    @NotNull
    default String getKey() {
        return getName();
    }

    /**
     * Attempts to create this table in the database.
     */
    void create() throws SQLException;

    /**
     * Attempts to delete this table from the database.
     *
     * @return {@code true}, if the operation was successful
     */
    boolean drop();

    /**
     * Checks if this table exists within the database.
     *
     * @return {@code true} if this table exists, {@code false} otherwise
     */
    boolean exists();

    /**
     * Selects data from the table.
     *
     * @param what     the column to select
     * @param whatElse additional columns to select
     * @return the query builder
     */
    @NotNull Query select(@NotNull String what, @NotNull String... whatElse);

    /**
     * Selects only different values from the table.
     *
     * @param what     the column to select
     * @param whatElse additional columns to select
     * @return the query builder
     */
    @NotNull Query selectDistinct(@NotNull String what, @NotNull String... whatElse);

    /**
     * Updates the table.
     *
     * @return the query builder
     */
    @NotNull SetQuery update();

    /**
     * Inserts values into the table.<br>
     * The values have to correspond with the columns of the table in the same order.
     *
     * @param value  the value to insert
     * @param values additional values to insert
     * @return {@code true}, if the operation was successful
     */
    boolean insert(@NotNull Object value, @NotNull Object... values);

    /**
     * Inserts values into the table.
     *
     * @param values the values to insert
     * @return the inserted primary key value if auto generated, if not, {@code true} if the operation was successful,
     * {@code false} otherwise
     */
    @NotNull Object insert(@NotNull Map<String, Object> values);

    /**
     * Deletes rows from the table.
     *
     * @return the query builder
     */
    @NotNull
    QueryStart delete();

    /**
     * Deletes all data inside the table.
     *
     * @return {@code true}, if the operation was successful
     */
    boolean truncate();

}
