package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Readable;
import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import org.jetbrains.annotations.NotNull;

/**
 * A {@link TransformerChainChannel} that can be read from.
 */
public interface ReadableTransformChannel extends TransformerChainChannel, Readable, ReadableDataChannel {
    @Override
    @NotNull
    default ReadableDataChannel channel() {
        return IOUtil.isolate(this);
    }
}
