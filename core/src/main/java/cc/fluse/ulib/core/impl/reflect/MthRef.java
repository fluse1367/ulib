package cc.fluse.ulib.core.impl.reflect;

import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.Arrays;

public final class MthRef implements cc.fluse.ulib.core.reflect.ref.MthRef {

    private final String name;
    private final ClsRef declaring, returnType;
    private final ClsRef[] parameterTypes;

    public MthRef(String name, ClsRef declaring, ClsRef returnType, ClsRef[] parameterTypes) {
        this.name = name;
        this.declaring = declaring;
        this.returnType = returnType;
        this.parameterTypes = parameterTypes;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    public ClsRef getDeclaringClass() {
        return declaring;
    }

    @Override
    public ClsRef getReturnType() {
        return returnType;
    }

    @Override
    public ClsRef[] getParameterTypes() {
        return parameterTypes.clone();
    }

    @Override
    public @NotNull Expect<Method, ?> tryLoad() {
        return getDeclaringClass()
                .tryLoad()
                .map(cls -> cls.getDeclaredMethod(getName(), Arrays.stream(parameterTypes)
                                                                   .map(cc.fluse.ulib.core.reflect.ref.ClsRef::tryLoad)
                                                                   .map(Expect::orElseRethrowRE)
                                                                   .toArray(Class[]::new)));
    }
}
