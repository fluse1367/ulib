package cc.fluse.ulib.core.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import org.jetbrains.annotations.NotNull;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Objects;

@BypassAnnotationEnforcement
public class SoftException extends RuntimeException {
    private final Throwable cause;

    public SoftException(@NotNull Throwable cause) {
        super(Objects.requireNonNull(cause));
        this.cause = cause;
    }

    @Override
    public String getMessage() {
        return cause.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return cause.getLocalizedMessage();
    }

    @Override
    public Throwable getCause() {
        return cause.getCause();
    }

    @Override
    public Throwable initCause(Throwable cause) {
        return this.cause.initCause(cause);
    }

    @Override
    public String toString() {
        return cause.toString();
    }

    @Override
    public void printStackTrace() {
        cause.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream s) {
        cause.printStackTrace(s);
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        cause.printStackTrace(s);
    }

    @Override
    public Throwable fillInStackTrace() {
        return cause.fillInStackTrace();
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return cause.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTrace) {
        cause.setStackTrace(stackTrace);
    }
}
