package cc.fluse.ulib.core.impl.http.jsonrpc.v2;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.http.jsonrpc.RpcRequest;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

final class V2Request implements RpcRequest {

    @Getter
    @NotNull
    private final V2Endpoint endpoint;
    @Getter
    @NotNull
    private final String method;
    @Nullable
    private final String id;
    @Nullable
    private final Object parameters;

    V2Request(@NotNull V2Endpoint endpoint, @NotNull String method, @Nullable String id, @Nullable Object parameters) {
        this.endpoint = endpoint;
        this.method = method;
        this.id = id;
        this.parameters = parameters;
    }

    @Override
    public @NotNull Optional<String> getId() {
        return Optional.ofNullable(this.id);
    }

    @Override
    public @NotNull Optional<Object> getParameters() {
        return Optional.ofNullable(this.parameters);
    }

    String buildBody() {
        // see https://www.jsonrpc.org/specification#request_object
        var json = JsonConfiguration.newJson();
        json.set("jsonrpc", endpoint.getRpcVersion());
        json.set("method", method);
        getId().ifPresent(id -> json.set("id", id));
        getParameters().ifPresent(params -> json.set("params", params));

        // serialize to body
        return json.dumpString();
    }

    @Override
    public String toString() {
        return "RpcRequest{" +
               "endpoint=" + endpoint +
               ", method='" + method + '\'' +
               ", id='" + id + '\'' +
               ", parameters=" + parameters +
               '}';
    }
}
