package cc.fluse.ulib.core.impl.database.sql.query;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.database.sql.query.SetQuery;
import cc.fluse.ulib.core.database.sql.query.Where;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public final class SetQueryImpl extends QueryImpl implements SetQuery {
    private final List<Pair<String, Object>> sets = new ArrayList<>();

    public SetQueryImpl(AbstractSqlDatabase sql, Table table, String what) {
        super(sql, table, what);
    }

    @Override
    public @NotNull SetQueryImpl setP(@NotNull Column<?> column) {
        return setP(column.getName());
    }

    @Override
    public @NotNull SetQueryImpl setP(@NotNull String column) {
        meta.skipParam();
        sets.add(Tuple.of(meta.sql.quote(column, true), "?"));
        return this;
    }

    @Override
    public cc.fluse.ulib.core.database.sql.query.@NotNull SetQuery setP(@NotNull Column<?> column, Object to) {
        return setP(column.getName(), to);
    }

    @Override
    public cc.fluse.ulib.core.database.sql.query.@NotNull SetQuery setP(@NotNull String column, Object to) {
        meta.opObj(to);
        sets.add(Tuple.of(meta.sql.quote(column, true), "?"));
        return this;
    }

    @Override
    public @NotNull SetQueryImpl set(@NotNull Column<?> column, @NotNull Object to) {
        return set(column.getName(), to);
    }

    @Override
    public @NotNull SetQueryImpl set(@NotNull String column, @NotNull Object to) {
        sets.add(Tuple.of(meta.sql.quote(column, true), meta.sql.quote(to, false)));
        return this;
    }

    private void append() {
        StringJoiner sj = new StringJoiner(", ", " set ", "");
        sets.forEach(pair -> sj.add(String.format("%s = %s", pair.getFirst(), pair.getSecond())));
        meta.query.append(sj);
    }

    @Override
    public @NotNull ConditionImpl<Where> where(@NotNull Column<?> column) {
        append();
        return super.where(column);
    }

    @Override
    public @NotNull ConditionImpl<Where> where(@NotNull String column) {
        append();
        return super.where(column);
    }

    @Override
    public @NotNull WhereImpl whereRaw(@NotNull String condition) {
        append();
        return super.whereRaw(condition);
    }

    @Override
    public @NotNull PreparedStatement build() {
        append();
        return super.build();
    }
}
