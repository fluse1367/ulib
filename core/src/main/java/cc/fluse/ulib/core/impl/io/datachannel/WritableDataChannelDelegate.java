package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.channel.WritableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/**
 * A {@link WritableDataChannel} that delegates all methods to a source channel.
 * <p>
 * This class is useful for wrapping a channel that does not implement the {@link WritableDataChannel} interface.
 * <p>
 * This class is not thread-safe.
 */
public class WritableDataChannelDelegate extends AbstractWritableDataChannel {

    private final WritableByteChannel channel;

    /**
     * Creates a new {@link WritableDataChannelDelegate} that delegates all methods to the given channel.
     *
     * @param channel the channel to delegate to
     */
    public WritableDataChannelDelegate(@NotNull WritableByteChannel channel) {
        this.channel = channel;
    }

    @Override
    public int write(@NotNull ByteBuffer src) throws IOException {
        bouncer.pass();
        return channel.write(src);
    }

    @Override
    public boolean isOpen() {
        return channel.isOpen() && super.isOpen();
    }

    @Override
    public void close() throws IOException {
        channel.close();
        super.close();
    }
}
