package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.ex.SoftException;
import cc.fluse.ulib.core.ex.SoftIOException;
import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.*;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.io.Isolated;
import cc.fluse.ulib.core.impl.io.SyntheticChannel;
import cc.fluse.ulib.core.impl.io.datachannel.SeekableChannelAdapter;
import cc.fluse.ulib.core.io.channel.*;
import cc.fluse.ulib.core.util.ArrayUtil;
import cc.fluse.ulib.core.util.Conditions;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadFactory;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

/**
 * Class containing I/O (stream) operations.
 */
public final class IOUtil {

    @NotNull
    public static final ByteBuffer EMPTY_BYTE_BUFFER = Internal.bufalloc(0).asReadOnlyBuffer();

    public static final byte @NotNull [] EMPTY_BYTE_ARRAY = new byte[0];

    // - construction methods -

    /**
     * Constructs a new isolated proxy for the given object. The proxy will not delegate a close operation to the
     * source. Multiple calls to {@link AutoCloseable#close()} on the proxy will be ignored. If {@code block} is
     * {@code true}, the proxy will throw an {@link IOException} (or {@link IllegalStateException} if the method does
     * not declare {@link IOException}) on any method call after the first call to {@link AutoCloseable#close()}.
     * <p>
     * <b>Note</b> the deprecation warning.
     *
     * @param t     the object to isolate
     * @param block whether to block all other methods
     * @param <T>   the interface type of the object
     * @return the isolated proxy
     * @deprecated The object returned by this method does only implement interfaces of the given object, but not the
     * object's class itself. This may lead to an {@link ClassCastException} if the object's class is used as a type
     * parameter instead of one of its interfaces.
     */
    @Deprecated
    public static <T extends AutoCloseable> @NotNull T isolate(@NotNull T t, boolean block) {
        return Isolated.Universal.of(t, block);
    }

    /**
     * Constructs a new input stream from the given source. The new input stream does not delegate a close operation to
     * the source. If the source is already an isolated stream, it will be returned as is.
     *
     * @param in the source
     * @return the isolated stream
     */
    public static @NotNull InputStream isolate(@NotNull InputStream in) {
        return in instanceof Isolated.InputStream ? in : new Isolated.InputStream(in);
    }

    /**
     * Constructs a new output stream with the given sink. The new output stream does not delegate a close operation to
     * the sink, however it will flush the sink on close. If the sink is already an isolated stream, it will be returned
     * as is.
     *
     * @param out the sink
     * @return the isolated stream
     */
    public static @NotNull OutputStream isolate(@NotNull OutputStream out) {
        return out instanceof Isolated.OutputStream ? out : new Isolated.OutputStream(out);
    }

    /**
     * Constructs a new readable byte channel with the given source channel. The new channel does not delegate a close
     * operation to the source channel. If the source channel is already an isolated channel, it will be returned as
     * is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull ReadableByteChannel isolate(@NotNull ReadableByteChannel channel) {
        return channel instanceof ReadableDataChannel rdc ? isolate(rdc)
            : channel instanceof ByteChannel bc ? isolate(bc)
            : channel instanceof Isolated.ReadableByteChannel ? channel : new Isolated.ReadableByteChannel(channel);
    }

    /**
     * Constructs a new readable data channel with the given source channel. The new channel does not delegate a close
     * operation to the source channel. If the source channel is already an isolated channel, it will be returned as
     * is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull ReadableDataChannel isolate(@NotNull ReadableDataChannel channel) {
        return channel instanceof DataChannel dc ? isolate(dc)
            : channel instanceof Isolated.ReadableDataChannel ? channel : new Isolated.ReadableDataChannel(channel);
    }

    /**
     * Constructs a new writable byte channel with the given sink channel. The new channel does not delegate a close
     * operation to the sink channel. If the sink channel is already an isolated channel, it will be returned as is.
     *
     * @param channel the sink channel
     * @return the isolated channel
     */
    public static @NotNull WritableByteChannel isolate(@NotNull WritableByteChannel channel) {
        return channel instanceof WritableDataChannel wdc ? isolate(wdc)
            : channel instanceof ByteChannel bc ? isolate(bc)
            : channel instanceof Isolated.WritableByteChannel ? channel : new Isolated.WritableByteChannel(channel);
    }

    /**
     * Constructs a new writable data channel with the given sink channel. The new channel does not delegate a close
     * operation to the sink channel. If the sink channel is already an isolated channel, it will be returned as is.
     *
     * @param channel the sink channel
     * @return the isolated channel
     */
    public static @NotNull WritableDataChannel isolate(@NotNull WritableDataChannel channel) {
        return channel instanceof DataChannel dc ? isolate(dc)
            : channel instanceof Isolated.WritableDataChannel ? channel : new Isolated.WritableDataChannel(channel);
    }

    /**
     * Constructs a new byte channel with the given source channel. The new channel does not delegate a close operation
     * to the source channel. If the source channel is already an isolated channel, it will be returned as is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull ByteChannel isolate(@NotNull ByteChannel channel) {
        return channel instanceof DataChannel dc ? isolate(dc)
            : channel instanceof SeekableByteChannel sbc ? isolate(sbc)
            : channel instanceof Isolated.ByteChannel ? channel : new Isolated.ByteChannel(channel);
    }

    /**
     * Constructs a new data channel with the given source channel. The new channel does not delegate a close operation
     * to the source channel. If the source channel is already an isolated channel, it will be returned as is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull DataChannel isolate(@NotNull DataChannel channel) {
        return channel instanceof SeekableDataChannel sdc ? isolate(sdc)
            : channel instanceof Isolated.DataChannel ? channel : new Isolated.DataChannel(channel);
    }

    /**
     * Constructs a new seekable byte channel with the given source channel. The new channel does not delegate a close
     * operation to the source channel. If the source channel is already an isolated channel, it will be returned as
     * is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull SeekableByteChannel isolate(@NotNull SeekableByteChannel channel) {
        return channel instanceof SeekableDataChannel sdc ? isolate(sdc)
            : channel instanceof Isolated.SeekableByteChannel ? channel : new Isolated.SeekableByteChannel(channel);
    }

    /**
     * Constructs a new seekable data channel with the given source channel. The new channel does not delegate a close
     * operation to the source channel. If the source channel is already an isolated channel, it will be returned as
     * is.
     *
     * @param channel the source channel
     * @return the isolated channel
     */
    public static @NotNull SeekableDataChannel isolate(@NotNull SeekableDataChannel channel) {
        return channel instanceof Isolated.SeekableDataChannel ? channel : new Isolated.SeekableDataChannel(channel);
    }

    /**
     * Constructs a new synthetic input stream with a circular update function as byte source.
     *
     * @param bufferFiller a function which provides bytes by filling the given byte buffer (see
     *                     {@link InputStream#read(byte[], int, int)})
     * @return the synthetic stream
     */
    @NotNull
    public static InputStream synthesizeInputStream(@NotNull TriParamFunc<byte @NotNull [], @NotNull Integer, @NotNull Integer, @NotNull Integer, ? extends IOException> bufferFiller) {
        return synthesizeInputStream(() -> true, bufferFiller);
    }

    /**
     * Constructs a new synthetic input stream with a circular update function as byte source.
     *
     * @param hasNext      a function which tests if there is next data to fetch
     * @param bufferFiller a function which provides bytes by filling the given byte buffer (see
     *                     {@link InputStream#read(byte[], int, int)}); returns the number of bytes written into the
     *                     buffer, a return value &lt; 1 will be interpreted as the end of stream
     * @return the synthetic stream
     */
    @NotNull
    public static InputStream synthesizeInputStream(@NotNull BooleanSupplier hasNext,
                                                    @NotNull TriParamFunc<? super byte @NotNull [], ? super @NotNull Integer, ? super @NotNull Integer, ? extends @NotNull Integer, ? extends IOException> bufferFiller) {
        var buf = new byte[1024];
        return Channels.newInputStream(synthesizeReadChannel(hasNext, dst -> {
            int len = bufferFiller.apply(buf, 0, buf.length);
            if (len <= 0) {
                return 0;
            }
            dst.put(buf, 0, len);
            return len;
        }));
    }

    /**
     * Constructs a new synthetic input stream with a byte source.
     *
     * @param byteSource a function which provides the bytes, may return null indicating the end of data
     * @return the synthetic stream
     */
    @NotNull
    public static InputStream synthesizeInputStream(@NotNull Func<byte @Nullable [], ? extends IOException> byteSource) {
        return synthesizeInputStream(() -> true, byteSource);
    }

    /**
     * Constructs a new synthetic input stream with a byte source.
     *
     * @param hasNext    a function which tests if there is next data to fetch
     * @param byteSource a function which provides the bytes, may return null indicating the end of data
     * @return the synthetic stream
     */
    @NotNull
    public static InputStream synthesizeInputStream(@NotNull BooleanSupplier hasNext,
                                                    @NotNull Func<byte @Nullable [], ? extends IOException> byteSource) {
        return Channels.newInputStream(synthesizeReadChannel(hasNext, byteSource));
    }

    /**
     * Constructs a new synthetic readable byte channel.
     *
     * @param hasNext        a function which tests whether there is additional data to obtain
     * @param bufferFiller a function which provides the bytes by filling the provided buffer with data (see
     *                       {@link WritableByteChannel#write(ByteBuffer)}), returns the number of bytes written into
     *                       the buffer
     * @return the synthetic channel
     */
    @NotNull
    public static ReadableByteChannel synthesizeReadChannel(@NotNull BooleanSupplier hasNext,
                                                            @NotNull ParamFunc<? super @NotNull ByteBuffer, ? extends @NotNull Integer, ? extends IOException> bufferFiller) {
        return new SyntheticChannel.Read(hasNext, bufferFiller, Internal.getDefaultBufferCapacity());
    }

    /**
     * Constructs a new synthetic readable byte channel.
     *
     * @param hasNext           a function which tests whether there is additional data to obtain
     * @param nextBytesSupplier a function which provides the next bytes, may return null
     * @return the synthetic channel
     */
    @NotNull
    public static ReadableByteChannel synthesizeReadChannel(@NotNull BooleanSupplier hasNext,
                                                            @NotNull Func<byte @Nullable [], ? extends IOException> nextBytesSupplier) {
        return SyntheticChannel.Read.nextBytes(hasNext, nextBytesSupplier, Internal.getDefaultBufferCapacity());
    }

    // - InputStream reading methods -

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads all available bytes from an input stream in blocks of 1024 and calls a consumer with the respective
     * buffer.
     * <p>
     * This method does not close the stream.
     *
     * @param in       the stream to read from
     * @param consumer the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateAllBlockwise(@NotNull InputStream in,
                                                                @NotNull TriParamTask<byte[], Integer, Integer, X> consumer)
    throws IOException, X {
        updateBlockwise(in, -1, consumer);
    }

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads up to N bytes from an input stream in blocks of 1024 and calls a consumer with the respective buffer.
     *
     * @param in       the stream to read from
     * @param limit    the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateBlockwise(@NotNull InputStream in, long limit,
                                                             @NotNull TriParamTask<byte[], Integer, Integer, X> consumer)
    throws IOException, X {
        updateBlockwise(1024, in, limit, consumer);
    }

    /**
     * Reads all available bytes from an input stream in blocks of 1024 and calls a consumer with the respective
     * buffer.
     * <p>
     * This method does not close the stream.
     *
     * @param in       the stream to read from
     * @param consumer the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readAllBlockwise(@NotNull InputStream in,
                                                              @NotNull BiParamTask<byte[], Integer, X> consumer)
    throws IOException, X {
        readBlockwise(in, -1, consumer);
    }

    /**
     * Reads up to N bytes from an input stream in blocks of 1024 and calls a consumer with the respective buffer.
     *
     * @param in       the stream to read from
     * @param limit    the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readBlockwise(@NotNull InputStream in, long limit,
                                                           @NotNull BiParamTask<byte[], Integer, X> consumer)
    throws IOException, X {
        readBlockwise(1024, in, limit, consumer);
    }

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads all available bytes from an input stream in blocks of a certain size and calls a consumer with the
     * respective buffer.
     * <p>
     * This method does not close the stream.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param consumer  the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateAllBlockwise(int blockSize, @NotNull InputStream in,
                                                                @NotNull TriParamTask<byte[], Integer, Integer, X> consumer)
    throws IOException, X {
        updateBlockwise(blockSize, in, -1, consumer);
    }

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads up to N bytes from an input stream in blocks of a certain size and calls a consumer with the respective
     * buffer.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param limit     the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer  the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateBlockwise(int blockSize, @NotNull InputStream in, long limit,
                                                             @NotNull TriParamTask<byte[], Integer, Integer, X> consumer)
    throws IOException, X {
        readBlockwise(blockSize, in, limit, (buf, len) -> consumer.execute(buf, 0, len));
    }


    /**
     * Reads all available bytes from an input stream in blocks of a certain size and calls a consumer with the
     * respective buffer.
     * <p>
     * This method does not close the stream.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param consumer  the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readAllBlockwise(int blockSize, @NotNull InputStream in,
                                                              @NotNull BiParamTask<byte[], Integer, X> consumer)
    throws IOException, X {
        readBlockwise(blockSize, in, -1, consumer);
    }

    /**
     * Reads up to N bytes from an input stream in blocks of a certain size and calls a consumer with the respective
     * buffer.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param limit     the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer  the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readBlockwise(int blockSize, @NotNull InputStream in, long limit,
                                                           @NotNull BiParamTask<byte[], Integer, X> consumer)
    throws IOException, X {
        if (limit == 0) return;
        if (blockSize <= 0) throw new IllegalArgumentException("Invalid blocksize");

        var stream = LimitInputStream.limited(in, limit);
        byte[] buf = new byte[blockSize];
        int len;
        while ((len = stream.read(buf)) != -1) {
            consumer.execute(buf, len);
        }
    }

    // - Reader reading methods -

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads up to N chars from a reader in blocks of 1024 and calls a consumer with the respective buffer.
     *
     * @param in       the stream to read from
     * @param limit    the maximum amount of chars to read, &lt; 0 for no limit
     * @param consumer the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateBlockwise(@NotNull Reader in, long limit,
                                                             @NotNull TriParamTask<char[], Integer, Integer, X> consumer)
    throws IOException, X {
        updateBlockwise(1024, in, limit, consumer);
    }

    /**
     * Reads up to N chars from a reader in blocks of 1024 and calls a consumer with the respective buffer.
     *
     * @param in       the stream to read from
     * @param limit    the maximum amount of chars to read, &lt; 0 for no limit
     * @param consumer the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readBlockwise(@NotNull Reader in, long limit,
                                                           @NotNull BiParamTask<char[], Integer, X> consumer)
    throws IOException, X {
        readBlockwise(1024, in, limit, consumer);
    }

    /**
     * Convenience method to pass updating methods as reference into a blockwise read operation.
     * <p>
     * Reads up to N chars from a reader in blocks of a certain size and calls a consumer with the respective buffer.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param limit     the maximum amount of chars to read, &lt; 0 for no limit
     * @param consumer  the consumer, called with the buffer, an offset (which is always {@code 0} and the read length
     */
    public static <X extends Exception> void updateBlockwise(int blockSize, @NotNull Reader in, long limit,
                                                             @NotNull TriParamTask<char[], Integer, Integer, X> consumer)
    throws IOException, X {
        readBlockwise(blockSize, in, limit, (buf, len) -> consumer.execute(buf, 0, len));
    }

    /**
     * Reads up to N chars from a reader in blocks of a certain size and calls a consumer with the respective buffer.
     *
     * @param blockSize the block size
     * @param in        the stream to read from
     * @param limit     the maximum amount of chars to read, &lt; 0 for no limit
     * @param consumer  the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readBlockwise(int blockSize, @NotNull Reader in, long limit,
                                                           @NotNull BiParamTask<char[], Integer, X> consumer)
    throws IOException, X {
        if (limit == 0) return;
        if (blockSize <= 0) throw new IllegalArgumentException("Invalid blocksize");

        char[] buf = new char[blockSize];
        int len;

        if (limit < 0) { // read all
            while ((len = in.read(buf)) != -1) {
                consumer.execute(buf, len);
            }
            return;
        }

        // limited read
        int read = 0;
        while (read < limit && (len = in.read(buf, 0, (int) Math.min(limit - read, Integer.MAX_VALUE))) != -1) {
            read += len;
            consumer.execute(buf, len);
        }
    }

    // - Channel reading methods -

    /**
     * Reads all available bytes from a channel in blocks of 1024 and calls a consumer with the respective buffer.
     * <p>
     * This method does not close the channel.
     *
     * @param ch       the channel to read from
     * @param consumer the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readAllBlockwise(@NotNull ReadableByteChannel ch,
                                                              @NotNull ParamTask<? super ByteBuffer, X> consumer)
    throws IOException, X {
        readBlockwise(ch, -1, consumer);
    }

    /**
     * Reads up to N bytes from a channel in blocks of 1024 and calls a consumer with the respective buffer.
     *
     * @param ch       the channel to read from
     * @param limit    the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readBlockwise(@NotNull ReadableByteChannel ch, long limit,
                                                           @NotNull ParamTask<? super ByteBuffer, X> consumer)
    throws IOException, X {
        readBlockwise(1024, ch, limit, consumer);
    }

    /**
     * Reads all available bytes from a channel in blocks of a certain size and calls a consumer with the respective
     * buffer.
     * <p>
     * This method does not close the channel.
     *
     * @param blockSize the block size
     * @param ch        the channel to read from
     * @param consumer  the consumer, called with the buffer and the read length
     */
    public static <X extends Exception> void readAllBlockwise(int blockSize, @NotNull ReadableByteChannel ch,
                                                              @NotNull ParamTask<? super ByteBuffer, X> consumer)
    throws IOException, X {
        readBlockwise(blockSize, ch, -1, consumer);
    }

    /**
     * Reads up to N bytes from a channel in blocks of a certain size and calls a consumer with the respective buffer.
     *
     * @param blockSize the block size
     * @param ch        the channel to read from
     * @param limit     the maximum amount of bytes to read, &lt; 0 for no limit
     * @param consumer  the consumer, called with a read-only buffer ready for reading
     */
    public static <X extends Exception> void readBlockwise(int blockSize, @NotNull ReadableByteChannel ch, long limit,
                                                           @NotNull ParamTask<? super ByteBuffer, X> consumer)
    throws IOException, X {
        if (limit == 0) return;
        if (blockSize <= 0) throw new IllegalArgumentException("Invalid blocksize");

        var e = Internal.getDefaultBufferCapacity() >= blockSize ? Pool.BYTE_BUFFER_DEFAULT_POOL.obtain() : null;
        try {
            var buf = e != null ? e.get().limit(blockSize) : Internal.bufalloc(blockSize);

            if (limit < 0) { // read all
                while (ch.read(buf.rewind()) != -1) {
                    consumer.execute(buf.asReadOnlyBuffer().flip());
                }
                return;
            }

            // limited read
            int read = 0, len;
            while (read < limit && (len = ch.read(buf.rewind().limit(Math.min(blockSize, (int) Math.min(limit - read, Integer.MAX_VALUE))))) != -1) {
                read += len;
                consumer.execute(buf.asReadOnlyBuffer().flip());
            }
        } finally {
            if (e != null) e.release();
        }
    }

    /**
     * Transfers all available bytes from one channel to another.
     * <p>
     * This method does not close either of the channels.
     *
     * @param src the source channel
     * @param dst the destination channel
     * @see InputStream#transferTo(OutputStream)
     */
    public static void transfer(@NotNull ReadableByteChannel src, @NotNull WritableByteChannel dst)
    throws IOException {
        transfer(src, dst, -1);
    }

    /**
     * Transfers up to N bytes from one channel to another.
     *
     * @param src   the source channel
     * @param dst   the destination channel
     * @param limit the maximum amount of bytes to transfer, &lt; 0 for no limit
     * @see InputStream#transferTo(OutputStream)
     */
    public static void transfer(@NotNull ReadableByteChannel src, @NotNull WritableByteChannel dst, long limit)
    throws IOException {
        readBlockwise(src, limit, dst::write);
    }

    // - data extract methods -

    /**
     * Reads all available bytes from an input stream.
     * <p>
     * This method does not close the stream.
     *
     * @param in the stream to read from
     * @return the bytes read
     *
     * @see InputStream#transferTo(OutputStream)
     */
    public static byte @NotNull [] readAll(@NotNull InputStream in) throws IOException {
        return read(in, -1);
    }

    /**
     * Reads up to N bytes from an input stream.
     * <p>
     * This method does not close the stream.
     *
     * @param in    the stream to read from
     * @param limit the maximum amount of bytes to read, &lt; 0 for no limit
     * @return the bytes read
     * @see InputStream#transferTo(OutputStream)
     */
    public static byte @NotNull [] read(@NotNull InputStream in, long limit) throws IOException {
        // no read
        if (limit == 0) return EMPTY_BYTE_ARRAY;

        try (var bout = new ByteArrayOutputStream()) {
            LimitInputStream.limited(in, limit).transferTo(bout);
            return bout.toByteArray();
        }
    }

    /**
     * Reads all available chars from a reader.
     * <p>
     * This method does not close the reader.
     *
     * @param reader the reader to read from
     * @return the bytes read
     *
     * @see InputStream#transferTo(OutputStream)
     */
    public static char @NotNull [] readAll(@NotNull Reader reader) throws IOException {
        return read(reader, -1);
    }

    /**
     * Reads up to N chars from a reader.
     *
     * @param reader the reader to read from
     * @param limit  the maximum amount of chars to read, &lt; 0 for no limit
     * @return the bytes read
     * @see InputStream#transferTo(OutputStream)
     */
    public static char @NotNull [] read(@NotNull Reader reader, long limit) throws IOException {
        // no read
        if (limit == 0) return new char[0];

        try (var cout = new CharArrayWriter()) {
            if (limit < 0) { // unlimited read
                reader.transferTo(cout);
            } else { // limited read
                readBlockwise(1024, reader, limit, (buf, len) -> cout.write(buf, 0, len));
            }
            return cout.toCharArray();
        }
    }

    /**
     * Reads all available bytes from a channel.
     * <p>
     * This method does not close the channel.
     *
     * @param ch the channel to read from
     * @return a writable byte buffer containing the bytes
     */
    @NotNull
    public static ByteBuffer readAll(@NotNull ReadableByteChannel ch) throws IOException {
        return read(ch, -1);
    }

    /**
     * Reads up to N bytes from a channel. This method does not close the channel.
     *
     * @param ch    the channel to read from
     * @param limit the maximum amount of bytes to read, &lt; 0 for no limit
     * @return a writable byte buffer containing the bytes
     */
    @NotNull
    public static ByteBuffer read(@NotNull ReadableByteChannel ch, long limit) throws IOException {
        // no read
        if (limit == 0) return EMPTY_BYTE_BUFFER;

        // read up to limit or up to EOF
        try (var buf = new PipeBufferChannel()) {
            readBlockwise(ch, limit, buf::write);
            return buf.obtain();
        }
    }

    /**
     * Reads all bytes from an input stream into a string. This method does not close the stream.
     *
     * @param in the stream to read from
     * @return the bytes read
     *
     * @see InputStream#transferTo(OutputStream)
     */
    @NotNull
    public static String toString(@NotNull InputStream in) throws IOException {
        return new String(read(in, -1));
    }

    // - misc -

    /**
     * Closes one or more closable objects while quietly discarding {@link IOException IOExceptions}.
     * <p>
     * Any other exception will be supressed, chained together and eventually thrown, in order to guarantee all objects
     * will be closed.
     */
    public static void closeQuietly(@NotNull AutoCloseable closeable, @NotNull AutoCloseable @NotNull ... closeables) {
        closeQuietly(List.of(ArrayUtil.concat(closeable, closeables)));
    }

    /**
     * Closes all closable objects queried from the iterable object while quietly discarding
     * {@link IOException IOExceptions}.
     * <p>
     * Any other exception will be suppressed, chained together and eventually thrown, in order to guarantee all objects
     * will be closed.
     */
    public static void closeQuietly(@NotNull Iterable<? extends AutoCloseable> iterable) {
        closeQuietly(iterable.iterator());
    }

    /**
     * Closes all closable objects queried from the iterator while quietly discarding
     * {@link Exception checked exceptions}.
     * <p>
     * Any other exception will be suppressed, chained together and eventually thrown, in order to guarantee all objects
     * will be closed.
     */
    public static void closeQuietly(@NotNull Iterator<? extends AutoCloseable> iterator) {
        RuntimeException re = null;

        while (iterator.hasNext()) {
            try {
                iterator.next().close();
            } catch (RuntimeException e) {
                if (re == null) {
                    re = e;
                    continue;
                }
                re.addSuppressed(e);
            } catch (Exception e) {
                // ignored
            }
        }

        if (re != null) throw re;
    }

    /**
     * Closes one or more closable objects.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void closeIO(@NotNull Closeable closeable, @NotNull Closeable @NotNull ... closeables)
    throws IOException {
        closeIO(List.of(ArrayUtil.concat(closeable, closeables)));
    }

    /**
     * Closes one or more closable objects.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void close(@NotNull AutoCloseable closeable, @NotNull AutoCloseable @NotNull ... closeables)
    throws Exception {
        close(List.of(ArrayUtil.concat(closeable, closeables)));
    }

    /**
     * Closes all closable objects queried from the iterable object.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void closeIO(@NotNull Iterable<? extends Closeable> iterable)
    throws IOException {
        closeIO(iterable.iterator());
    }

    /**
     * Closes all closable objects queried from the iterable object.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void close(@NotNull Iterable<? extends AutoCloseable> iterable)
    throws Exception {
        close(iterable.iterator());
    }

    /**
     * Closes all closable objects queried from the iterator.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void closeIO(@NotNull Iterator<? extends Closeable> iterator)
    throws IOException {
        try {
            close(iterator);
        } catch (Exception e) {
            // exception must be of type IOE or RE as per definition in Closable class
            if (e instanceof IOException ioe) throw ioe;
            if (e instanceof RuntimeException re) throw re;

            // make compiler happy
            throw new UndefinedStateError(e);
        }
    }

    /**
     * Closes all closable objects queried from the iterator.
     * <p>
     * Exceptions will be suppressed, chained together and eventually thrown, in order to guarantee all objects will be
     * closed.
     */
    public static void close(@NotNull Iterator<? extends AutoCloseable> iterator)
    throws Exception {
        Exception thrown = null;

        // close & catch
        while (iterator.hasNext()) {
            try {
                iterator.next().close();
            } catch (Exception e) {
                if (thrown == null) {
                    thrown = e;
                    continue;
                }
                thrown.addSuppressed(e);
            }
        }

        if (thrown != null) throw thrown;
    }

    /**
     * Opens a closeable resource using the given function, performs a given operation on it and closes it again in any
     * case.
     *
     * @param supplier     the function to open the resource
     * @param func         the operation to perform on the resource
     * @param closeQuietly whether to close the resource quietly
     * @param <R>          the return type of the operation
     * @param <T>          the type of the resource
     * @param <X>          the type of exception thrown by the supplier
     * @param <X2>         the type of exception thrown by the operation
     * @return the result of the operation
     * @throws X  any exception thrown by the supplier
     * @throws X2 any exception thrown by the operation
     */
    @SuppressWarnings("ThrowFromFinallyBlock")
    @BypassAnnotationEnforcement
    public static <R, T extends AutoCloseable, X extends Exception, X2 extends Exception> R
    op(@NotNull Func<T, X> supplier,
       @NotNull ParamFunc<? super T, R, X2> func, boolean closeQuietly) throws X, X2 {
        var t = supplier.execute();
        try {
            return func.execute(t);
        } finally {
            if (closeQuietly) closeQuietly(t);
            else {
                try {
                    t.close();
                } catch (IOException e) {
                    throw new SoftIOException(e);
                } catch (Exception e) {
                    throw new SoftException(e);
                }
            }
        }
    }

    /**
     * Wraps a {@link ReadableByteChannel} into a {@link SeekableByteChannel}. If the channel is already seekable, it
     * will be returned as-is.
     * <p>
     * The new channel will be backed by a dynamically growing {@link ByteBuffer} with a maximum size of
     * {@code memoryLimit}. If the buffer is full, the cached data will be written to a temporary file which will be
     * used as a backing store from then on. The temporary file will be deleted when the channel is closed. If the file
     * backing store is disabled, an {@link BufferOverflowException} will be thrown when the memory limit is reached.
     * <p>
     * The new channel will be non-writable.
     *
     * @param channel     the channel to wrap
     * @param memoryLimit the maximum size of the in-memory buffer, in bytes
     * @param allowFile   whether to allow the use of a temporary file as a backing store
     * @return the seekable channel
     */
    public static @NotNull SeekableByteChannel toSeekableByteChannel(@NotNull ReadableByteChannel channel, int memoryLimit, boolean allowFile)
    throws IOException {
        return channel instanceof SeekableByteChannel sch
               ? sch
               : new SeekableChannelAdapter(channel, memoryLimit, allowFile);
    }


    /**
     * Creates a new {@link Thread} that redirects all data read from an {@link InputStream} to an
     * {@link OutputStream}.
     * <p>
     * The {@link Thread} will not be started by this method.
     *
     * @param in  the stream to read from
     * @param out the stream to write to
     * @return the thread
     *
     * @see InputStream#transferTo(OutputStream)
     */
    @NotNull
    public static Thread redirect(@NotNull InputStream in, @NotNull OutputStream out) {
        return redirect(in, out, Thread::new);
    }

    /**
     * Creates a new {@link Thread} that redirects all data read from an {@link InputStream} to an {@link OutputStream}.
     * Any exception thrown by {@link InputStream#read()} will not be caught.
     * <p>
     * The {@link Thread} will not be started by this method.
     *
     * @param in      the stream to read from
     * @param out     the stream to write to
     * @param factory the factory to create the thread
     * @return the thread
     *
     * @see InputStream#transferTo(OutputStream)
     */
    @NotNull
    public static Thread redirect(@NotNull InputStream in, @NotNull OutputStream out, @NotNull ThreadFactory factory) {
        return factory.newThread(prepareRedirect(in, out));
    }

    /**
     * Creates a new {@link Runnable} that redirects all data read from an {@link InputStream} to an
     * {@link OutputStream}. Any exception thrown by {@link InputStream#read()} will not be caught.
     *
     * @param in  the stream to read from
     * @param out the stream to write to
     * @return the runnable
     *
     * @see InputStream#transferTo(OutputStream)
     */
    @NotNull
    public static Task<IOException> prepareRedirect(@NotNull InputStream in, @NotNull OutputStream out) {
        return () -> {
            int b;
            while (!Thread.interrupted() && (b = in.read()) != -1) {
                out.write(b);
            }
        };
    }

    /**
     * Reads lines from a stream (utilizing a {@link Scanner}) and compares it against a success and failure condition.
     *
     * @param stream  the stream to read the data from
     * @param success the success condition predicate
     * @param failure the failure condition predicate
     * @return {@code true} on success, {@code false} on failure, if an exception is thrown or the stream closes
     * @see Conditions#waitFor(Func, Predicate, Predicate)
     */
    public static boolean waitForStreamLine(@NotNull InputStream stream, @NotNull Predicate<? super @NotNull String> success,
                                            @NotNull Predicate<? super @NotNull String> failure) {
        var sc = new Scanner(stream);
        return Conditions.waitFor(() -> sc.hasNextLine() ? sc.nextLine() : null, success, failure);
    }
}
