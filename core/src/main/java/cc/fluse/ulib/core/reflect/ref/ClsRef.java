package cc.fluse.ulib.core.reflect.ref;

import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

/**
 * Class reference.
 */
public interface ClsRef extends Ref<Class<?>> {
    @NotNull
    default Expect<Class<?>, ClassNotFoundException> tryLoad() {
        return ReflectUtil.tryWithLoaders(l -> Class.forName(getName(), true, l));
    }
}
