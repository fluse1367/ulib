package cc.fluse.ulib.core.impl.patch;

import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.impl.inject.ClassLoaderDelegationHook;
import cc.fluse.ulib.core.inject.HookInjection;
import cc.fluse.ulib.core.inject.HookPoint;
import cc.fluse.ulib.core.inject.InjectUtil;
import cc.fluse.ulib.core.reflect.ReflectUtil;

/**
 * @see <a
 * href="https://github.com/HotswapProjects/HotswapAgent/blob/5064e775134b7d64c8c2a1110330ab0bf61646e9/hotswap-agent-core/src/main/java/org/hotswap/agent/util/classloader/ClassLoaderDefineClassPatcher.java#L93">Source
 * code to patch</a>
 */
public class HotswapAgentPluginClassloaderPatcherPatch implements Task<Exception> {
    @Override
    public void execute() throws Exception {
        // check if the hotswapagent is available
        if (ReflectUtil.forName("org.hotswap.agent.util.classloader.ClassLoaderDefineClassPatcher", false).isEmpty()) {
            return;
        }

        // patch the classloader define class method
        new HookInjection()
                .<Void>addHook("org/hotswap/agent/util/classloader/ClassLoaderDefineClassPatcher",
                        "patch(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/security/ProtectionDomain;)V",
                        InjectUtil.createHookingSpec(HookPoint.METHOD_CALL, "Lorg/hotswap/agent/javassist/CtClass;toClass(Ljava/lang/ClassLoader;Ljava/security/ProtectionDomain;)Ljava/lang/Class;"),
                        (params, cb) -> {
                            final var cl = (ClassLoader) params[2];
                            // test if the classloader is a delegation target
                            if (ClassLoaderDelegationHook.HOOKS.stream().anyMatch(h -> h.checkCl(cl))) {
                                // if so, cancel the class definition
                                cb.cancel();
                            }
                        }
                )
                .injectNowFlat();
    }
}
