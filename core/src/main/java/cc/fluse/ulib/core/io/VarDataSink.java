package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A data sink that supports writing variable-length integers and arrays.
 *
 * @see VarNum
 */
public interface VarDataSink extends DataSink {

    /**
     * @see VarNum#writeVarInt(int, ParamTask)
     */
    default void writeVarInt(int i) throws IOException {
        VarNum.writeVarInt(i, this::write);
    }

    /**
     * @see VarNum#writeVarLong(long, ParamTask)
     */
    default void writeVarLong(long l) throws IOException {
        VarNum.writeVarLong(l, this::write);
    }

    /**
     * Writes a variable-length array to this channel. The array is encoded as a variable-length integer specifying the
     * length of the array, followed by the array contents.
     * <p>
     * The encoding format is: <pre> {@code <len: varint32> <data: uint8[]>}</pre>
     *
     * @param arr the array
     * @throws IOException if an I/O error occurs
     */
    default void writeVarArray(byte @NotNull [] arr) throws IOException {
        writeVarInt(arr.length);
        write(arr);
    }

    /**
     * Writes a variable-length array to this channel. The array is encoded as a variable-length integer specifying the
     * length of the array, followed by the array contents.
     * <p>
     * The encoding format is: <pre> {@code <len: varint32> <data: uint8[]>}</pre>
     *
     * @param src the source buffer containing the array bytes
     * @throws IOException if an I/O error occurs
     */
    default void writeVarArray(@NotNull ByteBuffer src) throws IOException {
        writeVarInt(src.remaining());
        write(src);
    }

    /**
     * Writes a variable-length array to this channel. The array is encoded as a variable-length integer specifying the
     * length of the array, followed by the array contents. The given filler function is called repeatedly to fill the
     * given buffer with the array contents up to the given length.
     *
     * @param len    the length of the array
     * @param filler the filler function, retrieving the buffer to fill and returning the number of bytes written to it
     * @param <X>    the type of exception thrown by the filler function
     * @throws IOException if an I/O error occurs
     * @throws X           if the filler function throws an exception
     */
    default <X extends Exception> void writeVarArray(int len, @NotNull ParamFunc<? super ByteBuffer, Integer, X> filler) throws IOException, X {
        writeVarInt(len);
        try (var e = Pool.BYTE_BUFFER_POOL.obtain()) {
            var buf = e.get();

            var written = 0;
            while (written < len) {
                int w = 0, c = filler.execute(buf.clear().limit(Math.min(len - written, buf.capacity())));

                buf.flip();
                while (buf.hasRemaining()) w += write(buf);

                if (w != c)
                    throw new UndefinedStateError("Expected to write %d bytes, but wrote %d bytes".formatted(c, w));

                written += w;
            }
        }
    }


}
