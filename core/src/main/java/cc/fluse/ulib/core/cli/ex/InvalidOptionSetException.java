package cc.fluse.ulib.core.cli.ex;

import cc.fluse.ulib.core.cli.CliOption;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class InvalidOptionSetException extends OptionException {

    @Getter
    @Nullable
    private final CliOption otherOption;

    public InvalidOptionSetException(@NotNull CliOption option, @Nullable CliOption otherOption, @NotNull String message) {
        super(option, "Option '%s' %s".formatted(option.getName(), message));
        this.otherOption = otherOption;
    }
}
