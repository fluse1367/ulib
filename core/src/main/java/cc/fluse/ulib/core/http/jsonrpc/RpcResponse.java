package cc.fluse.ulib.core.http.jsonrpc;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public interface RpcResponse extends RpcObject {
    /**
     * The result can either a JSON primitive type (represented respectively either as a {@link Number}, {@link Boolean}
     * or {@link String}), a JSON Object (represented as {@link JsonConfiguration}), or an {@link Collection} composed
     * of those.
     */
    @NotNull
    Optional<Object> getResult();

    @NotNull
    Optional<RpcResponseError> getError();

    interface RpcResponseError {
        int getCode();

        @NotNull
        String getMessage();

        @NotNull
        default Optional<StandardError> asSpecStandard() {
            return Optional.ofNullable(StandardError.BY_CODE.get(getCode()));
        }

        @NotNull
        Optional<Object> getData();
    }

    enum StandardError {
        PARSE_ERROR(-32700, "Parse Error", "Error while parsing into JSON Object"),
        INVALID_REQUEST(-32600, "Invalid Request", "The JSON Object sent is not a valid request"),
        METHOD_NOT_FOUND(-32601, "Method not found", "The requested method does not exist or is not available"),
        INVALID_PARAMETERS(-32602, "Invalid Parameters", "Invalid parameters provided for selected method"),
        INTERNAL_ERROR(-32603, "Internal Error", "Internal JSON-RPC error"),
        ;

        private static final Map<Integer, StandardError> BY_CODE = new HashMap<>();

        static {
            for (StandardError value : StandardError.values()) {
                BY_CODE.put(value.code, value);
            }
        }

        @Getter
        private final int code;
        @Getter
        @NotNull
        private final String name;
        @Getter
        @NotNull
        private final String defaultDescription;

        StandardError(int code, @NotNull String name, @NotNull String defaultDescription) {
            this.code = code;
            this.name = name;
            this.defaultDescription = defaultDescription;
        }

        @NotNull
        public String toString(@NotNull String desc) {
            return "StandardError [%s/%d]: %s".formatted(name, code, desc);
        }

        @Override
        @NotNull
        public String toString() {
            return toString(defaultDescription);
        }
    }
}
