package cc.fluse.ulib.core.inject;

import cc.fluse.ulib.core.function.BiParamFunc;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Bitmask;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

/**
 * A container for class loading hooks. The hooks may throw exceptions,
 * however in this case the respective method will return {@code null}.
 *
 * @see InjectUtil
 */
@SuppressWarnings("JavadocReference")
public final class ClassLoaderDelegation {

    /**
     * Flag for enabling delegation of class loading ({@link ClassLoader#loadClass(String)}).
     */
    public static final int FLAG_DELEGATE_LOAD_CLASSES = 0x01;
    /**
     * Flag for enabling delegation of class finding ({@link ClassLoader#findClass(String)} and
     * {@link ClassLoader#findClass(String, String)}).
     */
    public static final int FLAG_DELEGATE_FIND_CLASSES = 0x02;
    /**
     * Flag for enabling delegation of resource finding ({@link ClassLoader#findResource(String)},
     * {@link ClassLoader#findResource(String, String)} and {@link ClassLoader#findResources(String)}).
     */
    public static final int FLAG_DELEGATE_FIND_RESOURCES = 0x04;

    // default delegation does nothing
    private static final BiParamFunc<String, Boolean, Class<?>, Exception> DEFAULT_DELEGATE_LOAD_CLASS = (name, resolve) -> null;
    private static final ParamFunc<String, Class<?>, Exception> DEFAULT_DELEGATE_FIND_CLASS = (name) -> null;
    private static final BiParamFunc<String, String, Class<?>, Exception> DEFAULT_DELEGATE_FIND_MODULE_CLASS = (module, name) -> null;
    private static final ParamFunc<String, URL, Exception> DEFAULT_DELEGATE_FIND_RESOURCE = (name) -> null;
    private static final BiParamFunc<String, String, URL, Exception> DEFAULT_DELEGATE_FIND_MODULE_RESOURCE = (module, name) -> null;
    private static final ParamFunc<String, Enumeration<URL>, Exception> DEFAULT_DELEGATE_FIND_RESOURCES = (name) -> null;

    private final ClassLoader target;
    private final BiParamFunc<String, Boolean, Class<?>, Exception> delegateLoadClass;
    private final ParamFunc<String, Class<?>, Exception> delegateFindClass;
    private final BiParamFunc<String, String, Class<?>, Exception> delegateFindModuleClass;
    private final BiParamFunc<String, String, URL, Exception> delegateFindModuleResource;
    private final ParamFunc<String, URL, Exception> delegateFindResource;
    private final ParamFunc<String, Enumeration<URL>, Exception> delegateFindResources;

    final AtomicReference<Map<String, Map<Class<?>[], Function<Object[], Optional<String>>>>> additional =
        new AtomicReference<>(new HashMap<>());

    /**
     * Constructs a delegation container that forwards any class loading request to the specified function.
     *
     * @param delegateLoadClass       see {@link ClassLoader#loadClass(String)}
     * @param delegateFindClass       see {@link ClassLoader#findClass(String)}
     * @param delegateFindModuleClass see {@link ClassLoader#findClass(String, String)}
     */
    public ClassLoaderDelegation(@NotNull BiParamFunc<String, Boolean, Class<?>, Exception> delegateLoadClass,
                                 @NotNull ParamFunc<String, Class<?>, Exception> delegateFindClass,
                                 @NotNull BiParamFunc<String, String, Class<?>, Exception> delegateFindModuleClass) {
        this(delegateLoadClass, delegateFindClass, delegateFindModuleClass,
            DEFAULT_DELEGATE_FIND_MODULE_RESOURCE, DEFAULT_DELEGATE_FIND_RESOURCE, DEFAULT_DELEGATE_FIND_RESOURCES);
    }

    /**
     * Constructs a delegation container that forwards any class &amp; resource loading request to the specified
     * function.
     *
     * @param delegateLoadClass          see {@link ClassLoader#loadClass(String)}
     * @param delegateFindClass          see {@link ClassLoader#findClass(String)}
     * @param delegateFindModuleClass    see {@link ClassLoader#findClass(String, String)}
     * @param delegateFindModuleResource see {@link ClassLoader#findResource(String, String)}
     * @param delegateFindResource       see {@link ClassLoader#findResource(String)}
     */
    public ClassLoaderDelegation(@NotNull BiParamFunc<String, Boolean, Class<?>, Exception> delegateLoadClass,
                                 @NotNull ParamFunc<String, Class<?>, Exception> delegateFindClass,
                                 @NotNull BiParamFunc<String, String, Class<?>, Exception> delegateFindModuleClass,
                                 @NotNull BiParamFunc<String, String, URL, Exception> delegateFindModuleResource,
                                 @NotNull ParamFunc<String, URL, Exception> delegateFindResource,
                                 @NotNull ParamFunc<String, Enumeration<URL>, Exception> delegateFindResources) {
        this.target = null;
        this.delegateLoadClass = delegateLoadClass;
        this.delegateFindClass = delegateFindClass;
        this.delegateFindModuleClass = delegateFindModuleClass;
        this.delegateFindModuleResource = delegateFindModuleResource;
        this.delegateFindResource = delegateFindResource;
        this.delegateFindResources = delegateFindResources;
    }

    /**
     * Constructs a delegation container that forwards any class loading request to the specified class loader. The
     * delegation <b>won't</b> forward resource loading requests.
     *
     * @param delegateTarget the class loader the requests should be forwarded to
     */
    public ClassLoaderDelegation(@NotNull ClassLoader delegateTarget) {
        this(delegateTarget, false);
    }

    /**
     * Constructs a delegation container that forwards any class &amp; resource loading request to the specified class
     * loader.
     *
     * @param delegateTarget        the class loader the requests should be forwarded to
     * @param delegateFindResources whether to delegate resource finding requests
     */
    public ClassLoaderDelegation(@NotNull ClassLoader delegateTarget, boolean delegateFindResources) {
        this(delegateTarget, FLAG_DELEGATE_LOAD_CLASSES | FLAG_DELEGATE_FIND_CLASSES | (delegateFindResources ? FLAG_DELEGATE_FIND_RESOURCES : 0));
    }

    /**
     * Constructs a delegation container that forwards requests to the specified class loader.
     *
     * @param delegateTarget the class loader the requests should be forwarded to
     * @param flag           bitset consisting of {@link #FLAG_DELEGATE_LOAD_CLASSES},
     *                       {@link #FLAG_DELEGATE_FIND_CLASSES} and {@link #FLAG_DELEGATE_FIND_RESOURCES}
     * @see Bitmask
     */
    public ClassLoaderDelegation(@NotNull ClassLoader delegateTarget, int flag) {
        this.target = Objects.requireNonNull(delegateTarget);

        if ((flag & FLAG_DELEGATE_LOAD_CLASSES) != 0) {
            this.delegateLoadClass = (name, resolve) -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(Class.class, target, "loadClass()",
                    Param.listOf(String.class, name, boolean.class, resolve))
            );
        } else {
            this.delegateLoadClass = DEFAULT_DELEGATE_LOAD_CLASS;
        }

        if ((flag & FLAG_DELEGATE_FIND_CLASSES) != 0) {
            this.delegateFindClass = name -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(Class.class, target, "findClass()",
                    Param.listOf(String.class, name))
            );
            this.delegateFindModuleClass = (module, name) -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(Class.class, target, "findClass()",
                    Param.listOf(String.class, module, String.class, name))
            );
        } else {
            this.delegateFindClass = DEFAULT_DELEGATE_FIND_CLASS;
            this.delegateFindModuleClass = DEFAULT_DELEGATE_FIND_MODULE_CLASS;
        }

        if ((flag & FLAG_DELEGATE_FIND_RESOURCES) != 0) {
            this.delegateFindResource = name -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(URL.class, target, "findResource()",
                    Param.listOf(String.class, name))
            );
            this.delegateFindModuleResource = (module, name) -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(URL.class, target, "findResource()",
                    Param.listOf(String.class, module, String.class, name))
            );
            this.delegateFindResources = name -> ReflectUtil.doPrivileged(() ->
                ReflectUtil.icall(Enumeration.class, target, "findResources()",
                    Param.listOf(String.class, name))
            );
        } else {
            this.delegateFindResource = DEFAULT_DELEGATE_FIND_RESOURCE;
            this.delegateFindModuleResource = DEFAULT_DELEGATE_FIND_MODULE_RESOURCE;
            this.delegateFindResources = DEFAULT_DELEGATE_FIND_RESOURCES;
        }

    }

    /**
     * Marks an additional method to be injected as well.
     *
     * @param method            the name of the method
     * @param paramTypes        the method's parameter types
     * @param classNameResolver a function which is able to resolve the class name that should be loaded out of the calling parameters
     */
    public void additionally(@NotNull String method, @NotNull Class<?>[] paramTypes, @NotNull Function<? super Object[], Optional<String>> classNameResolver) {
        if (this.additional.getPlain() == null)
            throw new IllegalStateException();

        this.additional.getPlain()
                .computeIfAbsent(method, __ -> new HashMap<>())
                .computeIfAbsent(paramTypes, __ -> classNameResolver::apply);

    }

    @Nullable
    public Class<?> loadClass(@NotNull String name, boolean resolve) {
        return Expect.compute(delegateLoadClass, name, resolve).orElse(null);
    }

    @Nullable
    public Class<?> findClass(@NotNull String name) {
        return Expect.compute(delegateFindClass, name).orElse(null);
    }

    @Nullable
    public Class<?> findClass(@NotNull String module, @NotNull String name) {
        return Expect.compute(delegateFindModuleClass, module, name).orElse(null);
    }

    @Nullable
    public URL findResource(@NotNull String module, @NotNull String name) {
        return Expect.compute(delegateFindModuleResource, module, name).orElse(null);
    }

    @Nullable
    public URL findResource(@NotNull String name) {
        return Expect.compute(delegateFindResource, name).orElse(null);
    }

    public @Nullable Enumeration<URL> findResources(@NotNull String name) {
        return Expect.compute(delegateFindResources, name).orElse(null);
    }

    @Override
    @NotNull
    public String toString() {
        return target != null ?
            "ClassLoaderDelegation[target=%s,resource-delegation:%s]".formatted(target,
                delegateFindResource != DEFAULT_DELEGATE_FIND_RESOURCE || delegateFindModuleResource != DEFAULT_DELEGATE_FIND_MODULE_RESOURCE || delegateFindResources != DEFAULT_DELEGATE_FIND_RESOURCES)
            : "ClassLoaderDelegation{" +
            "delegateLoadClass=" + delegateLoadClass +
            ", delegateFindClass=" + delegateFindClass +
            ", delegateFindModuleClass=" + delegateFindModuleClass +
            ", delegateFindModuleResource=" + delegateFindModuleResource +
            ", delegateFindResource=" + delegateFindResource +
            ", delegateFindResources=" + delegateFindResources +
            '}';
    }
}
