package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.database.RemoteDatabase;

/**
 * Representation of mysql type databases.
 */
public interface MySQLDatabase extends SqlDatabase, RemoteDatabase {}
