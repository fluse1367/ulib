package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.Quadruple;

public class QuadrupleImpl<T, U, V, W> extends TripleImpl<T, U, V> implements Quadruple<T, U, V, W> {

    public QuadrupleImpl(T t, U u, V v, W w) {
        super(t, u, v, w);
    }

    protected QuadrupleImpl(Object... elements) {
        super(elements);
    }

    @Override
    public W getFourth() {
        return getI(3);
    }
}
