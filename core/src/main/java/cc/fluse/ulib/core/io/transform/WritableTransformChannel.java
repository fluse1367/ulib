package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Writable;
import cc.fluse.ulib.core.io.channel.WritableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.Flushable;

/**
 * A {@link TransformerChainChannel} that can be written to.
 */
public interface WritableTransformChannel extends TransformerChainChannel, Writable, WritableDataChannel, Flushable {
    @Override
    @NotNull
    default WritableDataChannel channel() {
        return IOUtil.isolate(this);
    }
}
