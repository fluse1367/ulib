/**
 * Classes for object serialization and deserialization.
 */
package cc.fluse.ulib.core.configuration.serialization;