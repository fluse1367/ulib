package cc.fluse.ulib.core.http.jsonrpc;

import cc.fluse.ulib.core.http.HttpRequestExecutor;
import cc.fluse.ulib.core.impl.http.jsonrpc.v2.V2Endpoint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.*;

public interface RpcEndpoint {

    @NotNull
    static RpcEndpoint v2(@NotNull URI uri, @NotNull HttpRequestExecutor httpExecutor) {
        return new V2Endpoint(uri, httpExecutor);
    }

    @NotNull
    default Monitor startMonitor(@NotNull Function<RpcEndpoint, RpcRequest> dummyRequestSupplier, @NotNull Runnable onInvalidation) {
        return startMonitor(dummyRequestSupplier, 3, m -> m.onInvalidation(__ -> onInvalidation.run()));
    }

    /**
     * Stars a monitor, monitoring this endpoint. The monitor will immediately start an initial request, blocking this
     * method.
     *
     * @param dummyRequestSupplier  a supplier supplying dummy requests for checking the endpoint
     * @param invalidationThreshold after how many subsequent failed requests the monitor should invalidate the
     *                              endpoint, 0 to disable
     * @param preInit               a consumer that is run before the initial request, providing the monitor instance,
     *                              allowing to register event handlers
     * @return the monitor
     *
     * @apiNote use {@link Monitor#invalidate()} to invalidate the monitor and (this endpoint) to allow for successful
     * garbage collection
     */
    @NotNull
    Monitor startMonitor(@NotNull Function<RpcEndpoint, RpcRequest> dummyRequestSupplier,
                         int invalidationThreshold,
                         @NotNull Consumer<RpcEndpoint.Monitor> preInit);

    /**
     * @return an optional wrapping the monitor (if started), an empty optional otherwise
     */
    @NotNull
    Optional<Monitor> getMonitor();

    @NotNull
    String getRpcVersion();

    @NotNull
    URI getEndpointUri();

    @NotNull
    RpcRequest createRequest(@NotNull String method, @Nullable Object parameters);

    @NotNull
    CompletableFuture<RpcResponse> sendRequest(@NotNull RpcRequest request);

    @NotNull
    default CompletableFuture<RpcResponse> sendRequest(@NotNull String method, @Nullable Object parameters) {
        return sendRequest(createRequest(method, parameters));
    }

    interface Monitor {
        /**
         * @return the monitoring endpoint
         */
        @NotNull
        RpcEndpoint getEndpoint();

        /**
         * @return when the last check was started
         */
        @NotNull
        Instant getLastCheck();

        /**
         * @return how long it took the server to respond
         */
        @NotNull
        Duration getResponseTime();

        /**
         * @return {@code true} if the server sent a response, {@code false} if the connection failed
         */
        boolean wasOnline();

        /**
         * @return the last response the server sent, or {@code null} if the connection failed
         *
         * @see #wasOnline()
         */
        @Nullable
        RpcResponse getResponse();

        /**
         * Runs a check as soon as possible. Returns immediately as the check is scheduled.
         */
        void check();

        /**
         * Invalidates the endpoint.
         */
        void invalidate();

        /**
         * Adds a subscription. The subscriber is notified when the server sends a response to a check.
         *
         * @param eventHandler the subscriber
         */
        void onResponse(@NotNull Consumer<Monitor> eventHandler);

        /**
         * Adds a subscription. The subscriber is notified when the connection fails.
         *
         * @param eventHandler the subscriber
         */
        void onFailure(@NotNull BiConsumer<RpcRequest, Monitor> eventHandler);

        /**
         * Adds a subscription. The subscriber is notified when the monitor gets invalidated.
         *
         * @param eventHandler the subscriber
         */
        void onInvalidation(@NotNull Consumer<Monitor> eventHandler);
    }
}
