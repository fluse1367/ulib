package cc.fluse.ulib.test.iotransformer;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.ByteBufferReadChannel;
import cc.fluse.ulib.core.io.channel.PipeBufferChannel;
import cc.fluse.ulib.core.io.transform.ByteTransformer;
import cc.fluse.ulib.core.io.transform.TransformChannel;
import cc.fluse.ulib.test.TestUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.zip.*;

public class CompressionTransformerTest {

    @Test
    public void testCompression() throws IOException {
        var sample = IOTransformerTest.genSample(5128, 1234L);

        // compress with pipe
        ByteBuffer compressedSample;
        try (var pipe = TransformChannel.create((ReadableByteChannel) new ByteBufferReadChannel(sample))) {
            pipe.addTransformer(ByteTransformer.of(new Deflater()));
            compressedSample = IOUtil.readAll(pipe);
        }

        // decompress
        ByteBuffer decompressedSample;
        try (var out = new PipeBufferChannel()) {
            var decompressor = new Inflater();
            decompressor.setInput(compressedSample);
            IOUtil.transfer(IOUtil.synthesizeReadChannel(() -> !decompressor.finished(), dst -> {
                try {
                    return decompressor.inflate(dst);
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            }), out);
            decompressor.end();
            decompressedSample = out.obtain();
        }

        TestUtils.assertBBufEquals(sample.rewind(), decompressedSample);
    }

    @Test
    public void testDecompression() throws IOException {
        var sample = IOTransformerTest.genSample(5128, 1234L);

        // compress
        ByteBuffer compressedSample;
        try (var out = new PipeBufferChannel()) {
            var compressor = new Deflater();
            compressor.setInput(sample);
            compressor.finish();
            IOUtil.transfer(IOUtil.synthesizeReadChannel(() -> !compressor.finished(), compressor::deflate), out);
            compressor.end();
            compressedSample = out.obtain();
        }

        // decompress with pipe
        ByteBuffer decompressedSample;
        try (var pipe = TransformChannel.create((ReadableByteChannel) new ByteBufferReadChannel(compressedSample))) {
            pipe.addTransformer(ByteTransformer.of(new Inflater()));
            decompressedSample = IOUtil.readAll(pipe);
        }

        TestUtils.assertBBufEquals(sample.rewind(), decompressedSample);
    }

    @Test
    public void testCompressionPipe() throws IOException {
        IOTransformerTest.testPipe(5128,
            // compress on outgoing
            List.of(ByteTransformer.of(new Deflater())),
            // decompress on incoming
            List.of(ByteTransformer.of(new Inflater()))
        );
    }

    @Test
    public void testCompressionStreamPipe() throws IOException {
        IOTransformerTest.testPipe(5128,
            List.of(ByteTransformer.of(DeflaterOutputStream::new)),
            List.of(ByteTransformer.of(InflaterOutputStream::new))
        );
    }

}
