package cc.fluse.ulib.test.reflect;

import cc.fluse.ulib.core.reflect.ReflectUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReflectUtilTest {

    // test get caller

    @Test
    public void testGetCaller() {
        assertEquals(ReflectUtilTest.class, AnotherTest.test());
        assertEquals(AnotherTest.class, AnotherTest.test2());
    }

    private static final class AnotherTest {
        private static Class<?> test() {
            return ReflectUtil.getCallerClass();
        }

        private static Class<?> test2() {
            return test();
        }
    }

    @Test
    public void testPrivileged() throws ReflectiveOperationException {
        var i = Integer.valueOf(42);
        var val = ReflectUtil.doPrivileged(() -> ReflectUtil.icall(Integer.class, i, "value"));
        assertEquals(i, val);
    }
}
