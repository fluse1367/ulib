package cc.fluse.ulib.test.database;

import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.database.Database.Protocol;
import cc.fluse.ulib.core.database.RemoteDatabase;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.file.CachedFile;
import cc.fluse.ulib.core.file.FSUtil;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.util.Conditions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.HexFormat;
import java.util.Random;
import java.util.Scanner;

import static cc.fluse.ulib.test.TestUtils.exec;
import static cc.fluse.ulib.test.TestUtils.onlyLinux_x86_64;

/**
 * <b>Note:</b> This test will only work on an x86_64 linux system as it downloads a mysql server archive and starts
 * it.
 */
public class MySQLTest {

    private static Process mysqlServerProcess;
    private static Path dataDir;
    private static final int port = new Random().nextInt(10000) + 10000;

    @BeforeClass
    public static void startMysqlServer() throws IOException, InterruptedException {
        onlyLinux_x86_64();

        // download mysql server archive
        var cf = CachedFile.fromRemote(RemoteResource.of("https://cdn.mysql.com//Downloads/MySQL-8.0/mysql-8.0.33-linux-glibc2.28-x86_64.tar.gz"),
                () -> MessageDigest.getInstance("MD5"), HexFormat.of().parseHex("b3e68a7ca11cce1bac7e71755f3493c4"));
        var path = cf.get(false).join();

        // extract mysql server archive
        var extractTargetDir = CachedFile.getDefaultCacheDir().resolve("archive");
        FSUtil.mkDirs(extractTargetDir);
        exec(null, "tar", "-xzf", path.toAbsolutePath().toString(),
                "-C", extractTargetDir.toAbsolutePath().toString(), "--overwrite", "*/bin/mysqld", "*/lib/*").waitFor();
        var mysqlServerDir = extractTargetDir.resolve("mysql-8.0.33-linux-glibc2.28-x86_64");
        dataDir = mysqlServerDir.resolve("data");

        // initialize mysql server
        if (Files.exists(mysqlServerDir.resolve("data"))) FSUtil.rm(mysqlServerDir.resolve("data"), 0);
        exec(mysqlServerDir, "bin/mysqld", "--no-defaults", "--initialize-insecure").waitFor();

        // start mysql server
        mysqlServerProcess = exec(mysqlServerDir, "bin/mysqld", "--no-defaults",
                "--mysqlx=OFF", "--socket", "mysql.sock", "--bind-address", "127.0.0.1", "--port", String.valueOf(port));

        // wait for mysql server to be ready
        Conditions.waitFor(new Scanner(mysqlServerProcess.getErrorStream())::nextLine,
                line -> line.contains("ready for connections"), line -> false);
    }

    @AfterClass
    public static void stopMysqlServer() throws InterruptedException {
        if (mysqlServerProcess != null && mysqlServerProcess.isAlive()) {
            mysqlServerProcess.destroy();
            mysqlServerProcess.waitFor();
        }
        FSUtil.rm(dataDir, 0);
    }

    private <T extends SqlDatabase & RemoteDatabase> SqlDatabase conn(Protocol<T> proto) throws IOException {
        return Database.connect(proto, 4, "127.0.0.1", port, "mysql", "root", "");
    }

    private <T extends SqlDatabase & RemoteDatabase> void testDB(Protocol<T> protocol) throws IOException, SQLException {
        int insert_id;
        try (var db = conn(protocol)) {
            insert_id = Util.dbTest(db);
        }

        // it's important to close the database connection before we can open it again,
        // so we can ensure, dbTest2 fetches all data from the database, not from the cache
        try (var db = conn(protocol)) {
            Util.dbTest2(db, insert_id);
        }
    }

    @Test
    public void testMySQLClient() throws Exception {
        testDB(Protocol.MySQL);
    }

    @Test
    public void testMariaDBClient() throws Exception {
        testDB(Protocol.MariaDB);
    }

    @Test
    public void testEntityMySQLClient() throws IOException, SQLException, InterruptedException {
        try (var db = conn(Protocol.MySQL)) {
            Util.entityTest(db);
        }
    }

    @Test
    public void testEntityMariaDBClient() throws IOException, SQLException, InterruptedException {
        try (var db = conn(Protocol.MariaDB)) {
            Util.entityTest(db);
        }
    }
}
