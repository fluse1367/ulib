package cc.fluse.ulib.test.database;

import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.file.CachedFile;
import cc.fluse.ulib.core.file.FSUtil;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.util.Conditions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.HexFormat;
import java.util.Random;
import java.util.Scanner;

import static cc.fluse.ulib.test.TestUtils.exec;
import static cc.fluse.ulib.test.TestUtils.onlyLinux_x86_64;

public class PgSQLTest {

    private static Process pgSqlServerProcess;
    private static Path dataDir, socketDir;
    private static final int port = new Random().nextInt(10000) + 10000;

    @BeforeClass
    public static void startPgSqlServer() throws IOException, InterruptedException {
        onlyLinux_x86_64();

        // download pgsql server archive
        var cf = CachedFile.fromRemote(RemoteResource.of("https://gitlab.com/fluse1367/postgres-bin/-/raw/7c2763eb2f0a9d86d533fe4108944f9837ea3559/target/postgresql-15.3-x86_64-linux-bin.tar.gz"),
                () -> MessageDigest.getInstance("MD5"), HexFormat.of().parseHex("33f06aeb7e8a5a1c8221ae1dcf306898"));
        var path = cf.get(false).join();

        // extract pgsql server archive
        var pgServerDir = CachedFile.getDefaultCacheDir().resolve("archive").resolve(cf.getLocation().getFileName());
        FSUtil.mkDirs(pgServerDir);
        exec(null, "tar", "-xzf", path.toAbsolutePath().toString(),
                "-C", pgServerDir.toAbsolutePath().toString(), "--overwrite", "bin/*", "lib/*", "share/*").waitFor();

        // initialize pgsql server
        dataDir = pgServerDir.resolve("data").toAbsolutePath();
        if (Files.exists(dataDir)) FSUtil.rm(dataDir, 0);
        exec(pgServerDir, "bash", "-c", "bin/initdb -D data -U root --pwfile <(echo root) --auth-host password --auth-local trust").waitFor();

        // start pgsql server
        socketDir = Files.createTempDirectory(null).toAbsolutePath();
        pgSqlServerProcess = exec(pgServerDir, "bin/postgres", "-D", dataDir.toString(), "-k", socketDir.toString(),
                "-h", "127.0.0.1", "-p", String.valueOf(port));

        // wait for pgsql server to be ready
        Conditions.waitFor(new Scanner(pgSqlServerProcess.getErrorStream())::nextLine,
                s -> s.contains("database system is ready to accept connections"), s -> false);

        // create test database
        exec(pgServerDir, "bin/createdb", "-h", socketDir.toString(), "-p", String.valueOf(port), "-U", "root", "test").waitFor();
    }

    @AfterClass
    public static void stopPgSqlServer() throws InterruptedException {
        if (pgSqlServerProcess != null && pgSqlServerProcess.isAlive()) {
            pgSqlServerProcess.destroy();
            pgSqlServerProcess.waitFor();
        }
        FSUtil.rm(dataDir, 0);
        FSUtil.rm(socketDir, 0);
    }

    private SqlDatabase conn() throws IOException {
        return Database.connect(Database.Protocol.PostgreSQL, 4, "127.0.0.1", port, "test", "root", "root");
    }

    @Test
    public void testDB() throws Exception {
        int insert_id;
        try (var db = conn()) {
            insert_id = Util.dbTest(db);
        }

        // it's important to close the database connection before we can open it again,
        // so we can ensure, dbTest2 fetches all data from the database, not from the cache
        try (var db = conn()) {
            Util.dbTest2(db, insert_id);
        }

    }

    @Test
    public void testEntity() throws IOException, SQLException, InterruptedException {
        try (var db = conn()) {
            Util.entityTest(db);
        }
    }
}
