package cc.fluse.ulib.minecraft.impl.launchermeta;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.minecraft.launchermeta.RemoteMojangArtifact;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;

final class Artifact extends MojangResource implements RemoteMojangArtifact {
    private final Path path;

    Artifact(String id, String path, JsonConfiguration json) {
        super(id, json);
        this.path = Paths.get(path);
    }

    @Override
    public @NotNull Path getPath() {
        return path;
    }
}
