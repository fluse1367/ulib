package cc.fluse.ulib.minecraft.impl.launchermeta;

import cc.fluse.ulib.core.common.Keyable;
import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.minecraft.launchermeta.RemoteLibrary;
import cc.fluse.ulib.minecraft.launchermeta.RemoteMojangArtifact;
import org.jetbrains.annotations.NotNull;

import java.util.*;

final class Library implements RemoteLibrary {
    private final String mavenCoords;
    private final Map<String, RemoteMojangArtifact> downloads;

    Library(String mavenCoords, JsonConfiguration json /*downloads*/) {
        this.mavenCoords = mavenCoords;

        Map<String, Artifact> downloads = new HashMap<>();
        var arti = json.getSubsection(KeyPath.of("artifact")).orElseThrow();
        downloads.put("artifact", new Artifact("artifact", arti.string("path").orElseThrow(), arti));

        json.getSubsection(KeyPath.of("classifiers"))
            .map(s -> s.getSubsections(false))
            .ifPresent(c -> c.forEach(sub -> {
                @SuppressWarnings("unchecked")
                String id = ((Keyable<String>) sub).getKey();
                downloads.put(id, new Artifact(id, sub.string("path").orElseThrow(), sub));
            }));

        this.downloads = Collections.unmodifiableMap(downloads);
    }

    @Override
    public @NotNull Map<String, RemoteMojangArtifact> getDownloads() {
        return downloads;
    }

    @Override
    public @NotNull String getName() {
        return mavenCoords;
    }
}
