package cc.fluse.ulib.minecraft.mappings;

/**
 * Representation of a mapping for a minecraft vanilla jar (source -> vanilla jar).
 */
public interface VanillaMapping extends JarMapping {
}
