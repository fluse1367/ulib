package cc.fluse.ulib.minecraft.proxybridge.message;

public enum MessageType {
    REQUEST,
    COMMAND,
    ANSWER
}
