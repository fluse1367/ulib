package cc.fluse.ulib.minecraft.impl.mappings;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.file.*;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.impl.Internal;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;

import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

@Getter
final class BuildDataMeta {
    public static String SPIGOTMC_REST = "https://hub.spigotmc.org/stash/rest/api/1.0/projects/SPIGOT/repos/builddata";

    private final String mcVersion;
    private final String hash;

    private final String cm;
    private final FilesystemResource classMappings;
    private final String mm;
    private final FilesystemResource memberMappings;

    private BuildDataMeta(String ver, String hash, String cm, String mm) {
        this.mcVersion = ver;
        this.hash = hash;

        String mappingsUrl = SPIGOTMC_REST + "/raw/mappings/";

        this.cm = cm;
        this.mm = mm;

        this.classMappings = CachedFile.fromRemote(RemoteResource.of(mappingsUrl + this.cm + "?at=" + hash),
                                                   CachedFile.DEFAULT_DIGEST_PROVIDER, null);
        this.memberMappings = this.mm == null ? null : CachedFile.fromRemote(
                RemoteResource.of(mappingsUrl + this.mm + "?at=" + hash), CachedFile.DEFAULT_DIGEST_PROVIDER, null);
    }

    private static BuildDataMeta fromJson(JsonConfiguration json, String ver, String hash) {
        String cm = json.string("classMappings").orElseThrow();
        String mm = json.string("memberMappings").orElse(null);

        return new BuildDataMeta(ver, hash, cm, mm);
    }

    private static BuildDataMeta fromJson(JsonConfiguration json, String ver) {
        return fromJson(json, ver, json.string("hash").orElseThrow());
    }

    @SneakyThrows
    @Nullable
    private static BuildDataMeta fromCommit(String commitHash) {
        String url = SPIGOTMC_REST + "/raw/info.json?at=" + commitHash;


        JsonConfiguration json;
        try (var in = URI.create(url).toURL().openStream()) {
            json = JsonConfiguration.loadJson(in);
        }

        // ensure json is valid
        if (!json.isSet(KeyPath.of("minecraftVersion"))) {
            return null;
        }

        return fromJson(json, json.string("minecraftVersion").orElseThrow(), commitHash);
    }

    @SneakyThrows
    static BuildDataMeta loadBuildData(String ver) {

        String verKey = ver.replace(".", "_");

        // load cached meta
        JsonConfiguration cachedMeta;
        Path metaFile = Internal.getCacheDir().resolve("bukkitbuilddata/versions.json");
        if (Files.exists(metaFile)) {
            cachedMeta = JsonConfiguration.loadJson(metaFile);
        } else {
            cachedMeta = JsonConfiguration.newJson();
        }

        // load from versions.json
        if (cachedMeta.isSubsection(KeyPath.of(verKey))) {
            return fromJson(cachedMeta.getSubsection(KeyPath.of(verKey)).orElseThrow(), ver);
        }

        // download json data
        BuildDataMeta buildData;

        String url = String.format("https://hub.spigotmc.org/versions/%s.json", ver);
        try (var reader = new InputStreamReader(
                CachedFile.fromRemote(RemoteResource.of(url), CachedFile.DEFAULT_DIGEST_PROVIDER, null).streamRead())) {
            var json = JsonConfiguration.loadJson(reader);
            String commit = json.string("refs.BuildData").orElseThrow();

            buildData = fromCommit(commit);
            if (buildData == null) {
                return null;
            }

            cachedMeta.set(verKey, buildData.toJson());
        }

        // save meta.json
        FSUtil.mkParent(metaFile);
        cachedMeta.dumpTo(metaFile);
        return buildData;
    }

    public JsonConfiguration toJson() {
        var json = JsonConfiguration.newJson();
        json.set("hash", hash);
        json.set("classMappings", cm);
        json.set("memberMappings", mm);
        return json;
    }

    @Override
    public String toString() {
        return "BuildDataMeta{" +
               "mcVersion='" + mcVersion + '\'' +
               ", hash='" + hash + '\'' +
               ", cm='" + cm + '\'' +
               ", mm='" + mm + '\'' +
               '}';
    }
}
