/**
 * Controllers, that are able to control certain parts of a plugin.
 */
package cc.fluse.ulib.minecraft.plugin.controllers;