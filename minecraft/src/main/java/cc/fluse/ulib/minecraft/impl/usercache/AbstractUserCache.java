package cc.fluse.ulib.minecraft.impl.usercache;

import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.database.sql.ColumnBuilder;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.LazyValue;
import cc.fluse.ulib.core.util.SingletonInstance;
import cc.fluse.ulib.minecraft.impl.SharedConstants;
import cc.fluse.ulib.minecraft.plugin.PluginBase;
import cc.fluse.ulib.minecraft.usercache.UserCache;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

public abstract class AbstractUserCache implements UserCache {

    public static final SingletonInstance<BiFunction<PluginBase<?, ?>, Table, AbstractUserCache>> PROVIDER = new SingletonInstance<>();
    public static final LazyValue<UserCache> MAIN_CACHE = LazyValue.immutable(AbstractUserCache::getMainCache);
    public static final SingletonInstance<SqlDatabase> MAIN_CACHE_DB = new SingletonInstance<>();

    private static UserCache getMainCache() {
        var plugin = SharedConstants.BASE.get();

        // init database connection
        var database = (SqlDatabase) Database.prepare(plugin.getConf().getSubsection(KeyPath.of("user-cache-backend")).orElseThrow());

        var table = database.addTable("cached_users",
                ColumnBuilder.of(DataType.VARCHAR, "uuid").size(36).primary(),
                ColumnBuilder.of(DataType.VARCHAR, "name").size(16).notNull()
        );


        try {
            database.connect();
        } catch (IOException e) {
            throw new RuntimeException("Failed to connect to database", e);
        }

        if (!table.exists()) {
            Expect.compute(table::create).rethrowRE();
        }

        MAIN_CACHE_DB.setInstance(database);

        return PROVIDER.get().apply(plugin, table);
    }

    private final Table table;
    protected final Map<UUID, String> cache = new ConcurrentHashMap<>();

    protected AbstractUserCache(Table table) {
        this.table = table;
    }

    @SneakyThrows
    public void cache(@NotNull UUID uuid, @NotNull String name) {
        if (getUsername(uuid).isEmpty()) {
            table.insert(uuid.toString(), name);
        } else {
            table.update().setP("name", name).where("uuid").isEqualToP(uuid.toString());
        }
        cache.put(uuid, name);
    }

    @SneakyThrows
    public void purge(@NotNull UUID uuid) {
        if (getUsername(uuid).isEmpty())
            return;
        table.delete().where("uuid").isEqualToP(uuid.toString()).update();
        cache.remove(uuid);
    }

    public void purge(@NotNull String username) {
        getUUID(username).ifPresent(this::purge);
    }

    @SneakyThrows
    public @NotNull Optional<String> getUsername(@NotNull UUID uuid) {
        if (!cache.containsKey(uuid)) {
            var rs = table.select("name").where("uuid").isEqualToP(uuid.toString()).query();
            if (rs.next()) {
                cache.put(uuid, rs.getString("name"));
            }
        }
        return Optional.ofNullable(cache.get(uuid));
    }

    @SneakyThrows
    public @NotNull Optional<UUID> getUUID(@NotNull String username) {
        if (!cache.containsValue(username)) {
            var rs = table.select("uuid").where("name").isEqualToP(username).query();
            if (rs.next()) {
                cache.put(UUID.fromString(rs.getString("uuid")), username);
            }
        }
        for (Map.Entry<UUID, String> entry : cache.entrySet()) {
            if (entry.getValue().equals(username))
                return Optional.ofNullable(entry.getKey());
        }
        return Optional.empty();
    }
}
