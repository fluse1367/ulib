package cc.fluse.ulib.minecraft.impl.mappings;

import cc.fluse.ulib.core.file.CachedFile;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.Concurrent;
import cc.fluse.ulib.core.impl.UnsafeOperations;
import cc.fluse.ulib.core.util.LazyValue;
import cc.fluse.ulib.minecraft.impl.SharedConstants;
import cc.fluse.ulib.minecraft.launchermeta.*;
import cc.fluse.ulib.minecraft.util.Protocol;
import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.util.HexFormat;
import java.util.Optional;


public final class MappingsImpl {
    private static final LazyValue<VanillaMapping> currentVanilla = LazyValue.immutable(() -> {

        var ver = SharedConstants.MC_VER.get();
        var manifest = VersionsMeta.getCurrent().get(ver)
                                   .orElseThrow(() -> new IllegalStateException(String.format("launchermeta.mojang.com: Unknown Server Version (%s)", ver)));
        return loadVanilla(manifest);
    });
    private static final LazyValue<BukkitMapping> currentBukkit = LazyValue.immutable(() ->
            loadBukkit(SharedConstants.MC_VER.get()));
    private static final LazyValue<MixedMapping> currentMixed = LazyValue.immutable(() -> {
        var ver = SharedConstants.MC_VER.get();
        var manifest = VersionsMeta.getCurrent().get(ver)
                .orElseThrow(() -> new IllegalStateException(String.format("launchermeta.mojang.com: Unknown Server Version (%s)", ver)));
        return loadMixed(manifest);
    });

    @SneakyThrows
    public static VanillaMapping loadVanilla(VersionManifest version) {
        if (version == null) {
            return null;
        }
        return version.getDownload("server_mappings").map((ParamFunc<RemoteMojangResource, VanillaMapping, ?>) res -> {
            var file = CachedFile.fromRemote(res, () -> MessageDigest.getInstance("SHA-1"),
                                             res.getSha1().map(HexFormat.of().withLowerCase()::parseHex).orElse(null));

            var out = new ByteArrayOutputStream();
            try (var in = file.streamRead()) {
                in.transferTo(out);
            }
            return new VanillaMapping(out.toString());
        }).orElse(null);
    }

    public static VanillaMapping getCurrentVanilla() {
        return currentVanilla.get();
    }

    @SneakyThrows
    public static BukkitMapping loadBukkit(String version) {
        var protocol = Protocol.of(version);
        if (!UnsafeOperations.comply(protocol.isDummy(), "Bukkit Mappings loading",
                String.format("version '%s' unknown", version),
                String.format("Ignoring version '%s' being unknown", version)) && protocol.below(Protocol.v1_8_R1)) {
            return null; // no bukkit mappings before 1.8
        }

        var data = BuildDataMeta.loadBuildData(version);
        if (data == null)
            return null;

        var res = Concurrent.await(() -> {
            try (var in = data.getClassMappings().streamRead(); var clOut = new ByteArrayOutputStream()) {
                in.transferTo(clOut);
                return clOut;
            }
        }, () -> {
            var mm = data.getMemberMappings();
            if (mm == null) {
                return null;
            }
            try (var in = mm.streamRead(); var memOut = new ByteArrayOutputStream()) {
                in.transferTo(memOut);
                return memOut;
            }
        });

        return new BukkitMapping(res.get(0).toString(),
                Optional.ofNullable(res.get(1))
                        .map(ByteArrayOutputStream::toString)
                        .orElse(null),
                protocol);
    }

    public static BukkitMapping getCurrentBukkit() {
        return currentBukkit.get();
    }

    @SneakyThrows
    public static MixedMapping loadMixed(VersionManifest version) {
        var res = Concurrent.await(
                () -> loadVanilla(version),
                () -> loadBukkit(version.getId())
        );

        var vm = res.get(0);
        var bm = res.get(1);
        if (!(vm instanceof VanillaMapping) || !(bm instanceof BukkitMapping))
            return null;
        return new MixedMapping((BukkitMapping) bm, (VanillaMapping) vm);
    }

    public static MixedMapping getCurrentMixed() {
        return currentMixed.get();
    }
}
