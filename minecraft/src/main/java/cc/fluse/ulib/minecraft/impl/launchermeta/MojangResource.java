package cc.fluse.ulib.minecraft.impl.launchermeta;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.minecraft.launchermeta.RemoteMojangResource;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Getter
class MojangResource implements RemoteMojangResource {
    @NotNull
    private final String id;
    private final long size;
    @Getter(AccessLevel.NONE)
    private final String urlStr;
    private final String sha1;
    private final URL remoteLocation;

    MojangResource(String id, JsonConfiguration json) {
        this(
                id,
                json.string("sha1").orElseThrow(),
                json.int64("size").orElseThrow(),
                json.string("url").orElseThrow()
        );
    }

    @SneakyThrows
    MojangResource(@NotNull String id, String sha1, long size, String url) {
        this.id = id;
        this.size = size;
        this.urlStr = url;
        this.sha1 = sha1;
        this.remoteLocation = new URL(url);
    }

    public @NotNull Optional<String> getSha1() {
        return Optional.ofNullable(sha1);
    }

    @Override
    public @NotNull InputStream streamRead() {
        return Expect.compute(remoteLocation::openStream).orElseThrow();
    }

    @Override
    public @NotNull <R> CompletableFuture<R> streamRead(@NotNull ParamFunc<? super @NotNull InputStream, R, ?> reader) {
        return CompletableFuture.supplyAsync(this::streamRead).thenApplyAsync(reader);
    }

    @Override
    public @NotNull ReadableByteChannel channel() throws IOException {
        return Channels.newChannel(streamRead());
    }
}
