module ulib.minecraft {
    // static
    requires static lombok;
    requires static org.jetbrains.annotations;

    // java
    requires java.sql;

    // 3rd party

    // ulib
    requires transitive ulib.core;
    requires java.net.http;

    // api exports
    exports cc.fluse.ulib.minecraft.io;
    exports cc.fluse.ulib.minecraft.launchermeta;
    exports cc.fluse.ulib.minecraft.mappings;
    exports cc.fluse.ulib.minecraft.plugin.controllers;
    exports cc.fluse.ulib.minecraft.plugin;
    exports cc.fluse.ulib.minecraft.proxybridge.command;
    exports cc.fluse.ulib.minecraft.proxybridge.message;
    exports cc.fluse.ulib.minecraft.proxybridge;
    exports cc.fluse.ulib.minecraft.usercache;
    exports cc.fluse.ulib.minecraft.util;

    // impl exports
    exports cc.fluse.ulib.minecraft.impl to ulib.velocity, ulib.bungeecord, ulib.spigot;
    exports cc.fluse.ulib.minecraft.impl.proxybridge to ulib.velocity, ulib.bungeecord, ulib.spigot;
    exports cc.fluse.ulib.minecraft.impl.usercache to ulib.velocity, ulib.bungeecord, ulib.spigot;
    exports cc.fluse.ulib.minecraft.impl.launchermeta to ulib.spigot;
}