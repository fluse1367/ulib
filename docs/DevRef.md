[<- Back to Overview](Readme.md)

# uLib Developer Reference

---

## Contents

1. [Build from Source](#build-from-source)
2. [Maven Repository](#maven-repository)
3. [Loading/Initializing uLib](#loading-ulib-into-the-runtime)
4. [The Javaagent](#about-the-javaagent)
5. [Unsafe Operations](#unsafe-operations)
6. [Troubleshooting](#troubleshooting)

---

## Build from Source

Building ulib from source is super easy. Clone the repository and build it with gradle.

Note: You need to have the JDK 17 installed.

1. **Clone this repository**
   ```shell
   git clone https://gitlab.com/fluse1367/ulib.git
   ```
2. **`cd` into the directory**
   ```shell
   cd ulib
   ```
3. <details><summary><b>Switch to another branch</b> (Optional)</summary>

   ```shell
   git checkout BRANCH_NAME
   ```
   </details>


4. **Build it**

   Linux (bash):

   ```shell
   ./gradlew build
   ```

   Windows (cmd):

   ```shell
   ./gradlew.bat build
   ```

   You will find the loader in `loader/build/libs/`.

---

## Maven Repository

![Maven metadata URL](https://img.shields.io/maven-metadata/v?label=snapshot&metadataUrl=https%3A%2F%2Frepo.fluse.cc%2Fcc%2Ffluse%2Fulib-snapshot%2Fulib-loader%2Fmaven-metadata.xml) ![Maven metadata URL](https://img.shields.io/maven-metadata/v?label=release&metadataUrl=https%3A%2F%2Frepo.fluse.cc%2Fcc%2Ffluse%2Fulib%2Fulib-loader%2Fmaven-metadata.xml)

Make sure you include only the `loader` as runtime library.

<details><summary>Gradle</summary>

```groovy
repositories {
    /* ... */
    maven {
        url 'https://repo.fluse.cc/'
        // or url 'https://gitlab.com/api/v4/projects/40820639/packages/maven/'
    }
    /* ... */
}
dependencies {
    /* ... */

    // for snapshots use group id 'cc.fluse.ulib-snapshot'
    implementation 'cc.fluse.ulib:ulib-loader:VERSION'
    compileOnly 'cc.fluse.ulib:ulib-core:VERSION'
    compileOnly 'cc.fluse.ulib:ulib-spigot:VERSION'
    compileOnly 'cc.fluse.ulib:ulib-bungeecord:VERSION'
    compileOnly 'cc.fluse.ulib:ulib-velocity:VERSION'

    /* ... */
}
```

</details>
<details><summary>Maven</summary>

```xml

<project>
    <!-- ... -->
    <repositories>
        <!-- ... -->
        <repository>
            <id>fluse1367-repo</id>
            <url>https://repo.fluse.cc/</url>
            <!-- or <url>https://gitlab.com/api/v4/projects/40820639/packages/maven/</url> -->
        </repository>
        <!-- ... -->
    </repositories>
    <dependencies>
        <!-- ... -->

        <!-- for snapshots use group id 'cc.fluse.ulib-snapshot' -->
        <dependency>
            <groupId>cc.fluse.ulib</groupId>
            <artifactId>ulib-loader</artifactId>
            <version>VERSION</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>cc.fluse.ulib</groupId>
            <artifactId>ulib-core</artifactId>
            <version>VERSION</version>
        </dependency>

        <dependency>
            <groupId>cc.fluse.ulib</groupId>
            <artifactId>ulib-spigot</artifactId>
            <version>VERSION</version>
        </dependency>

        <dependency>
            <groupId>cc.fluse.ulib</groupId>
            <artifactId>ulib-bungeecord</artifactId>
            <version>VERSION</version>
        </dependency>

        <dependency>
            <groupId>cc.fluse.ulib</groupId>
            <artifactId>ulib-velocity</artifactId>
            <version>VERSION</version>
        </dependency>
        <!-- ... -->
    </dependencies>
    <!-- ... -->
</project>
```

</details>

---

## Loading uLib into the Runtime

Before you do anything with uLib, get sure you load the library with its loader.

When using the one of the Plugins implementations, you don't have to take care of loading it; It's enough to put the
loader in the respective `plugins` folder, but don't forget to declare uLib as dependency!

When using the standalone implementation, you have to load the library class by yourself. There are several ways how to
do this.

If you put uLib into your classpath, you can use the `Installer` class from the loader and load uLib into your current
class loader. You may also activate additional modules (optional). However, this needs to be done before the installer
initializes anything. Otherwise, it will have no effect.

```
// activate additional modues (optional)
Installer.activateModules(Module.MINECRAFT);

// install!
Installer.installTo(getClass().getClassLoader());
//or
Installer.installMe();
```

Also, if you are installing it into a modular context, you may have to add a `reads` record to that module before using
uLib. Otherwise, your module won't have access to the uLib API. When using `Installer.installMe()` this is automatically
done for you.

This example shows how it can be done easily:

```
getClass().getModule().addReads(Installer.getLayer().findModule("ulib.core").get());
```

Make sure installing uLib properly into the runtime **before** you do _anything_ with uLib (this includes loading one of
ulib's classes!).

### Alternatives

Another way is to use the launch function. For this, run the loader directly. Supply either the
argument `--launch /path/to/application.jar` (the loader will look up the main class in the manifest file)
or `--main path.to.MainClass` (here the jar file with this class have to be already in the classpath).

With both options you can also specify arguments that should be passed to the main class. Anything after "--" will be
passed to the main class as arguments (similarly how most GNU tools work):

`-- arg0 arg1 arg2`

All in all, your command could look like this:

```shell
java -jar ulib-loader-VERSION.jar --launch my-application.jar -- --demo "John Doe"
```

or this:

```shell
java -cp ulib-loader-VERSION.jar:my-application.jar cc.fluse.ulib.loader.launch.Main --main my.application.Main -- --demo "John Doe"
```

## About the Javaagent

ULib realizes several things utilizing a so-called Javaagent. This agent is **crucial** for the library to run.  
In fact, the loader even depends on it, to properly load it. **Without this agent, uLib will fail in every extend.**

By default, the loader uses a workaround method to self-initialize the Javaagent, however this should be avoided it
possible.

The best solution is to supply the loader as javaagent to the JVM (with an additional flag):

```shell
java -javaagent:path/to/ulib-loader.jar ...
```

If the solution above does not work for you, another thing you can try is to allow the application to self-attach a
javaagent (again, with an additional flag):

```shell
java -Djdk.attach.allowAttachSelf=true ... 
```

## Unsafe Operations

While developing with ulib, you might come across a so-called "unsafe operation". These are functions of the library
that are considered (potentially) unsafe; **Untrusted code cannot access these operations by default**. Trusted code can
however access these, as it is assumed that the unsafe operations are used in a safe manner. You can also allow access
to unsafe operations by _untrusted code_ within [ulib's config](../core/src/main/resources/META-INF/coreconfig.yml).

At the time of writing this, the unsafe operations are
[privileged reflection access](../core/src/main/java/cc/fluse/ulib/core/reflect/ReflectUtil.java)
and [code injection](../core/src/main/java/cc/fluse/ulib/core/inject/HookInjection.java).

Jar files signed by a valid certificate chain (anchored by
the [ulib CA root certificate](../core/src/main/resources/META-INF/root-ca.cer)) are considered trusted code.

## Troubleshooting

Because ulib uses complex mechanics to inject itself into your desired class loader context, it is fairly easy for it to
fail. Analyzing and understanding what went wrong can be pretty tough. Common malfunctions and possible fixes listed are
listed below.

- ```
  Module ulib.core not found, required by mymodule
  ```
  Because uLib is loaded by the installer **after** the initialization of the boot layer, the uLib API module is not
  available at the time of initialization. Change the `requires ulib.core;` record in your module info file
  to `requires static`.
- ```
  class myclass (in module mymodule) cannot access class ulibclass (in module ulib.core) ...
  ```
  Because the `reads` record in your module info file is declared as static, you must add a `reads` record to your
  module manually before you can access the uLib API: `getClass().getModule().addReads(Installer.getModule());`

Please also refer to the [user troubleshooting guide](Troubleshooting.md).

---