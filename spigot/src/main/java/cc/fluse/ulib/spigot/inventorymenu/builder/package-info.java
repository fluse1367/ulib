/**
 * Contains the builders for creating {@link cc.fluse.ulib.spigot.inventorymenu.entry.Entry} and
 * {@link cc.fluse.ulib.spigot.inventorymenu.menu.Menu} instances.
 * <p>
 * Entry builders:
 * <ul>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.EntryBuilder}</li>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.ToggleableEntryBuilder}</li>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.MultiStateEntryBuilder}</li>
 * </ul>
 * Menu builders:
 * <ul>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.MenuBuilder}</li>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.MultiPageMenuBuilder}</li>
 *     <li>{@link cc.fluse.ulib.spigot.inventorymenu.builder.PageBuilder}</li>
 * </ul>
 */
package cc.fluse.ulib.spigot.inventorymenu.builder;