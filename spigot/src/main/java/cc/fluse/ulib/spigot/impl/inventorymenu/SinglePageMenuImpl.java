package cc.fluse.ulib.spigot.impl.inventorymenu;

import cc.fluse.ulib.spigot.inventorymenu.entry.Entry;
import cc.fluse.ulib.spigot.inventorymenu.menu.SinglePageMenu;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.function.Consumer;

public class SinglePageMenuImpl extends PageImpl implements SinglePageMenu {
    public SinglePageMenuImpl(String title, int rows, Map<Integer, Entry> entries, Consumer<Player> openHandler, Consumer<Player> closeHandler) {
        super(title, rows, entries, openHandler, closeHandler);
    }
}
