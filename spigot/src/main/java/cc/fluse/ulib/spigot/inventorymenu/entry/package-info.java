/**
 * The different entries a {@link cc.fluse.ulib.spigot.inventorymenu.menu.Page} or
 * {@link cc.fluse.ulib.spigot.inventorymenu.menu.SinglePageMenu} can have.
 */
package cc.fluse.ulib.spigot.inventorymenu.entry;