/**
 * {@link java.util.function.Consumer}-like functional interfaces, made for the InventoryMenu API.
 */
package cc.fluse.ulib.spigot.inventorymenu.handlers;