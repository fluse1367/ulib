/**
 * An API for easy and fast Menu/GUI creation and management using classic chest-like inventories ("InventoryMenu").
 */
package cc.fluse.ulib.spigot.inventorymenu;