/**
 * Classes regarding extended Spigot plugin functionality.
 *
 * @see cc.fluse.ulib.minecraft.plugin
 */
package cc.fluse.ulib.spigot.plugin;