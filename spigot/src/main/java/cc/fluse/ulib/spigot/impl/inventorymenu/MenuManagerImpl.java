package cc.fluse.ulib.spigot.impl.inventorymenu;

import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.spigot.impl.DelegationListener;
import cc.fluse.ulib.spigot.inventorymenu.MenuManager;
import cc.fluse.ulib.spigot.inventorymenu.menu.*;
import lombok.Synchronized;
import org.bukkit.event.*;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Stream;

public final class MenuManagerImpl implements MenuManager {

    private final Plugin plugin;
    private final MenuManagerListener handler;
    private final List<Menu> menus = new LinkedList<>();

    private Listener dummyListener;
    private boolean listening = false;

    public MenuManagerImpl(Plugin plugin) {
        this.plugin = plugin;
        this.handler = new MenuManagerListener(this);
    }

    private void registerHandles() {
        DelegationListener d = new DelegationListener();
        DelegationListener.registerDelegation(d, InventoryClickEvent.class, handler::handle,
                EventPriority.NORMAL, false, plugin);
        DelegationListener.registerDelegation(d, InventoryOpenEvent.class, handler::handle,
                EventPriority.NORMAL, false, plugin);
        DelegationListener.registerDelegation(d, InventoryCloseEvent.class, handler::handle,
                EventPriority.NORMAL, false, plugin);
        this.dummyListener = d;
    }

    private void reset() {
        handler.getNoTriggerOpenClose().clear();
    }

    @Override
    @Synchronized
    public void listen() {
        if (listening)
            return;
        registerHandles();
        reset();
        listening = true;
    }

    @Override
    @Synchronized
    public void stopListening() {
        if (!listening)
            return;
        HandlerList.unregisterAll(dummyListener);
        reset();
        listening = false;
    }

    @Override
    public void registerMenu(@NotNull Menu menu) {
        menus.add(menu);
    }

    @Override
    public void unregisterMenu(@NotNull Menu menu) {
        menus.remove(menu);
    }

    @Override
    public @NotNull Optional<Page> getPage(@NotNull Inventory inventory) {
        return menus.stream()
                    .flatMap(menu -> {
                        if (menu instanceof Page page) return Stream.of(page);
                        if (!(menu instanceof MultiPageMenu mm)) return Stream.empty();
                        return mm.getPages().values().stream();
                    })
                    .filter(page -> page.getInventory() == inventory)
                    .findFirst();
    }

    @Override
    public @NotNull Optional<Menu> getMenu(@NotNull Page page) {
        if (page instanceof SinglePageMenu) return Optional.of((SinglePageMenu) page);

        return menus.stream()
                    .filter(MultiPageMenu.class::isInstance)
                    .map(MultiPageMenu.class::cast)
                    .filter(mm -> mm.getPages().values().stream().anyMatch(p -> p == page))
                    .map(Menu.class::cast)
                    .findFirst();
    }

    @Override
    public @NotNull Optional<Menu> getMenu(@NotNull Inventory inventory) {
        return getPage(inventory).flatMap(this::getMenu);
    }

    @Override
    public @NotNull Optional<Pair<MultiPageMenu, Integer>> tryPage(@NotNull Page page) {
        return menus.stream()
                    .filter(MultiPageMenu.class::isInstance)
                    .map(MultiPageMenu.class::cast)
                    .flatMap(menu ->
                                     menu.getPages().entrySet().stream()
                                         .filter(e -> e.getValue().equals(page))
                                         .findFirst()
                                         .map(Map.Entry::getKey)
                                         .map(i -> Tuple.of(menu, i))
                                         .stream()
                    )
                    .findFirst();
    }
}
