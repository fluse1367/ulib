package cc.fluse.ulib.loader.impl.install;

import lombok.Getter;
import lombok.SneakyThrows;

import java.io.File;
import java.lang.module.*;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ModuleLoader extends ClassLoader {

    static ModuleLayer.Controller loadLayer(ModuleLoader providerParent, Collection<File> files, ClassLoader loaderParent, List<ModuleLayer> parentLayers, boolean comply) {
        var finder = makeFinder(files, parentLayers, comply);
        var emptyFinder = ModuleFinder.of();
        var roots = finder.findAll().stream()
                .map(ModuleReference::descriptor)
                .map(ModuleDescriptor::name)
                .toList();


        var parents = parentLayers.stream().map(ModuleLayer::configuration).toList();

        var conf = Configuration.resolve(finder, parents, emptyFinder, roots);

        return ModuleLayer.defineModulesWithOneLoader(conf, parentLayers, loaderParent);
    }

    private static ModuleFinder makeFinder(Collection<File> files, List<ModuleLayer> parentLayers, boolean comply) {
        List<Path> paths = new ArrayList<>(files.stream().map(File::toPath).toList());

        ModuleFinder finder;
        do {
            finder = ModuleFinder.of(paths.toArray(Path[]::new));
        } while (comply && (paths = checkComply(finder, parentLayers)) != null);

        return finder;
    }

    private static List<Path> checkComply(ModuleFinder finder, List<ModuleLayer> parentLayers) {
        AtomicBoolean foundInvalid = new AtomicBoolean(false);
        List<Path> leftOver = new LinkedList<>();

        finder.findAll().forEach(ref -> {
            boolean invalid = parentLayers.stream().anyMatch(layer -> layer.findModule(ref.descriptor().name()).isPresent());
            if (!invalid) {
                leftOver.add(Path.of(ref.location().orElseThrow()));
                return;
            }
            foundInvalid.set(true);
        });

        return foundInvalid.get() ? leftOver : null;
    }

    private final Initializer initializer;
    private final ModuleLoader providerParent;
    @Getter
    private final ModuleLayer.Controller controller;
    @Getter
    private final ModuleLayer layer;

    public ModuleLoader(Initializer initializer, ModuleLoader providerParent, Collection<File> files, ClassLoader loaderParent, ModuleLayer parentLayer) {
        this(initializer, providerParent, files, loaderParent, parentLayer, false);
    }

    public ModuleLoader(Initializer initializer, ModuleLoader providerParent, Collection<File> files, ClassLoader loaderParent, ModuleLayer parentLayer, boolean comply) {
        this(initializer, providerParent, files, loaderParent, List.of(parentLayer), comply);
    }

    public ModuleLoader(Initializer initializer, ModuleLoader providerParent, Collection<File> files, ClassLoader loaderParent, List<ModuleLayer> parentLayers, boolean comply) {
        this.initializer = initializer;
        this.providerParent = providerParent;
        this.controller = loadLayer(providerParent, files, loaderParent, parentLayers, comply);
        this.layer = this.controller.layer();
    }


    static final int FLAG_DENY_IMPL = 0x01;
    static final int FLAG_THROW = 0x02;

    Optional<Module> findApiModule(final String ulibFQCN, final int flags) {
        final boolean denyNonExported = (flags & 0x01) != 0,
                thr = (flags & 0x02) != 0;
        var packageName = ulibFQCN.substring(0, ulibFQCN.lastIndexOf('.'));

        var modules = initializer.getApiModules().stream()
                .filter(m -> m.getPackages().contains(packageName))
                .toList();

        if (modules.size() != 1) {
            if (thr)
                throw new IllegalStateException("Found more than one module with the same package");
            return Optional.empty();
        }

        var module = modules.get(0);
        return !denyNonExported || module.isExported(packageName) ? Optional.of(module) : Optional.empty();
    }

    private ClassLoader findApiClassLoader(String ulibFQCN) throws NoSuchElementException {
        return findApiModule(ulibFQCN, FLAG_DENY_IMPL | FLAG_THROW).orElseThrow().getClassLoader();
    }

    @Override
    @SneakyThrows
    public Class<?> loadClass(String name, boolean resolve) {
        try {
            return Class.forName(name, resolve, findApiClassLoader(name));
        } catch (NoSuchElementException e) {
            if (providerParent != null)
                return providerParent.loadClass(name, resolve);
        }
        return null;
    }

    @Override
    @SneakyThrows
    public Class<?> findClass(String name) {
        try {
            return findApiClassLoader(name).loadClass(name);
        } catch (NoSuchElementException e) {
            if (providerParent != null)
                return providerParent.findClass(name);
        }
        return null;
    }

    @Override
    @SneakyThrows
    public Class<?> findClass(String module, String name) {
        try {
            return layer.findLoader(module).loadClass(name);
        } catch (NoSuchElementException e) {
            if (providerParent != null)
                return providerParent.findClass(module, name);
        }
        return null;
    }
}
