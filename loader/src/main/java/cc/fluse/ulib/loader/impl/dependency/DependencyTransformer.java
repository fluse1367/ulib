package cc.fluse.ulib.loader.impl.dependency;

import cc.fluse.ulib.loader.impl.Logging;
import cc.fluse.ulib.loader.impl.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.jar.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Note: {@code shortCoords} means the coords without version.
 */
public final class DependencyTransformer {

    @FunctionalInterface
    interface Consumer<T> {
        void accept(T t) throws Exception;
    }

    @FunctionalInterface
    interface Function<T, R> {
        R apply(T t) throws Exception;
    }

    private final Logger logger = Logging.LOGGER;

    // transformers map, stores file transformer
    // collection of short coords -> runnable
    private final HashMap<List<String>, Consumer<? super List<File>>> transformers = new HashMap<>();

    // dependency cache, stores downloaded dependency references
    // short coords -> file
    private final HashMap<String, File> cached = new HashMap<>();

    public DependencyTransformer() {
        // org.apache.maven.model.merge.MavenModelMerger in maven-model-builder -> maven-model
        addEntrySwapTransformer("org.apache.maven:maven-model-builder", "org.apache.maven:maven-model",
                List.of(Util.classify("org.apache.maven.model.merge.MavenModelMerger")),
                List.of());

        // org.apache.maven.artifact.repository.metadata.RepositoryMetadataStoreException in maven-artifact -> maven-repository-metadata
        addEntrySwapTransformer("org.apache.maven:maven-artifact", "org.apache.maven:maven-repository-metadata",
                List.of(Util.classify("org.apache.maven.artifact.repository.metadata.RepositoryMetadataStoreException")),
                List.of());

        // Automatic-Module-Name
        addAutomaticModuleNameTransformer("com.github.cryptomorin:XSeries", "xseries");
        addAutomaticModuleNameTransformer("com.github.cliftonlabs:json-simple", "cliftonlabs.jsonsimple");
    }

    /**
     * Transforms the given file with the registered transformers. Any transformer matching the short coords will be
     * executed.
     *
     * @param coords     full coords in the form of {@code groupId:artifactId:version}
     * @param targetFile the file
     */
    public void transform(String coords, File targetFile) {
        // cache the file
        var shortCoords = coords.substring(0, coords.lastIndexOf(':'));
        cached.putIfAbsent(shortCoords, targetFile);

        // execute transformers
        var it = transformers.entrySet().iterator();
        while (it.hasNext()) {
            var en = it.next();
            var shortCoordsList = en.getKey();

            // skip if any coord is missing
            if (shortCoordsList.stream().anyMatch(Predicate.not(cached::containsKey))) continue;
            // remove transformer to prevent multiple executions
            it.remove();

            var files = shortCoordsList.stream().map(cached::get).toList();

            logger.finest(() -> {
                // generate map
                var map = new HashMap<String, String>();
                for (var i = 0; i < shortCoordsList.size(); i++) {
                    map.put(shortCoordsList.get(i), files.get(i).getName());
                }
                return "Transforming dependency files " + map;
            });

            try {
                // execute transformer with the files
                en.getValue().accept(files);
            } catch (Exception e) {
                logger.log(Level.SEVERE, e, () -> "Error while transforming dependency files " + shortCoordsList);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Registers a transformer. The transformer will be executed if all the given short coords are present in the
     * cache.
     *
     * @param shortCoords collection of short coords
     * @param transformer transformer function, will be called with all the files corresponding to the given short
     *                    coords
     */
    public void addTransformer(List<String> shortCoords, Consumer<? super List<File>> transformer) {
        transformers.put(shortCoords, transformer);
    }

    /**
     * Adds a transformer that adds the {@code Automatic-Module-Name} attribute to the manifest file of the given
     * dependency.
     *
     * @param shortCoords         short coords of the dependency
     * @param automaticModuleName the automatic module name
     */
    public void addAutomaticModuleNameTransformer(String shortCoords, String automaticModuleName) {
        addManifestAttributesTransformer(shortCoords, attr -> attr.putValue("Automatic-Module-Name", automaticModuleName));
    }

    /**
     * Adds a transformer that transforms the main attributes of the manifest file of the given dependency.
     *
     * @param shortCoords short coords of the dependency
     * @param visitor     visitor function, will be called with the manifest file of the given dependency
     */
    public void addManifestAttributesTransformer(String shortCoords, Consumer<? super Attributes> visitor) {
        addManifestTransformer(shortCoords, man -> {
            visitor.accept(man.getMainAttributes());
            return man;
        });
    }

    /**
     * Adds a transformer that transforms the manifest file of the given dependency.
     *
     * @param shortCoords short coords of the dependency
     * @param transformer transformer function, will be called with the manifest file of the given dependency, must
     *                    return the new manifest file
     */
    public void addManifestTransformer(String shortCoords, Function<? super Manifest, ? extends Manifest> transformer) {
        addTransformer(List.of(shortCoords), files -> {
            // move file to temp location
            var file = files.get(0);
            var tmp = Files.move(file.toPath(), Files.createTempFile(file.getName(), null),
                    StandardCopyOption.REPLACE_EXISTING).toFile();

            try {
                // apply transformer
                var jf = new JarFile(tmp);
                var man = transformer.apply(jf.getManifest());
                jf.close();

                // write to original location with new manifest
                try (var jin = new JarInputStream(new FileInputStream(tmp));
                     var jout = new JarOutputStream(new FileOutputStream(file), man)) {
                    // copy jar file (never write to alt, because we just want to copy it)
                    varJarCopy(jin, jout, null, je -> false);
                }
            } finally {
                // delete temp file
                tmp.delete();
            }
        });
    }

    /**
     * Adds a transformer that swaps the entries of the given dependencies. Swapping means that any given entry that
     * passes the respective predicate will be put in the other dependency file.
     *
     * @param shortCoords1 short coords of the first dependency
     * @param shortCoords2 short coords of the second dependency
     * @param swap1to2     list of entry names that should be swapped from the first to the second
     * @param swap2to1     list of entry names that should be swapped from the second to the first
     */
    public void addEntrySwapTransformer(String shortCoords1, String shortCoords2,
                                        List<String> swap1to2, List<String> swap2to1) {
        addTransformer(List.of(shortCoords1, shortCoords2), files -> {
            var file1 = files.get(0);
            var file2 = files.get(1);

            // move files to temp locations
            var tmp1 = Files.move(file1.toPath(), Files.createTempFile(file1.getName(), null), StandardCopyOption.REPLACE_EXISTING).toFile();
            var tmp2 = Files.move(file2.toPath(), Files.createTempFile(file2.getName(), null), StandardCopyOption.REPLACE_EXISTING).toFile();

            // create streams from temp files to original locations (while keeping the manifest)
            try (
                    var in1 = new JarInputStream(new FileInputStream(tmp1)); var out1 = new JarOutputStream(new FileOutputStream(file1), in1.getManifest());
                    var in2 = new JarInputStream(new FileInputStream(tmp2)); var out2 = new JarOutputStream(new FileOutputStream(file2), in2.getManifest())
            ) {

                // copy entries from one file to the other while swapping entries that pass the respective predicate
                varJarCopy(in1, out1, out2, je -> swap1to2.contains(je.getName()));
                varJarCopy(in2, out2, out1, je -> swap2to1.contains(je.getName()));

            } finally {
                // finally, delete temp files
                tmp1.delete();
                tmp2.delete();
            }
        });
    }

    /**
     * Writes all entries from the given input stream to one of the given output streams. The predicate determines which
     * output stream is used.
     *
     * @param in         input stream
     * @param defOut     default output stream
     * @param altOut     alternative output stream
     * @param writeToAlt predicate that determines whether an entry should be written to the alternative output
     */
    private void varJarCopy(JarInputStream in, JarOutputStream defOut, JarOutputStream altOut, Predicate<? super JarEntry> writeToAlt) throws IOException {
        JarEntry entry;
        while ((entry = in.getNextJarEntry()) != null) {
            // get output stream
            var out = writeToAlt.test(entry) ? altOut : defOut;

            // copy entry to new jar file
            out.putNextEntry(new JarEntry(entry.getName()));
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.closeEntry();
        }
    }
}
