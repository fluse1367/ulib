package cc.fluse.ulib.loader.minecraft;

import cc.fluse.ulib.loader.environment.Environment;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.install.InitAccess;
import cc.fluse.ulib.loader.install.Installer;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.loader.api.entrypoint.PreLaunchEntrypoint;
import org.quiltmc.loader.impl.entrypoint.EntrypointUtils;

public class ModQuilt implements PreLaunchEntrypoint {

    static {
        RuntimeImpl.environment(Environment.QUILT);
        Installer.installTo(ClassLoader.getSystemClassLoader());
        Installer.installMe();

        try {
            ModFabricQuiltUtil.inject("org.quiltmc", InitAccess.getInstance()::classBytes);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    @Override
    public void onPreLaunch(ModContainer mod) {
        EntrypointUtils.invoke("ulib_pre_launch", PreLaunchEntrypoint.class, PreLaunchEntrypoint::onPreLaunch);
    }
}
