package cc.fluse.ulib.loader.agent;

import cc.fluse.ulib.loader.impl.Logging;
import jdk.internal.org.objectweb.asm.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static java.util.function.Predicate.not;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class AgentMain {

    public static void premain(String agentArgs, Instrumentation inst) {
        agentmain(agentArgs, inst);
    }

    public static void agentmain(String agentArgs, Instrumentation inst) {
        var logger = Logging.LOGGER;
        logger.info(() -> "Agent initialized");
        System.getProperties().put("ulib.javaagent", inst);

        Optional.of(System.getProperty("ulib.install.tweak", ""))
                .filter(s -> !s.isBlank())
                .ifPresent(s -> {
                    try {
                        inst.addTransformer(new Tweaker(List.of(s.split(",")), logger));
                    } catch (IllegalAccessError e) {
                        logger.severe(() -> "Cannot tweak targets. Please run with --add-export java.base/jdk.internal.org.objectweb.asm=" +
                                Optional.ofNullable(AgentMain.class.getModule().getName()).orElse("ALL-UNNAMED"));
                        System.exit(15);
                    }
                });
    }

    private static class Tweaker implements ClassFileTransformer {
        /**
         * Regex that matches exactly one valid JNI identifier. No capturing groups.
         */
        private static final String REGEX_JNI_IDENTIFIER = "[\\p{L}_$][\\p{L}\\p{N}_$]*";

        /**
         * Regex that matches exactly one valid JNI member identifier (like JNI identifier but also allows
         * <code>{@code <init>}</code> and <code>{@code <clinit>}</code>). No capturing groups.
         */
        private static final String REGEX_JNI_MEMBER_IDENTIFIER = "(?:%s)|<init>|<clinit>"
                .formatted(REGEX_JNI_IDENTIFIER);

        /**
         * Regex that matches a valid JNI fully qualified class name (without the leading 'L' and trailing ';'). No
         * capturing groups.
         */
        private static final String REGEX_JNI_FULLY_QUALIFIED_CLASS = "(?:%s\\/)*%1$s"
                .formatted(REGEX_JNI_IDENTIFIER);

        /**
         * Regex that matches exactly one valid JNI type descriptor. No capturing groups.
         *
         * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/types.html#type_signatures"
         * target="_blank">JNI Type Signatures</a>
         */
        private static final String REGEX_JNI_TYPE_DESCRIPTOR = "\\[*(?:[ZBCSIJFDV]|(?:L" + REGEX_JNI_FULLY_QUALIFIED_CLASS + ";))";


        /**
         * Regex that matches exactly one fully valid descriptor of a class member. Capturing groups:
         * <ol>
         *     <li>class name: fully qualified jni class name</li>
         *     <li>member name: jni member identifier</li>
         *     <li>member method parameters: type descriptors; optional</li>
         *     <li>member method return type: type descriptor; optional</li>
         * </ol>
         */
        // TODO: allow <init> and <clinit> as member names
        private static final String REGEX_JVM_DESCRIPTOR = "L(${Q});(${M})(?:\\(((?:${T})*)\\)(${T}))?"
                .replace("${Q}", REGEX_JNI_FULLY_QUALIFIED_CLASS)
                .replace("${M}", REGEX_JNI_MEMBER_IDENTIFIER)
                .replace("${T}", REGEX_JNI_TYPE_DESCRIPTOR);

        private static final Pattern PAT_JVM_DESCRIPTOR = Pattern.compile(REGEX_JVM_DESCRIPTOR);


        private final Logger logger;
        // class -> member -> collection of descriptors
        private final Map<String, Map<String, Collection<String>>> targets;

        private Tweaker(Collection<String> targets, Logger logger) {
            this.logger = logger;
            this.targets = new HashMap<>();
            // parse targets
            targets.forEach(s -> {
                var matcher = PAT_JVM_DESCRIPTOR.matcher(s);
                if (!matcher.matches()) throw new IllegalArgumentException("Invalid target: " + s);

                var className = matcher.group(1);
                var memberName = matcher.group(2);
                var descs = this.targets
                        .computeIfAbsent(className, k -> new HashMap<>())
                        .computeIfAbsent(memberName, k -> new LinkedList<>());
                String descriptor;

                // <clinit> cannot have parameters or return type
                if (memberName.equals("<clinit>")) {
                    descriptor = "()V";
                } else {
                    if (matcher.groupCount() != 4) throw new IllegalArgumentException("Invalid target: " + s);
                    descriptor = "(%s)%s".formatted(matcher.group(3), matcher.group(4));
                }

                descs.add(descriptor);

                logger.info(() -> "Configuring tweak target: " + className + "." + memberName + descriptor);
            });
        }

        @Override
        public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
            if (!targets.containsKey(className)) return null;

            var cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);

            // deep copy targets
            var map = targets.get(className);
            var targets = new HashMap<String, Collection<String>>(map.size(), 1F); // load factor 1 to avoid rehashing
            map.forEach((k, v) -> targets.put(k, new LinkedList<>(v))); // mutable list to allow removal

            logger.fine(() -> "Tweaking class: %s %s".formatted(className, targets.toString()));

            // visit whole class structure, intercepting methods
            new ClassReader(classfileBuffer).accept(new ClassVisitor(Opcodes.ASM8, cw) {
                @Override
                public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                    var mv = super.visitMethod(access, name, descriptor, signature, exceptions);
                    if (!targets.containsKey(name) || !targets.get(name).contains(descriptor)) return mv;
                    targets.get(name).remove(descriptor);

                    // add static call to Installer.installMe() at the beginning of target methods
                    return new MethodVisitor(Opcodes.ASM8, mv) {
                        @Override
                        public void visitCode() {
                            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "cc/fluse/ulib/loader/install/Installer", "installMe", "()V", /*isInterface*/false);
                            super.visitCode();
                        }
                    };
                }
            }, 0);

            // manually create <clinit> if it wasn't in the class
            if (Optional.ofNullable(targets.remove("<clinit>")).filter(not(Collection::isEmpty)).isPresent()) {
                logger.fine(() -> "Adding <clinit> to class: " + className);

                var mv = cw.visitMethod(Opcodes.ACC_STATIC, "<clinit>", "()V", null, null);
                mv.visitCode();
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, "cc/fluse/ulib/loader/install/Installer", "installMe", "()V", /*isInterface*/false);
                mv.visitInsn(Opcodes.RETURN);
                mv.visitMaxs(0, 0);
                mv.visitEnd();
            }

            // check for non-intercepted targets
            targets.forEach((k, v) -> {
                if (!v.isEmpty()) logger.warning(() -> "Target not found: %s.%s%s".formatted(className, k, v));
            });

            return cw.toByteArray();
        }
    }
}
