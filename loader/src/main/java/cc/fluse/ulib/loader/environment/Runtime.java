package cc.fluse.ulib.loader.environment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;

import java.util.Collection;

/**
 * Represents the ulib runtime.
 */
public interface Runtime {

    /**
     * Returns the environment ulib was started in.
     *
     * @return the environment
     */
    @NotNull
    Environment getEnvironment();

    /**
     * Returns ulib's version. If the current runtime is a release, the returned string complies to the <a
     * href="https://semver.org/">semantic versioning scheme</a>. In case of a snapshot, the version string has
     * "{@code -SNAPSHOT-HASH}" appended, where {@code HASH} is replaced by the last 7 characters of the git commit
     * hash.
     *
     * @return ulib's version
     */
    String getVersion();

    /**
     * Returns a collection of available modules in the ulib runtime.
     *
     * @return an immutable collection of the activated modules
     */
    @NotNull
    @Unmodifiable
    Collection<@NotNull Module> getAvailableModules();

    /**
     * Determines if a certain module is available in the ulib runtime.
     *
     * @param module the module to test for
     * @return {@code true} if the module is available, {@code false} otherwise
     */
    boolean isModuleAvailable(@NotNull Module module);


    /**
     * Checks if the environment is considered standalone.
     *
     * @return {@code true}, if this environment is running in standalone mode, {@code false} otherwise
     */
    boolean isStandalone();

    /**
     * Checks if the environment is considered a minecraft-context environment.
     *
     * @return {@code true}, if this environment is running in a minecraft context mode, {@code false} otherwise
     */
    boolean isMinecraft();

    /**
     * Checks if the environment is considered a bungeecord environment.
     *
     * @return {@code true}, if this environment is running in bungeecord mode, {@code false} otherwise
     */
    boolean isBungeecord();

    /**
     * Checks if the environment is considered a velocity environment.
     *
     * @return {@code true}, if this environment is running in velocity mode, {@code false} otherwise
     */
    boolean isVelocity();

    /**
     * Checks if the environment is considered a spigot environment.
     *
     * @return {@code true}, if this environment is running in spigot mode, {@code false} otherwise
     */
    boolean isSpigot();

    /**
     * Checks if the environment is considered a fabric environment.
     *
     * @return {@code true}, if this environment is running in fabric mode, {@code false} otherwise
     */
    boolean isFabric();

    /**
     * Checks if the environment is considered a quilt environment.
     *
     * @return {@code true}, if this environment is running in quilt mode, {@code false} otherwise
     */
    boolean isQuilt();

    /**
     * Checks if the environment is considered a server environment.
     *
     * @return {@code true}, if this environment is considered a server environment, {@code false} otherwise
     */
    boolean isServer();

    /**
     * Checks if the environment is considered a client environment.
     *
     * @return {@code true}, if this environment is considered a client environment, {@code false} otherwise
     */
    boolean isClient();


}
