package cc.fluse.ulib.loader.impl;

import cc.fluse.ulib.loader.environment.Environment;
import cc.fluse.ulib.loader.environment.Module;
import cc.fluse.ulib.loader.environment.Runtime;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.impl.FabricLoaderImpl;
import org.jetbrains.annotations.NotNull;
import org.quiltmc.loader.impl.QuiltLoaderImpl;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RuntimeImpl implements Runtime {

    private static RuntimeImpl runtime;
    private static Environment environment;

    @NotNull
    public static synchronized RuntimeImpl get() {
        if (runtime != null) return runtime;
        return runtime = new RuntimeImpl(Optional.ofNullable(System.getProperty("ulib.install.env_overwrite"))
                                                 .map(envName -> {
                                                     try {
                                                         var env = Environment.valueOf(envName);
                                                         Logging.LOGGER.warning(() ->
                                                                 "(ulib) Overwriting environment with " + env.name());
                                                         return env;
                                                     } catch (IllegalArgumentException e) {
                                                         // ignored
                                                     }
                                                     return null;
                                                 })
                                                 .or(() -> Optional.ofNullable(environment))
                                                 .orElse(Environment.STANDALONE));
    }

    public static synchronized void environment(@NotNull Environment env) {
        if (runtime != null || environment != null) throw new IllegalStateException();
        environment = env;
    }

    public static RuntimeImpl deserialize(@NotNull Object obj) throws IllegalArgumentException {
        try {
            var mv = Module.values();

            var arr = (Object[]) obj;
            return new RuntimeImpl(
                    (String) arr[0],
                    Environment.values()[(int) arr[1]],
                    IntStream.of((int[]) arr[2]).mapToObj(i -> mv[i]).collect(Collectors.toSet())
            );
        } catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
        }
    }

    // --


    private final String ver;
    private final Environment env;
    private final Set<Module> availableModules;

    private RuntimeImpl(@NotNull Environment env) {
        this.env = env;
        this.availableModules = new HashSet<>(env.getDefaults());
        this.ver = Optional.ofNullable(getClass().getPackage().getImplementationVersion()).orElse("0.0.0-UNKNOWN");
    }

    private RuntimeImpl(String ver, Environment env, Set<Module> availableModules) {
        this.ver = ver;
        this.env = env;
        this.availableModules = availableModules;
    }

    public Object serialize() {

        var modules = new int[availableModules.size()];
        int i = 0;
        for (Module mod : availableModules) {
            modules[i++] = mod.ordinal();
        }

        return new Object[]{
                ver, env.ordinal(),
                modules
        };
    }

    @Override
    public String getVersion() {
        return ver;
    }

    @Override
    public @NotNull Environment getEnvironment() {
        return env;
    }

    @NotNull
    @Override
    public Set<Module> getAvailableModules() {
        return Collections.unmodifiableSet(availableModules);
    }

    @Override
    public boolean isModuleAvailable(@NotNull Module module) {
        return availableModules.contains(module);
    }

    public void addActiveModules(Collection<? extends Module> modules) {
        availableModules.addAll(modules);
    }

    @Override
    public boolean isStandalone() {
        return env == Environment.STANDALONE;
    }

    @Override
    public boolean isMinecraft() {
        return env.getDefaults().contains(Module.MINECRAFT);
    }

    @Override
    public boolean isBungeecord() {
        return env == Environment.BUNGEECORD;
    }

    @Override
    public boolean isVelocity() {
        return env == Environment.VELOCITY;
    }

    @Override
    public boolean isSpigot() {
        return env == Environment.SPIGOT;
    }

    @Override
    public boolean isFabric() {
        return env == Environment.FABRIC;
    }

    @Override
    public boolean isQuilt() {
        return env == Environment.QUILT;
    }

    @Override
    public boolean isServer() {
        return isVelocity() || isBungeecord() || isSpigot() || !isClient();
    }

    @Override
    public boolean isClient() {
        return isQuilt()
               ? QuiltLoaderImpl.INSTANCE.getEnvironmentType() == EnvType.CLIENT
               : isFabric() && FabricLoaderImpl.INSTANCE.getEnvironmentType() == EnvType.CLIENT;
    }


}
