package cc.fluse.ulib.loader.impl.dependency;

import cc.fluse.ulib.loader.impl.Util;
import lombok.SneakyThrows;

import java.io.*;
import java.util.Base64;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.jar.JarFile;
import java.util.logging.Logger;

public final class DependencyProvider {

    private final Logger logger;
    final File modsDir;
    final File libsDir;
    private final JarFile jar;
    private final Map<String, Map<String, ?>> modulesGraph;

    @SneakyThrows
    public DependencyProvider(Logger logger) {
        this.logger = logger;

        var dataDir = new File(System.getProperty("ulib.directory.main", ".ulib"));
        this.modsDir = new File(dataDir, "modules");
        this.libsDir = new File(modsDir, "libraries");
        this.jar = new JarFile(new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()));

        logger.fine(() -> "DependencyProvider initialized; data dir: %s, mods dir: %s, libs dir: %s"
                .formatted(dataDir, modsDir, libsDir));

        initDir(dataDir);
        initDir(modsDir);
        initDir(libsDir);


        // read modules graph
        try {
            var val = jar.getManifest().getMainAttributes().getValue("Modules");
            if (val == null)
                throw new IllegalArgumentException("Modules key not found");

            // deserialize modules graph
            byte[] serialized = Base64.getDecoder().decode(val);
            try (var in = new ObjectInputStream(new ByteArrayInputStream(serialized))) {
                this.modulesGraph = (Map<String, Map<String, ?>>) in.readObject();
            }

            logger.finer(() -> "Modules graph: %s".formatted(modulesGraph.toString()));
        } catch (Exception e) {
            throw new IllegalArgumentException("MANIFEST.MF file invalid", e);
        }
    }

    private void initDir(File f) {
        if (!f.exists())
            if (!f.mkdirs()) {
                throw new RuntimeException("Cannot create '" + f + "' directory!");
            }
    }

    public Collection<File> extractModules(Collection<String> what) {
        logger.finer(() -> "Extracting modules: {%s}".formatted(String.join(", ", what)));
        return what.stream()
                .map(this::extractModule)
                .flatMap(Collection::stream)
                .toList();
    }

    public Collection<File> extractModule(String what) {
        return extractJars(what, modsDir);
    }

    @SneakyThrows
    Collection<File> extractJars(String module, File dir) {
        return ((Collection<String>) modulesGraph.get(module).get("jars")).stream()
                .map(elem -> extractJar(elem, dir))
                .toList();
    }

    @SneakyThrows
    private File extractJar(String jarName, File destDir) {
        logger.finer(() -> "Extracting jar %s to %s".formatted(jarName, destDir));

        File dest = new File(destDir, jarName);
        String location = "META-INF/jars/" + jarName;
        var en = jar.getEntry(location);

        var in = jar.getInputStream(en);

        if (dest.exists()) { // library already exists, check hash
            if (Util.getCRC32(in) == Util.getCRC32(new FileInputStream(dest))) {
                return dest; // same hash, skip extraction
            }
            in.close();
            in = jar.getInputStream(en);
        }
        // extract!
        Util.write(in, new FileOutputStream(dest, false));
        return dest;
    }

    public Collection<File> downloadLibraries(Collection<String> modules, Predicate<String> coordsFilter, BiConsumer<String, File> callback) {
        logger.finer(() -> "Downloading libraries for modules: {%s}".formatted(String.join(", ", modules)));
        var downloader = new DependencyDownloader(logger);
        return modules.stream()
                .flatMap(module -> ((Collection<String>) modulesGraph.get(module).get("libraries")).stream()) // unpack libraries
                .distinct()
                .filter(coordsFilter)
                .map(coords -> downloader.download(coords, libsDir, f -> callback.accept(coords, f)))
                .toList();
    }
}
