package cc.fluse.ulib.loader.impl.install;

import cc.fluse.ulib.loader.agent.AgentInstaller;
import cc.fluse.ulib.loader.environment.Module;
import cc.fluse.ulib.loader.impl.Logging;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.dependency.DependencyProvider;
import cc.fluse.ulib.loader.impl.dependency.DependencyTransformer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


// initializes ulib
public class Initializer {
    private static Initializer instance;

    @Synchronized
    public static Initializer provide(Object obj) {
        if (instance == null) {
            instance = obj instanceof RuntimeImpl r ? new Initializer(r)
                                                    : new Initializer(RuntimeImpl.deserialize(obj));
        }
        return instance;
    }

    private final Logger logger = Logging.LOGGER;
    private final RuntimeImpl runtime;

    @Getter
    private final Injector injector;
    private final DependencyProvider dependencyProvider = new DependencyProvider(logger);

    private Collection<File> filesModules, filesAdditional;

    @Getter
    private ModuleLoader loader;
    @Getter(AccessLevel.PACKAGE)
    private Set<java.lang.Module> apiModules;
    private final ClassLoader coreLoader;

    private Initializer(RuntimeImpl runtime) {
        this.runtime = runtime;
        this.injector = new Injector(this, logger);

        provideDependencies();
        initLoaders(); // init loader, apiModules and layer

        this.coreLoader = loader.getLayer().findLoader("ulib.core");
        initImpl();
        injector.initDelegation();

        // apply additional hooks
        @Nullable
        @SuppressWarnings("SwitchStatementWithTooFewBranches")
        Map<String, Collection<Class<?>[]>> hooks = switch (runtime.getEnvironment()) {
            case SPIGOT ->
                    Map.of("loadClass0", Collections.singletonList(new Class<?>[]{String.class, boolean.class, boolean.class, boolean.class}));
            default -> null;
        };
        if (hooks != null) {
            hooks.forEach((method, coll) -> coll
                    .forEach(types ->
                                     injector.additionally(method, types, params ->
                                             params.length > 0 && params[0] instanceof String name
                                             ? Optional.of(name)
                                             : Optional.empty()
                                     )));
        }
    }

    @SneakyThrows
    Class<?> coreClass(String name) {
        return Class.forName(name, true, coreLoader);
    }

    @SneakyThrows
    public Object construct(String module, String className, Object[] initArgs) {
        // only `InitAccess` is permitted to directly construct objects
        var caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        if (caller == null || !caller.getName().equals(InitAccess.class.getName())) {
            throw new SecurityException();
        }

        String fullClassName = "%s.%s.%s".formatted("cc.fluse.ulib", module, className);
        var loader = this.loader.getLayer().findLoader("ulib." + module);
        var cl = Class.forName(fullClassName, true, loader);
        return cl.getConstructors()[0].newInstance(initArgs);
    }

    private void provideDependencies() {
        var modules = runtime.getAvailableModules().stream().map(Module::getName).collect(Collectors.toSet());
        logger.fine(() -> "Providing dependencies with {%s}".formatted(String.join(", ", modules)));
        this.filesModules = dependencyProvider.extractModules(modules);

        var transformer = new DependencyTransformer();

        Predicate<String> filter = coords ->
                !(runtime.isVelocity() && coords.startsWith("org.slf4j:"));
        this.filesAdditional = dependencyProvider.downloadLibraries(modules, filter, transformer::transform);
    }

    private void initLoaders() {
        // init loader for ulib regular layer
        var files = Stream.of(filesModules.stream(), filesAdditional.stream())
                .flatMap(s -> s)
                .toList();

        logger.finer(() -> "Initializing module layer with classpath {%s}".formatted(String.join(", ", files.stream().map(File::toString).toList())));

        var directParent = Optional.ofNullable(getClass().getModule().getLayer()).orElseGet(ModuleLayer::boot);

        List<ModuleLayer> parentLayers = new ArrayList<>(1);

        boolean comply = false;
        switch (System.getProperty("ulib.install.module_layer", "parent")) {
            case "boot":
                logger.warning(() -> "(ulib-loader) Ignoring parent module layer");
                break;
            case "comply":
                logger.warning(() -> "(ulib-loader) Respecting parent modules");
                comply = true;
                // fallthrough
            case "parent":
                // fallthrough
            default:
                parentLayers.add(directParent);
        }

        this.loader = new ModuleLoader(this, null, files, getClass().getClassLoader(), parentLayers, comply);

        logger.fine(() -> "Initialized module layer with {%s}".formatted(String.join(", ", loader.getLayer().modules().stream().map(java.lang.Module::getName).toList())));

        var apiWhitelist = runtime.getAvailableModules().stream()
                .map(m -> "ulib." + m.getName())
                .toList();

        this.apiModules = loader.getLayer().modules().stream()
                .filter(m -> apiWhitelist.contains(m.getName()))
                .collect(Collectors.toSet());

        // add reads record if necessary
        var me = getClass().getModule();
        apiModules.stream()
                  .filter(m -> !me.canRead(m))
                  .forEach(me::addReads);
    }

    @SneakyThrows
    private void initImpl() {
        // agent init
        if (!System.getProperties().containsKey("ulib.javaagent") && !new AgentInstaller(logger).install()) {
            throw new RuntimeException("Unable to install agent");
        }

        logger.info(() -> "Initializing ulib implementation");

        // pass agent to implementation
        var clInit = coreClass("cc.fluse.ulib.core.impl.init.Init");
        clInit.getMethod("init", Object.class).invoke(null, System.getProperties().remove("ulib.javaagent"));
    }

}
