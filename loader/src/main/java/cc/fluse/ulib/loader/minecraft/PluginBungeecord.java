package cc.fluse.ulib.loader.minecraft;

import cc.fluse.ulib.loader.environment.Environment;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.install.InitAccess;
import cc.fluse.ulib.loader.install.Installer;
import net.md_5.bungee.api.plugin.Plugin;

public class PluginBungeecord extends Plugin {
    static {
        RuntimeImpl.environment(Environment.BUNGEECORD);
        Installer.installMe();
    }

    private Plugin pluginSubstitute;

    @Override
    public void onLoad() {
        this.pluginSubstitute = (Plugin) InitAccess.getInstance().construct("bungeecord", "impl.PluginSubst",
                                                                            this, getProxy(), getDescription());
    }

    @Override
    public void onEnable() {
        pluginSubstitute.onEnable();
    }

    @Override
    public void onDisable() {
        pluginSubstitute.onDisable();
    }
}
