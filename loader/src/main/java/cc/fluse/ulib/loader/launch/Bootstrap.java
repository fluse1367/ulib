package cc.fluse.ulib.loader.launch;

import cc.fluse.ulib.loader.install.Installer;

public final class Bootstrap {
    public static void main(String[] args) {
        Installer.installMe();
        CLI.parse(args);
    }
}
