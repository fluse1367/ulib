package cc.fluse.ulib.loader.install;

import cc.fluse.ulib.loader.environment.Module;
import cc.fluse.ulib.loader.environment.Runtime;
import cc.fluse.ulib.loader.impl.install.InitAccess;
import lombok.*;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Installer {

    private static final InitAccess access = InitAccess.getInstance();

    /**
     * Sets modules to be loaded on initialization. Can only be done before initialization.
     *
     * @param modules the modules to get activated
     * @throws IllegalStateException in case ulib is already initialized
     */
    @Synchronized
    public static void activateModules(@NotNull Module @NotNull ... modules) throws IllegalStateException {
        for (Module module : modules) {
            access.addModule(module);
        }
    }

    /**
     * Returns the environment ulib is running in. Can only be called after initialization.
     *
     * @return the environment object
     */
    public static Runtime getRuntime() throws IllegalStateException {
        return access.getRuntime();
    }

    /**
     * Installs the uLib API to a class loader by injecting code into it.
     *
     * @param target the class loader to install the API to
     * @throws IllegalArgumentException If the uLib API has already been installed to that class loader
     * @implNote If the target is the loader of a named module, an
     * {@link java.lang.Module#addReads(java.lang.Module) reads} record must be added by that module manually in order
     * for that module to be able to interact with the uLib API.
     * @see #getLayer()
     */
    @Synchronized
    public static void installTo(ClassLoader target) {
        try {
            access.install(target, null);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Install the uLib API to the class loader of the calling class.
     *
     * @implNote Unlike {@link #installTo(ClassLoader)}, this method will automatically add an
     * {@link java.lang.Module#addReads(java.lang.Module) reads} record to the module of the calling class (if
     * necessary).
     */
    @Synchronized
    public static void installMe() {
        var caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        var loader = caller.getClassLoader();
        try {
            access.install(loader, caller.getModule());
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the module layer object from the uLib api.
     *
     * @return the module layer object
     */
    public static ModuleLayer getLayer() {
        return access.layer();
    }

    /**
     * Ensures the library is initialized. Attempts to initialize the library if necessary.
     *
     * @throws IllegalStateException if the initialization failed
     */
    public static void ensureInitialization() throws IllegalStateException {
        try {
            access.ensureInit();
        } catch (RuntimeException re) {
            throw new IllegalStateException(re.getMessage(), re.getCause());
        }
    }
}
