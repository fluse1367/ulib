package cc.fluse.ulib.loader.agent;

import cc.fluse.ulib.loader.impl.Logging;
import com.sun.tools.attach.VirtualMachine;
import lombok.SneakyThrows;

/*
This class is designed to be started in a dedicated java process.
It loads the AgentMain and attaches it to the original java instance.
 */
public final class LoaderExternal {
    @SneakyThrows
    public static void main(String[] args) {
        String pid = args[0];
        String agentPath = args[1];

        Logging.LOGGER.fine(() -> "Attaching agent to process " + pid + " with agent " + agentPath);

        // attach this extra process to original jvm
        VirtualMachine vm = VirtualMachine.attach(pid);

        // load agent!
        vm.loadAgent(agentPath);
    }
}
