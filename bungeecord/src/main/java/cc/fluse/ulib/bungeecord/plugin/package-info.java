/**
 * Classes regarding extended BungeeCord-Proxy plugin functionality.
 *
 * @see cc.fluse.ulib.minecraft.plugin
 */
package cc.fluse.ulib.bungeecord.plugin;