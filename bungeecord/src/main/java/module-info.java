module ulib.bungeecord {
    // static
    requires static lombok;
    requires static org.jetbrains.annotations;
    requires static bungeecord.api;

    // java
    requires java.logging;

    // 3rd party

    // ulib
    requires transitive ulib.minecraft;

    // api exports
    exports cc.fluse.ulib.bungeecord.player;
    exports cc.fluse.ulib.bungeecord.plugin;

    // impl exports
    exports cc.fluse.ulib.bungeecord.impl to ulib.loader;
}