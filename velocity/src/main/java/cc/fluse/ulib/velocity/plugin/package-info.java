/**
 * Classes regarding extended Velocity-Proxy plugin functionality.
 *
 * @see cc.fluse.ulib.minecraft.plugin
 */
package cc.fluse.ulib.velocity.plugin;